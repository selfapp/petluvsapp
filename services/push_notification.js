import { Permissions, Notifications } from "expo";
import { AsyncStorage } from "react-native";
import { baseurl } from "./../BaseUrl";

const PUSH_ENDPOINT = `${baseurl}/users/push-token`;

export default async userToken => {
  const promise = new Promise(async (resolve, reject) => {
    const previousToken = await AsyncStorage.getItem("pushToken");

    if (previousToken) {
      resolve({ pushToken: previousToken, message: "success" });
    }
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );

    let finalStatus = existingStatus;

    if (existingStatus !== "granted") {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    if (finalStatus !== "granted") {
      reject({ message: "permission_denied", token: null });
    }

    const pushToken = await Notifications.getExpoPushTokenAsync();
    // resolve({ pushToken, message: "permission_granted" });

    if (userToken) {
      try {
        const res = await fetch(PUSH_ENDPOINT, {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${userToken}`
          },
          body: JSON.stringify({ pushToken })
        });
        const resJson = await res.json();
        if (res.status === 200) {
          resolve({ pushToken: resJson, message: "success" });
        }
      } catch (e) {
        console.log(
          `Error while making saving push notification call${e}`
        );
      }
    } else {
      resolve({ message: "error" });
    }

  });

  return promise;
};
