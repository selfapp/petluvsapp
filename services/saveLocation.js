import { Permissions, Location, Constants, AsyncStorage } from "expo";
import { baseurl } from "./../BaseUrl";

const LOCATION_ENDPOINT = `${baseurl}/users/save-location`;

export default async userToken => {
  const promise = new Promise(async (resolve, reject) => {
    const { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status != "granted") {
      reject({ message: "permission_denied", currentLocation: null });
    }
    const location = await Location.getCurrentPositionAsync({});
    //
    const loc = {
      lat: null,
      lng: null
    };

    if (location && location.coords) {
      const { coords } = location;

      if (coords.latitude & coords.longitude) {
        const { latitude, longitude } = coords;

        loc.lat = latitude;
        loc.lng = longitude;
      }
      if (userToken) {
        // console.log(LOCATION_ENDPOINT, userToken);
        try {
          const postRes = await fetch(LOCATION_ENDPOINT, {
            method: "PUT",
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization: `Bearer ${userToken}`
            },
            body: JSON.stringify({ currentLocation: loc })
          });
          const postResponseJson = await postRes.json();
          if (postRes.status === 200) {
            // console.log("Saving location successfull");
            resolve({ message: "success", currentLocation: loc });
          }
        } catch (e) {
          reject({ message: "Save location failed", currentLocation: null });
        }
      }
    }
    // navigator.geolocation.getCurrentPosition(
    //   position => {
    //     // this.setState({
    //     //   latitude: position.coords.latitude,
    //     //   longitude: position.coords.longitude
    //     // });
    //     loc.lat = position.coords.latitude;
    //     loc.lng = position.coords.longitude;
    //
    //     // this.saveLocation();
    //     console.log('lat, lng', loc, loc.lat);
    //   },
    //   error => {},
    //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 10000 }
    // );
  });
};
