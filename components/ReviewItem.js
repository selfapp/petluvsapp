import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

import GeneralStyles from "../Styles/General";
import ProfilePhoto from "../components/ProfilePhoto";

class ReviewItem extends React.Component {

  renderStar() {

    var starView = [];
    for (let i = 0; i < 5; i++) {
      if (i < this.props.starCount) {
        starView.push(<Icon key={i.toString()} name="star" size={15} color="#56C1EF"/>)
      } else {
        starView.push(<Icon key={i.toString()} name="star-border" size={15} color="#56C1EF"/>)
      }
    }
    return starView;
  }

  render() {
    return (
      <View style={{
        paddingVertical: 10,
        paddingHorizontal: 10
      }}
      >
        <View style={{flexDirection: "row"}}>
          <ProfilePhoto
            photo={this.props.ProfilePic}
          />
          <View style={{paddingLeft: 15}}>
            <Text style={GeneralStyles.h1}>{this.props.Name}</Text>
            <Text style={{opacity: 0.5}}>
              Reviewed {this.props.playdateOwner}'s playdate
            </Text>
          </View>
        </View>
        <View>
          <View style={{flexDirection: "row", marginTop: 14}}>
            {
              this.renderStar()
            }
          </View>
          <Text style={styles.ReviewText}>{this.props.ReviewText}</Text>
        </View>
        {!this.props.showverification ? (
          <View
            style={{
              justifyContent: "flex-end",
              flexDirection: "row"
            }}
          >
            <TouchableOpacity
              style={{
                height: 30,
                backgroundColor: "#cccccc",
                marginRight: 15,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 5
              }}
              onPress={() => {
                this.props.decline(this.props.id);
                this.props.fetchReviews();
              }}
            >
              <Text style={{color: "#ffffff", marginHorizontal: 10}}>
                Reject
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: 30,
                backgroundColor: "#56C1EF",
                marginRight: 15,
                alignItems: "center",
                justifyContent: "center",
                borderRadius: 5
              }}
              onPress={() => {
                this.props.accept(this.props.id);
                this.props.fetchReviews();
              }}
            >
              <Text style={{color: "#ffffff", marginHorizontal: 10}}>
                Accept
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ReviewText: {
    color: "#8F858C"
  }
});

export default ReviewItem;
