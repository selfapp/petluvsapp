import React from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import GeneralStyles from "../Styles/General";

class AbbreviatedAttendees extends React.Component {
  render() {
    let pets = this.props.pets;
    const totalLength = pets.length;

    let displableNumber = 0;
    let howmayToShow = 3;
    displableNumber =  totalLength - howmayToShow;
    return (
      <View style={{flexDirection: 'row', marginLeft: 10}}>
          {this.props.pets.slice(0,3).map((pet, key) => {
            return (
                <View style={styles.ProfileCircle} key={key}>
                  { pet.imagePath[0] && pet.imagePath[0].url && pet.imagePath[0].url !== ""
                    ? <Image source={{ uri: pet.imagePath[0].url }} style={{width: 30, height: 30}} resizeMode='contain' />
                    : <Image source={require('../assets/profile.png')} style={{width: 30, height: 30}} resizeMode='contain'/>
                  }
                </View>
            )
          })}
          { totalLength > 3 &&
            <View style={styles.TruncateCircle}>
              <Text style={{color:'white'}}>+{displableNumber}</Text>
            </View>
          }
      </View>

    );
  }
}

const styles = StyleSheet.create({
  ProfileCircle: {
    height: 30,
    width: 30,
    borderRadius: 20,
    overflow: "hidden",
    // justifyContent: "center",
    // alignItems: "center",
    marginLeft: -10,
    borderColor: "white",
    borderWidth: 3
  },
  TruncateCircle: {
    height: 30,
    width: 30,
    borderRadius: 20,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: -10,
    borderColor: "white",
    borderWidth: 3,
    backgroundColor: "#EF5595"
  },
  ProfilePhoto: {
    resizeMode: "contain"
  }
});

export default AbbreviatedAttendees;
