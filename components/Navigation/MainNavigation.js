/* eslint-disable prettier/prettier */
import React, {Component} from "react";
import {
  Image,
  View,
  TouchableOpacity,
  Keyboard,
  Text
} from "react-native";
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import Icon from "react-native-vector-icons/FontAwesome";
import {connect} from "react-redux";

import HomeScreen from "../../screens/HomeScreen";
import HotSpotsScreen from "../../screens/HotSpotsScreen";
import ServicesScreen from "../../screens/ServicesScreen";
import ProfileScreen from "../../screens/Profile/ProfileScreen";
import EditProfile from "../../screens/Profile/EditProfile";
import PlaydateCreation from "../../screens/Playdate/PlaydateCreation";
import PlaydateDetails from "../../screens/Playdate/PlaydateDetails";
import QuickActions from "../QuickActions";
import SearchDetails from "../../screens/SearchDetails";
import Website from "../../screens/Website";
import PlaydateItem from "../PlaydateItem";
import CreateBreederEvent from "../../screens/CreateBreederEvent";
import AddPet from "../../screens/AddPet";
import SaveServiceProviderInfo from "../../screens/SaveServiceProviderInfo";
import PetProfile from "../../screens/Profile/PetProfile";
import Notifications from "../../screens/Notifications";
import PlaydateEdit from "../../screens/Playdate/PlaydateEdit";

const TabBar = new createBottomTabNavigator(
  {
    Home: {screen: HomeScreen},
    HotSpots: {screen: HotSpotsScreen},
    ActionModal: {
      screen: () => null,
      navigationOptions: ({navigation}) => ({
        tabBarIcon: ({focused, tintColor}) => {
          return <QuickActions navigation={navigation}/>
        }
      })
    },
    Services: {screen: ServicesScreen},
    Profile: {screen: ProfileScreen}
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === "Home") {
          iconName = `home`;
        } else if (routeName === "HotSpots") {
          iconName = `paw`;
        } else if (routeName === "ActionModal") {
          iconName = `plus`;
        } else if (routeName === "Services") {
          iconName = `tags`;
        } else if (routeName === "Profile") {
          iconName = `user`;
        }
        let iconSize;
        if (routeName === "Home") {
          iconSize = 25;
        } else if (routeName === "HotSpots") {
          iconSize = 25;
        } else if (routeName === "ActionModal") {
          iconSize = 50;
        } else if (routeName === "Services") {
          iconSize = 25;
        } else if (routeName === "Profile") {
          iconSize = 25;
        }
        if (routeName === "Services") {
          return (
            <Image
              style={{tintColor}}
              source={require("../../assets/servicesInactive.png")}
              resizeMode="contain"
            />
          );
        }
        if (routeName === "HotSpots") {
          return (
            <Image
              style={{tintColor}}
              source={require("../../assets/hotspotsInactive.png")}
              resizeMode="contain"
            />
          );
        }
        return <Icon name={iconName} size={iconSize} color={tintColor}/>;
      }
    }),
    tabBarOptions: {
      activeTintColor: "#56C1EF",
      inactiveTintColor: "#D9D6D8",
      showLabel: false,
      lazyLoad: true,
      style: {
        backgroundColor: "white",
        borderTopWidth: 0,
        shadowColor: "#000",
        shadowOffset: {width: 0, height: -2},
        shadowOpacity: 0.05,
        shadowRadius: 15
      }
    }
  }
);

let routeNavigator;

class Header extends Component {
  render() {
    return <View style={{
      height: 65,
      backgroundColor: 'white',
      flexDirection: 'row',
      justifyContent: 'center',
      shadowColor: '#595959',
      shadowOffset: {width: 0, height: 2},
      shadowRadius: 8,
      shadowOpacity: 0.3
    }}>
      <View style={{flex: 1}}>
        {this.props.backButton ? <TouchableOpacity
          style={{
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
          onPress={() => {
            Keyboard.dismiss();
            this.props.navigation.goBack();
          }}
        >
          <Image
            style={{height: 20, width: 20}}
            source={require("../../assets/backIcon.png")}
            resizeMode={'contain'}
          />
        </TouchableOpacity> : null}
      </View>
      <View style={{flex: 6, paddingVertical: 10, alignItems: 'center', justifyContent: 'center'}}>
        {this.props.title ? <Text
            style={{
              fontSize: 18,
              color: "#e55595"
            }}
          >{this.props.title}</Text> :
          <Image
            style={{resizeMode: "contain", flex: 1}}
            source={require("../../assets/petluvs.png")}
            resizeMode={'contain'}
          />
        }
      </View>
      <TouchableOpacity
        style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
        onPress={() => {
          this.props.navigation.navigate('Notifications')
        }}
      >
        <Image
          source={require("../../assets/notificationIcon.png")}
          style={{flex: 1, height: 24, width: 24, tintColor: this.props.notificationsCount > 0 ? '#e55595' : 'gray'}}
          resizeMode={'contain'}
        />
        {
          this.props.notificationsCount > 0
            ?
            <View style={{height: 20, width: 20, borderRadius: 10, backgroundColor: 'red', alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 5, right: 5}}>
              <Text style={{color: 'white', fontSize: 16}}>{this.props.notificationsCount}</Text>
            </View>
            :
            null
        }
      </TouchableOpacity>
    </View>
  }
}

let HeaderContainer = connect(state => ({
  backButton: state.home.backButton,
  title: state.home.headerTitle,
  notificationsCount: state.home.notificationsCount
}))(Header);

export default routeNavigator = createStackNavigator(
  {
    tabBar: {
      screen: TabBar,
      headerMode: "none",
      defaultNavigationOptions: {header: null, gesturesEnabled: false}
    },
    AddPet: {
      screen: AddPet
    },
    Website: {
      screen: Website
    },
    EditProfile: {screen: EditProfile},
    PlaydateDetails: {screen: PlaydateDetails},
    PlaydateEdit: {screen: PlaydateEdit},
    PlaydateCreation: {screen: PlaydateCreation},
    CreateBreederEvent: {screen: CreateBreederEvent},
    SearchDetails: {screen: SearchDetails},
    SaveServiceProviderInfo: {screen: SaveServiceProviderInfo},
    Services: {screen: ServicesScreen},
    PetProfile: {screen: PetProfile},
    Notifications: {screen: Notifications},
    PlaydateItem: {screen: PlaydateItem}
  },
  {
    defaultNavigationOptions: ({navigation}) => {
      return {
        headerVisible: true,
        header: <HeaderContainer navigation={navigation}/>,
        gesturesEnabled: true
      }
    }
  }
);
