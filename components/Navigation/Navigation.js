import {createSwitchNavigator, createAppContainer} from 'react-navigation';

import MainNavigation from './MainNavigation';
import AuthNavigator from './AuthNavigation';
import AuthLoadingScreen from '../../screens/AuthLoadingScreen';
import OnBoardingNavigator from './OnBordingNavigation';

const navigationContainer = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: MainNavigation,
    Auth: AuthNavigator,
    OnBoarding: OnBoardingNavigator
  },
  {
    initialRouteName: 'AuthLoading'
  }
);

export default createAppContainer(navigationContainer);
