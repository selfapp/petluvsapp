import React from "react";
import { Text, View, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import StarRating from "react-native-star-rating";

import GeneralStyles from "../Styles/General";

class SearchListingItem extends React.Component {
  render() {
    return (
      <View style={styles.SearchListingItemContainer}>
        <View style={{ flex: 1 }}>
          <Text style={GeneralStyles.h1}>{this.props.Name}</Text>
          <Text style={{ opacity: 0.5 }}>
            { this.props.Type.toUpperCase()} {this.props.Distance} { this.props.ClosingTime ? "Open" : "Close" }
          </Text>
        </View>
        <View style={{marginTop: 10, marginRight: 10}}>
          <StarRating
            disabled
            maxStars={5}
            rating={this.props.Rating}
            starSize={15}
            starColor={"#56C1EF"}
            fullStarColor={"#56C1EF"}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  SearchListingItemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
    backgroundColor: "white",
    marginBottom: 2,
    shadowColor: "#451B2D",
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.18,
    shadowRadius: 7
  },
  Rating: {
    color: "#56C1EF",
    fontSize: 16,
    textAlign: "right"
  }
});

export default SearchListingItem;
