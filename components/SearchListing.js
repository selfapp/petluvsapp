import React from "react";
import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
  Text
} from "react-native";
import {withNavigation} from "react-navigation";

import SearchListingItem from "../components/SearchListingItem";

const {width} = Dimensions.get("window");

class SearchListing extends React.Component {

  onNavigate = item => {
    this.props.navigation.navigate("SearchDetails", {
      placeId: item.placeId,
      inAppServiceProvider: item.inAppServiceProvider ? true : false,
      serviceProviderId: item.serviceProviderId,
      searchKey: this.props.searchKey,
      distance: item.Distance
    })
  };

  render() {
    return (
      <View style={{flex: 1}}>
        {(this.props.data.length > 0) ?
          <FlatList
            data={this.props.data}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => this.onNavigate(item)}>
                <SearchListingItem
                  Name={item.name}
                  Type={item.Type}
                  Distance={item.Distance}
                  ClosingTime={item.ClosingTime}
                  Rating={item.Rating}
                  inAppServiceProvider={item.inAppServiceProvider}
                />
              </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
            initialNumToRender={30}
            extraData={this.props.data}
            onRefresh={this.props.handleRefresh}
            refreshing={this.props.refreshing}
          />
          :
          <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
            <Image
              style={{resizeMode: "contain"}}
              source={require("./../assets/petluvs.png")}
            />
            <Text style={{ color: 'black', textAlign: 'center', marginHorizontal: 30, marginTop: 20 }}>Find local server providers by selecting a service above</Text>
          </View>
        }
      </View>
    );
  }
}

export default withNavigation(SearchListing);
