import React from "react";
import {
  Text,
  StyleSheet,
  TouchableHighlight
} from "react-native";

class SearchListingCategoryItem extends React.Component {
  state = {
    selectedCategory: "Pet Trainers"
  };

  onCategoryPress = async selectedCategory => {
    await this.setState({ selectedCategory });
    this.props.categorySelectionHandler(this.props);
  };

  render() {
    return (
        <TouchableHighlight
            onPress={() => this.onCategoryPress(this.props.Name)}
            style={[styles.CategoryButton, { opacity: this.props.selectedCategory === this.props.Name ? 0.9 : 0.3 }]}
            activeOpacity={1}
        >
          <Text style={{ color: "white" }}>{this.props.Name}</Text>
        </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  CategoryButton: {
    backgroundColor: "#EF5595",
    paddingVertical: 6,
    paddingHorizontal: 16,
    marginRight: 12,
    marginHorizontal: 10,
    borderRadius: 4
  }
});

export default SearchListingCategoryItem;
