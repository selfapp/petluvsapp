import React from 'react';
import {connect} from "react-redux";
import {
    Share,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
    Linking, Modal, View, Text, Dimensions,
    StyleSheet
} from 'react-native';

import { Video } from 'expo-av';
import { MaterialIcons, Octicons } from '@expo/vector-icons';
import {Row, Column as Col, ScreenInfo, Grid} from 'react-native-responsive-grid';

import ConfirmationModal from "../components/UI/ConfirmationModal";
import { shareMedia, homeData } from "../store/actions";

const sizes = {sm: 50, md: 25, lg: 12.5, xl: 10};
const { height, width } = Dimensions.get("window");

const layout = (state, media, canShare) => {

    const numCols = Math.floor(100 / sizes[ScreenInfo().mediaSize])
    const numRows = Math.ceil(  (media != null) ? media.length / numCols : 0)
    const colWidth = state.layout.grid ? state.layout.grid.width / numCols : 0

    let layoutMatrix = [], layoutCols = []

    for (let col = 0; col < numCols; col++) {
        layoutMatrix.push([])
        for (let row = 0, i = col; row < numRows; row++, i += numCols) {
            if (media != null && media[i])
                layoutMatrix[col].push(
                    <Item
                        key={i}
                        id={i}
                        name={media[i].mediaName}
                        url={media[i].url}
                        mediaId={media[i]._id}
                        type={media[i].mediaType}
                        height={300}
                        width={300}
                        margin={15}
                        colWidth={colWidth}
                        state={state}
                        canShare={canShare}
                    />
                )
        }
        layoutCols.push(
            <Col
                key={col}
                smSize={state.layout.grid ? sizes.sm : 0}
                mdSize={state.layout.grid ? sizes.md : 0}
                lgSize={state.layout.grid ? sizes.lg : 0}
                xlSize={state.layout.grid ? sizes.xl : 0}
            >
                {layoutMatrix[col]}
            </Col>
        )
    }

    return layoutCols
};

class Item extends React.Component{

    constructor(props){
        super(props)
        this.state ={
            isVideoFullScreenModel: false,
            mute: false,
            shouldPlay: true,
            showConfirmModal: false,
            mediaId: null
        }
    }
    handlePlayAndPause = () => {
        this.setState(prevState => ({
            shouldPlay: !prevState.shouldPlay
        }));
    }

    handleVolume = () => {
        this.setState(prevState => ({
            mute: !prevState.mute,
        }));
    }

    onMediaShare = async () => {
        let res = await shareMedia(this.state.mediaId);
        if (res) {
            await this.setState({ showConfirmModal: false });
        }
    }

    onHideConfirmationModal = () => {
        this.setState({ showConfirmModal: false })
    }

    render(){
        if (!this.props.colWidth) return null;

        return (

            <Row
                style={{
                    backgroundColor: 'white',
                    margin: 5, borderRadius: 5,
                }}
            >
                <ConfirmationModal 
                    visible={this.state.showConfirmModal} 
                    onCancel={this.onHideConfirmationModal} 
                    onConfirm={this.onMediaShare}
                />

                <Col fullWidth>
                        <TouchableOpacity
                            onPress = {() => this.setState({isVideoFullScreenModel: true})}
                        >
                        {(this.props.type === 'image')? (
                            <ImageBackground
                                source={{uri: this.props.url}}
                                style={{
                                    width: this.props.colWidth,
                                    height: this.props.height + ((this.props.colWidth - this.props.width) * this.props.height / this.props.width),
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}

                            />):(
                            <Video
                                source={{ uri: this.props.url }}
                                rate={1.0}
                                volume={1.0}
                                isMuted={false}
                                resizeMode="cover"
                                shouldPlay={false}
                                isLooping
                                // useNativeControls
                                style={{ width: 300, height: this.props.height + ((this.props.colWidth - this.props.width) * this.props.height / this.props.width) }}
                            />)}
                        </TouchableOpacity>
                        
                        {this.props.canShare ? 
                            <TouchableOpacity
                                style={{
                                    alignItems: 'flex-end',
                                    justifyContent: 'flex-end',
                                    height: 30,
                                    width: 30,
                                    borderTopLeftRadius: 35,
                                    backgroundColor: '#e55595',
                                    opacity: .7,
                                    position: 'absolute',
                                    right: 0,
                                    bottom: 0
                                }}
                                onPress={() => this.setState({ showConfirmModal: true, mediaId: this.props.mediaId } )}
                                // onPress={() => Linking.openURL('mailto:Admin@petluvs.com?subject=Report about it&body=body')}
                            >
                                <Image
                                    style={{marginRight: 5, marginBottom: 5}}
                                    source={require('./../assets/share.png')}
                                />
                            </TouchableOpacity>
                            : null 
                        }
                    {/*</TouchableOpacity>*/}

                </Col>

{/*Media full screen*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.isVideoFullScreenModel}
                >
                    <TouchableOpacity style={{ backgroundColor:'#00000030', flex: 1 }}
                                      onPress={() => this.setState({isVideoFullScreenModel:false})}
                    >
                        <View style={{ backgroundColor:'white', borderRadius: 10, marginHorizontal: 40, marginVertical: (height - (width - 80) * 1.5)/2}}>

                            <View style={{height: 50, alignItems:'center', justifyContent:'flex-end', flexDirection:'row'}}>
                                {/*<Text> {this.props.name}</Text>*/}
                            <TouchableOpacity style={{ justifyContent:'flex-end', alignItems:'flex-end', marginRight:10}}
                                              onPress={() => this.setState({isVideoFullScreenModel:false})}
                            >
                                <Image
                                    source={ require('./../assets/cross.png')}
                                />
                            </TouchableOpacity>
                            </View>
                            {(this.props.type === 'image')? (
                                <ImageBackground
                                    source={{uri: this.props.url}}
                                    style={{
                                        width: width - 80,
                                        height: (width - 80) * 1.5,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}

                                />):(
                                    <View>
                                    <Video
                                    source={{ uri: this.props.url }}
                                    rate={1.0}
                                    volume={1.0}
                                    isMuted={this.state.mute}
                                    resizeMode="cover"
                                    shouldPlay={this.state.shouldPlay}
                                    isLooping
                                    style={{ width: width - 80, height: (width - 80) * 1.5 }}
                                    />

                                        <View style={styles.controlBar}>
                                            <MaterialIcons
                                                name={this.state.mute ? "volume-mute" :
                                                    "volume-up"}
                                                size={45}
                                                color="white"
                                                onPress={this.handleVolume}
                                            />
                                            <MaterialIcons
                                                name={this.state.shouldPlay ? "pause" :
                                                    "play-arrow"}
                                                size={45}
                                                color="white"
                                                onPress={this.handlePlayAndPause}
                                            />
                                        </View>
                                    </View>
                                        )}
                        </View>
                    </TouchableOpacity>

                </Modal>

            </Row>
        )
    }
}

connect({ homeData })(Item);

const styles = StyleSheet.create({

    controlBar: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0, 0, 0, 0.5)",
    }
});

export const SearchImages = (props) => (
    <Grid>{({state, setState}) => (
        <Row fullHeight>
            <ScrollView removeClippedSubviews={true}>
                <Row>
                    {layout(state, props.media, props.canShare)}
                </Row>
            </ScrollView>
        </Row>
    )}
    </Grid>)

const data = [
    {
        name: 'Bella',
        url: 'https://placeimg.com/640/640/nature',
        pixelHeight: 354,
        pixelWidth: 236,
        mediaType:'Image'
    },
    {
        name: 'Max',
        url: 'https://placeimg.com/640/640/people',
        pixelHeight: 157,
        pixelWidth: 236,
        mediaType:'Image'
    },
    {
        name: 'Archie',
        url: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
        pixelHeight: 359,
        pixelWidth: 336,
        mediaType:'Video'

    },
    {
        name: 'Buddy',
        url: 'https://placeimg.com/640/640/beer',
        pixelHeight: 289,
        pixelWidth: 236,
        mediaType:'Image'

    },
    {
        name: 'Gaurav',
        url: 'https://placeimg.com/640/640/animals',
        pixelHeight: 326,
        pixelWidth: 236,
        mediaType:'Image'

    },
    {
        name: 'Abc',
        url: 'https://placeimg.com/640/640/beer',
        pixelHeight: 354,
        pixelWidth: 236,
        mediaType:'Image'

    },
]


export default SearchImages;
