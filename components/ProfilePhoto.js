import React from "react";
import { Image, View, StyleSheet } from "react-native";

class ProfilePhoto extends React.Component {
  render() {
    return (
      <View style={styles.Circle}>
        <Image
          style={{flex:1,width:40,height:40}}
          source={{
            uri: this.props.photo
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Circle: {
    height: 40,
    width: 40,
    borderRadius: 20,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
  },
  ProfilePhoto: {
    resizeMode: "contain"
  }
});

export default ProfilePhoto;
