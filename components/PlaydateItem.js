import React from "react";
import {connect} from "react-redux";
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Share
} from "react-native";
import {withNavigation} from "react-navigation";

import {
  fetchPlaydate,
  fetchSinglePlaydate,
  joinPlaydate,
  leavePlaydate
} from "../store/actions";
import ProfilePhoto from "../components/ProfilePhoto";
import GeneralStyles from "../Styles/General";
import {playdateDurationCalc} from "../utility/playdateDurationCal";
import AbbreviatedAttendees from "./AbbreviatedAttendees";

class PlaydateItem extends React.Component {
  componentDidMount() {
    // this.props.fetchPlaydate(this.props.playdateId);
  }

  timeLeftLatest = (startTime, endTime) => {
    const timeLeft = playdateDurationCalc(startTime, endTime);
    return timeLeft;
  }

  _onClick() {
    if (this.props.navigator && this.props.navigator.pop) {
      this.props.navigator.pop();
    } else {
      console.warn(
        "Warning: _onBack requires navigator property to function. Either modify the onBack prop or pass a navigator prop"
      );
    }
  }

  detailsHandler = playdateId => (
    <TouchableOpacity
      onPress={() =>
        this.props.navigation.navigate("PlaydateDetails", {
          playdateId,
          item: this.props.item,
          userJoining: this.props.userJoining,
          playdateOwnerId: this.props.playdateOwnerId
        })
      }
      style={{
        backgroundColor: "#EF5595",
        paddingVertical: 8,
        paddingHorizontal: 25,
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Text style={{color: "white", fontSize: 16}}>View</Text>
    </TouchableOpacity>
  );

  render() {
    let isUserJoined = false;
    let isJoinShow = true;
    if (this.props.userJoining) {
      isUserJoined = this.props.userJoining.some(
        el => el === this.props.authInfo.id
      );
    }
    if (this.props.isJoinShow) {
      isJoinShow = false;
    }

    return (
      <View style={[GeneralStyles.Card, {marginBottom: 24}]}>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("PetProfile", {
              tab: 0,
              _id: this.props.item.record.pets[0]._id,
              static: (this.props.item.record.userId._id === this.props.authInfo.id) ? false : true,
              species: this.props.item.record.pets[0].species
            })
          }
          style={styles.Header}
        >
          <ProfilePhoto photo={this.props.photo}/>

          <View style={{paddingLeft: 15, flex:1 }}>
            <View style={{ flexDirection:'row', justifyContent:'space-between', flex:1}}>
              <View style={{ justifyContent:'center', flex:1}}>
            <Text style={GeneralStyles.h1}>
              {this.props.Name}
            </Text>
              </View>
              <View
                style={
                  styles.TimeBubble
                }
              >
                <Text style={styles.TimeText}>{this.timeLeftLatest(this.props.StartTime, this.props.EndTime)}</Text>
              </View>
            </View>



            <Text style={{opacity: 0.5}}>Started a playdate</Text>
          </View>

        </TouchableOpacity>
        <View style={styles.PhotoSection}>
          <TouchableOpacity
            style={{width: "100%", height: "100%"}}
            onLongPress={() => {
              Share.share({
                title: "Shared via Petluvs",
                message: "Shared via Petluvs",
                url:
                  this.props.locPhotos && this.props.locPhotos.length > 0
                    ? {uri: this.props.locPhotos[0]}
                    : require("../assets/LocationPhoto1.png")
              });
            }}
          >
            <ImageBackground
              resizeMode="cover"
              style={styles.LocationPhoto}
              source={
                this.props.locPhotos && this.props.locPhotos.length > 0
                  ? {uri: this.props.locPhotos[0]}
                  : require("../assets/LocationPhoto1.png")
              }
            >
              <Text style={styles.LocationText}>{this.props.Location}</Text>
              <Text style={styles.MilesAwayText}>
                {this.props.MilesAway} Miles Away
              </Text>
            </ImageBackground>
          </TouchableOpacity>
        </View>
        <View style={styles.ActionSection}>
          <AbbreviatedAttendees pets={this.props.item.record.pets}/>
          {isJoinShow ? this.detailsHandler(this.props.playdateId) : null}
        </View>      
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.home.data,
  loading: state.home.loading || state.user.loading,
  user: state.user.user,
  authInfo: state.auth.authInfo,
  playdate: state.playDates.playdate
});

const styles = StyleSheet.create({
  Header: {
    flex:1,
    padding: 10,
    flexDirection: "row"
  },
  TimeBubble: {
    backgroundColor: "#E8ACB7",
    paddingHorizontal: 5,
    paddingVertical: 5,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  TimeText: {
    color: "#fff",
    fontSize: 12,
    fontWeight: "bold"
  },
  PhotoSection: {
    alignSelf: "stretch",
    height: 200,
    alignItems: "stretch"
  },
  LocationPhoto: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: "flex-end",
    padding: 20
  },
  LocationText: {
    fontSize: 22,
    color: "#FFFFFF",
    fontWeight: "600"
  },
  MilesAwayText: {
    opacity: 0.75,
    fontSize: 18,
    color: "#FFFFFF"
  },
  ActionSection: {
    padding: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    // alignItems: "flex-end"
  }
});

export default withNavigation(
  connect(
    mapStateToProps,
    {
      joinPlaydate,
      leavePlaydate,
      fetchPlaydate,
      fetchSinglePlaydate
    }
  )(PlaydateItem)
);
