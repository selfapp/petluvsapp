import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ImageBackground,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";

const height1 = Dimensions.get("window").height;
const width1 = Dimensions.get("window").width;

export default class Pillpopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: this.props.showModal
    };
  }

  render() {
    const navigate = this.props.navigation.navigate;
    return (
      <Modal
        animationType="fade"
        transparent
        visible={this.state.showModal }
      >
        <TouchableWithoutFeedback
          onPress={() => {
            this.setState({
              showModal: !this.state.showModal
            });
          }}
        >
          <View
            style={{
              backgroundColor: "#00000030",
              // opacity: 0.3,
              alignItems: "center",
              justifyContent: "center",
              flex: 1
            }}
          >
            <TouchableWithoutFeedback
              // {/*>*/}
              //s {/*<View*/}
              onPress={() => {
                this.setState({
                  showModal: !this.state.showModal
                });
              }}
              style={{
                position: "absolute",
                top: 0,
                right: 0,
                height: height1,
                width,
                backgroundColor: "#00000050",
                alignItems: "center",
                justifyContent: "flex-start",
                flex: 1
              }}
            >
              <View
                style={{
                  // flex: 1,
                  height: 250,
                  // top: 50,
                  backgroundColor: "#ffffff",
                  width: width - 100,
                  borderRadius: 15,
                  marginHorizontal: 50
                  // marginTop: 100
                }}
              >
                {/*{this.state.showEditReview ? (*/}
                <View>
                  {/*<View*/}
                  {/*style={{*/}
                  {/*flexDirection: "row",*/}
                  {/*marginTop: 10,*/}
                  {/*marginRight: 10,*/}
                  {/*marginBottom: 10,*/}
                  {/*marginHorizontal: 25,*/}
                  {/*justifyContent: "center",*/}
                  {/*alignItems: "center"*/}
                  {/*// backgroundColor: 'red'*/}
                  {/*}}*/}
                  {/*>*/}
                  {/*<Text*/}
                  {/*style={{*/}
                  {/*fontSize: 15*/}
                  {/*}}*/}
                  {/*>*/}
                  {/*Please rate{" "}*/}
                  {/*</Text>*/}
                  {/*<StarRating*/}
                  {/*disabled={false}*/}
                  {/*maxStars={5}*/}
                  {/*rating={this.state.starCount}*/}
                  {/*selectedStar={rating =>*/}
                  {/*this.onStarRatingPress(rating)*/}
                  {/*}*/}
                  {/*starSize={25}*/}
                  {/*starColor={"#fff"}*/}
                  {/*fullStarColor={"#EF5595"}*/}
                  {/*/>*/}
                  {/*</View>*/}

                  {/*<TextInput*/}
                  {/*name={"UserReview"}*/}
                  {/*fontSize={14}*/}
                  {/*style={styles.TextInput}*/}
                  {/*placeholder="Enter your review(optional)"*/}
                  {/*placeholderTextColor="gray"*/}
                  {/*multiline*/}
                  {/*showWarning*/}
                  {/*autoCorrect={false}*/}
                  {/*autoCapitalize="none"*/}
                  {/*selectionColor="gray"*/}
                  {/*value={this.state.review}*/}
                  {/*onChangeText={text => this.setState({ review: text })}*/}
                  {/*underlineColorAndroid="transparent"*/}
                  {/*/>*/}
                  <View
                    style={[
                      styles.TextInput,
                      ,
                      {
                        alignItems: "center",
                        justifyContent: "center",
                        height: 120,
                        marginTop: 10
                      }
                    ]}
                  >
                    <Text
                      style={[
                        // styles.TextInput,
                        {
                          fontSize: 15
                        }
                      ]}
                    >
                      Are you sure you want to share this review in Home page.
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={[
                      styles.saveButton,
                      { alignSelf: "center", width: 200 }
                    ]}
                    onPress={() => {
                      this.setState({
                        showModal: !this.state.showModal
                      });
                      this.props.shareReview(this.state.shareReviewid);
                    }}
                  >
                    <Text style={{ fontSize: 15, color: "#fff" }}>Yes</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{ marginBottom: 5, marginTop: 10 }}
                    // onPress={()=> {}}
                    onPress={() => {
                      this.setState({
                        showModal: !this.state.showModal
                      });
                    }}
                  >
                    <View style={{ alignSelf: "center" }}>
                      <Text style={{ fontSize: 15, color: "#000" }}>
                        Cancel
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                {/*)*/}
                // : null}
              </View>
              {/*</View>*/}
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({});
