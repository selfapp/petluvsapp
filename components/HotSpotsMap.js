import React from "react";
import { AppRegistry, StyleSheet, Text, View } from "react-native";

import MapView from "react-native-maps";

export default (HotSpotsMap = props => (
  <View style={{ flex: 1 }}>
    <MapView
      style={{ flex: 1 }}
      showsUserLocation
      region={{
        latitude: props.latitude,
        longitude: props.longitude,
        latitudeDelta: 0.2422,
        longitudeDelta: 0.2421
      }}
    >
      {props.items.map((marker, index) => {
        const coords = {
          latitude: marker.location.lat,
          longitude: marker.location.lng
        };

        return (
          <MapView.Marker
            key={index}
            coordinate={coords}
            image={props.markerIcon}
            title={marker.name}
            style = {{shadowColor: "black", shadowOffset: { height: 5}, shadowOpacity: 0.3,}}
            onPress={() =>
              props.navigation.navigate("SearchDetails", {
                businessName: marker.name,
                rating: marker.rating,
                servicesOffered: marker.types,
                description: marker.description,
                address: marker.location.address + marker.location.city,
                placeId: marker.place_id
              })
            }
          />
        );
      })}
    </MapView>
  </View>
));

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  },
  marker: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "white",
    borderWidth: 5,
    borderColor: "#4191AA"
  }
});
