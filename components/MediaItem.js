import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ImageBackground
} from "react-native";
import { Video } from 'expo-av';

import GeneralStyles from "../Styles/General";
import ProfilePhoto from "./ProfilePhoto";

class MediaItem extends React.Component {
  render() {
    let mediaType = this.props.MediaType;
    return (
      <View style={[GeneralStyles.Card]}
      >
        <View style={{flexDirection: "row", padding: 10 }}>
          <ProfilePhoto
            photo={this.props.Photo}
          />
          <View style={{paddingLeft: 15}}>
            <Text style={GeneralStyles.h1}>{this.props.Name}</Text>
            <Text style={{opacity: 0.5}}> Shared the pic</Text>
          </View>
        </View>

        <View style={[styles.PhotoSection, {paddingBottom: 20 }]}>
          <TouchableOpacity
            style={{width: "100%", height: "100%"}}
          >
            { mediaType === "image"
              ? <ImageBackground
                resizeMode="cover"
                style={styles.LocationPhoto}
                source={
                  this.props.MediaUri 
                  ? {uri: this.props.MediaUri }
                  :require("../assets/LocationPhoto1.png")
                }
            ></ImageBackground>
            : <Video
                source={{ uri: this.props.MediaUri }}
                rate={1.0}
                volume={1.0}
                isMuted={false}
                resizeMode="cover"
                shouldPlay={false}
                isLooping
                useNativeControls
                style={{ width: "100%", height: "100%" }}
              />
            }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ReviewText: {
    color: "#8F858C"
  },
  LocationPhoto: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: "flex-end",
    padding: 20
  },
  PhotoSection: {
    alignSelf: "stretch",
    height: 200,
    alignItems: "stretch"
  }
});

export default MediaItem;
