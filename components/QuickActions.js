/* eslint-disable prettier/prettier */
import React from "react";
import {
  Image,
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  AsyncStorage,
  Linking,
} from "react-native";
import Icon from "react-native-vector-icons/Feather";

import * as petluvsConst from "../utility/utils";

class QuickActions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pressStatus: false,
      petProfile: false,
      breeder: true,
      enableServices: []
    };
    this.breeder = "";
    this.updateArray();
  }

  updateArray = async () => {
    try {
      const myArray1 = await AsyncStorage.getItem("@MySuperStore1:key");
      const enableServices = await AsyncStorage.getItem(
        JSON.parse(petluvsConst.ENABLE_SERVICES)
      );
      this.setState({enableServices});
      if (myArray1 !== null) {
        this.setState({breeder: JSON.parse(myArray1)});
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  onToggleActions = () => {
    this.setState({
      pressStatus: !this.state.pressStatus
    });
  };

  getItem = async () => {
    try {
      const myname = await AsyncStorage.getItem("breeder");

      this.setState({petProfile: myname});
    } catch (error) {
    }
  };

  componentDidMount() {
    this.getItem();
  }

  navigateToPetServices = () => {
    if (this.state.enableServices.indexOf("BREEDER") > -1) {
      this.props.navigation.navigate("CreateBreederEvent");
      this.onToggleActions();
    } else {
      this.props.navigation.navigate("Services");
      this.onToggleActions();
    }
  };

  iconText = () => {
    if (this.state.enableServices.indexOf("BREEDER") > -1) {
      return (
        <Text
          style={{
            color: "white",
            paddingTop: 50,
            position: "absolute",
            textAlign: "center",
            zIndex: 999,
            fontSize: 12
          }}
        >
          Create
          {"\n"}
          Breeder Event
        </Text>
      );
    }
    return (
      <Text
        style={{
          color: "white",
          paddingTop: 50,
          position: "absolute",
          textAlign: "center",
          zIndex: 999,
          fontSize: 12
        }}
      >
        Find
        {"\n"}
        Pet Services
      </Text>
    );
  };

  render() {
    return (
      <View
        style={{flex: 1, justifyContent: "flex-end", alignItems: "center"}}
      >
        <View
          style={{flex: 1, justifyContent: "flex-end", zIndex: 999}}
          pointerEvents="box-none"
        >
          <TouchableOpacity
            activeOpacity={1}
            style={
              styles.ActionButton
            }
          >
            <Icon
              name="plus"
              size={50}
              color="white"
              onPress={this.onToggleActions}
              style={
                this.state.pressStatus
                  ? [styles.ActionPlusActive]
                  : {}
              }
            />
          </TouchableOpacity>
        </View>
        <View
          onPress={this.onToggleActions}
          style={
            this.state.pressStatus
              ? [styles.ActionOverlay, styles.ActionOverlayActive]
              : {zIndex: 99}
          }
        />

        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate("PlaydateCreation");
            this.onToggleActions();
          }}
          style={
            this.state.pressStatus
              ? [styles.ActionItem, styles.ActionItem1]
              : styles.ActionDeactive
          }
        >
          <Image source={require("../assets/createPlaydateButton.png")}/>
          <Text
            style={{
              color: "white",
              paddingTop: 40,
              position: "absolute",
              zIndex: 999,
              fontSize: 12,
            }}
          >
            Create Playdate
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            {
              this.props.navigation.navigate("AddPet");
              this.onToggleActions();
            }
          }}
          style={
            this.state.pressStatus
              ? [styles.ActionItem, styles.ActionItem3]
              : styles.ActionDeactive
          }
        >
          <Image
            source={require("../assets/addPetIcon.png")}
          />
          <Text
            style={{
              color: "white",
              paddingTop: 40,
              position: "absolute",
              fontSize: 12,
              zIndex: 999
            }}
          >
            Add Pet
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            {
              this.navigateToPetServices();
            }
          }}
          style={
            this.state.pressStatus
              ? [styles.ActionItem, styles.ActionItem2]
              : styles.ActionDeactive
          }
        >
          <Image
            source={require("../assets/createBreederEventButton.png")}
          />
          {this.iconText()}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            {
              this.onToggleActions();
              Linking.openURL('mailto:admin@petluvs.com?subject=Report a Problem')
            }
          }}
          style={
            this.state.pressStatus
              ? [styles.ActionItem, styles.ActionItem4]
              : styles.ActionDeactive
          }
        >
          <Image
            source={require("../assets/feedback.png")}
          />
          <Text
            style={{
              color: "white",
              paddingTop: 40,
              position: "absolute",
              fontSize: 12,
              zIndex: 999
            }}
          >
            Help
          </Text>
        </TouchableOpacity>




      </View>
    );
  }
}

const styles = StyleSheet.create({
  ActionButton: {
    backgroundColor: "#EF5595",
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 0,
    shadowColor: "#EF5595",
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.58,
    shadowRadius: 10,
    zIndex: 99,
    alignSelf: "center"
  },
  ActionButtonActive: {
    height: 110,
    justifyContent: "flex-start",
    paddingTop: 17
  },
  ActionPlusActive: {
    transform: [{rotateX: "30deg"}, {rotateZ: "0.785398rad"}],
    overflow: "visible"
  },
  ActionOverlay: {
    flex: 1,
    width: 1000,
    height: 1000
  },
  ActionOverlayActive: {
    flex: 1,
    position: "absolute",
    backgroundColor: "rgba(0, 0, 0, 0.5)"
  },
  ActionDeactive:{
    // width: 0,
    // height: 0,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    display: "none"
  },
  ActionItem: {
    width: 0,
    height: 0,
    backgroundColor: "white",
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    zIndex: 999,
    display: "none"
  },
  ActionItem1: {
    display: "flex",
    transform: [{translateX: -100}, {translateY: -100}]
  },
  ActionItem2: {
    display: "flex",
    transform: [{translateX: -100}, {translateY: -220}]
  },
  ActionItem3: {
    display: 'flex',
    transform: [{translateX: 100}, {translateY: -220}]
  },
  ActionItem4: {
    display: "flex",
    transform: [{translateX: 100}, {translateY: -100}]
  }
});

export default QuickActions;
