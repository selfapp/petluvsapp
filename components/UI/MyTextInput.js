import React from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";

const MyTextInput = props => {
  const { input, meta, ...inputProps } = props;
  const { invalid, touched, error, pristine, visited } = meta;

  return (
    <View style={styles.ViewShadow}>
      <TextInput
        {...inputProps}
        defaultValue={props.defaultValue}
        onBlur={input.onBlur}
        onFocus={input.onFocus}
        value={props.defaultValue}
        onChangeText={input.onChange}
      />
      {(touched || pristine === false || visited) &&
      invalid &&
      props.showWarning !== false ? (
        <View style={{ marginTop: 5 }}>
          <View>
            <Text style={styles.validationAddress}>{error}</Text>
          </View>
        </View>
      ) : !props.showWarning ? (
        props.saveValueinState(props.input.value)
      ) : null}
    </View>
  );
};
const styles = StyleSheet.create({
  ViewShadow: {
    // flexDirection: "row",
    marginHorizontal: 15,
    borderColor: "#000",
    justifyContent: "center",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 5,
    shadowColor: "#e55595"
  },
  validation: {
    fontSize: 10,
    color: "red"
  },
  validationAddress: {
    position: "absolute",
    bottom: -10,
    fontSize: 10,
    color: "red"
  }
});

export default MyTextInput;
