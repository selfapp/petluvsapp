import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  ActivityIndicator
} from "react-native";

const {height} = Dimensions.get("window");

const ModalSpinner = props => {
  
  return (
    <View>
      <Modal
        animationType="fade"
        transparent
        visible={props.visible}
      >
        <TouchableOpacity
          style={{
            backgroundColor: "#00000030",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        >
          <TouchableWithoutFeedback>
            <View
              style={{
                height: 50,
                width: 50,
                backgroundColor: "#fff",
                borderRadius: 10,
              }}
              opacity={0.9}
            >
            <View style={styles.activityContainer}>
                <ActivityIndicator
                animating
                color="#0000ff" // color of your choice
                size="large"
                style={styles.activityIndicator}
                />
            </View>
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
    saveButton: {
        height: 50,
        width: "100%",
        borderRadius: 10,
        backgroundColor: "#e55595",
        margin: 10
    },
    saveButtonContainer: {
        marginHorizontal: 15,
        alignItems: "center",
        justifyContent: "center",
        height: "10%",
        marginBottom: 10,
        marginTop: 10
    },
    activityContainer: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "pink"
    }

});

export default ModalSpinner;
