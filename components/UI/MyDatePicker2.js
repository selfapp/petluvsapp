import React from "react";
import {View, StyleSheet} from "react-native";
import DatePicker from "react-native-datepicker";

const dateFormat = "MM-DD-YYYY hh:mm A";

const MyDatePicker2 = props => {
  const {
    input: {onChange, value},
    meta,
    ...inputProps
  } = props;

  const formStates = [
    "active",
    "autofilled",
    "asyncValidating",
    "dirty",
    "invalid",
    "pristine",
    "submitting",
    "touched",
    "valid",
    "visited"
  ];

  return (
    <View style={styles.ViewShadow}>
      <DatePicker
        {...inputProps}
        showTime={{user12hours: true}}
        date={props.date}
        onDatetimePicked={value => onChange(value)}
        value={value}
        datevalue={value}
        format={dateFormat}
        onDateChange={datevalue => {
          props.saveDateinState(datevalue);
        }}
        minDate={props.minDate}
        // minuteInterval={30}
      />
      <View style={{marginTop: 5}}>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  ViewShadow: {
    marginHorizontal: 15,
    justifyContent: "center",

  },
  validation: {
    fontSize: 10,
    color: "red"
  }
});

export default MyDatePicker2;
