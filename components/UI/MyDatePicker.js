import React, { Component } from "react";
import { View, Text, TextInput, Image, StyleSheet } from "react-native";
import DatePicker from "react-native-datepicker";

const MyDatePicker = props => {
  const {
    input: { onChange, value },
    meta,
    ...inputProps
  } = props;
  const formStates = [
    "active",
    "autofilled",
    "asyncValidating",
    "dirty",
    "invalid",
    "pristine",
    "submitting",
    "touched",
    "valid",
    "visited"
  ];

  return (
    <View style={styles.ViewShadow}>
      <DatePicker
        {...inputProps}
        // onDatetimePicked={(value )=> onChange(value)}
        // onDateChange={(date) => onChange(moment(new Date(date)).format("MM/DD/YYYY"))}
        // selected={value ? moment(value) : null}
        date={props.date}
        // value={props.date}
        datePicked={value => onChange(value)}
        value={value}
        datevalue={value}
        format="YYYY-MM-DD"
        onDateChange={datevalue => {
          props.dateHandler(datevalue);
        }}
      />
      <View style={{ marginTop: 5 }}>
        {props.date ? null : ( // !(props.meta.pristine&&props.meta.valid)
          <View>
            <Text style={styles.validation}>Please enter a date!!</Text>
          </View>
        )}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  ViewShadow: {
    // flexDirection: "row",
    marginHorizontal: 15,
    borderColor: "#000",
    justifyContent: "center",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.18,
    shadowRadius: 1,
    elevation: 1,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    shadowColor: '#451B2D',
  },
  validation: {
    marginLeft: 10,
    fontSize: 10,
    color: "red"
  }
});

export default MyDatePicker;
