import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Dimensions
} from "react-native";

const {height} = Dimensions.get("window");

const ConfirmationModal = props => {

  onCancel = () => {
    props.onCancel()
  }

  onConfirm = () => {
    props.onConfirm();
  }

  let modalText = "Are you sure you want to share this Media?";
  console.log(height);
  
  return (
    <View>
      <Modal
        animationType="slide"
        transparent
        visible={props.visible}
      >
        <TouchableOpacity
          style={{
            backgroundColor: "#00000030",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
          onPress={() => onCancel()}
        >
          <TouchableWithoutFeedback>
            <View
              style={{
                height: 250,
                width: 300,
                backgroundColor: "#fff",
                borderRadius: 10,
              }}
              opacity={0.9}
            >
              <View
                style={{
                  margin: 5,
                  alignItems: "center",
                  justifyContent: "center",
                  marginVertical: 15,
                  shadowOffset: { width: 0, height: 2 },
                  shadowOpacity: 0.2,
                  shadowRadius: 5,
                  elevation: 5,
                  shadowColor: "#000"
                }}
              >
                <Image
                  style={{
                    resizeMode: "contain",
                    height: 30,
                    marginBottom: 10
                  }}
                  source={require("../../assets/petluvs.png")}
                />
              </View>
              <View style={{ padding: 10 }}>
                <Text>{modalText}</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View
                  style={[
                    styles.saveButtonContainer,
                    {
                      height: 60,
                      flex: 1,
                      marginHorizontal: 15,
                      paddingTop: 10
                    }
                  ]}
                >
                  <TouchableOpacity
                    style={[styles.saveButton]}
                    // disabled={this.props.loading}
                    onPress={() => onConfirm()}
                  >
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                        <Text style={{ color: "#fff", fontSize: 16 }}>YES</Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={[
                    styles.saveButtonContainer,
                    {
                      height: 60,
                      flex: 1,
                      marginHorizontal: 15,
                      paddingTop: 10
                    }
                  ]}
                >
                  <TouchableOpacity
                    style={[styles.saveButton, { backgroundColor: "gray" }]}
                    onPress={() => onCancel()}
                  >
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                      }}
                    >
                      <Text style={{ color: "#fff", fontSize: 16 }}>NO</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
    saveButton: {
        height: 50,
        width: "100%",
        borderRadius: 10,
        backgroundColor: "#e55595",
        margin: 10
    },
    saveButtonContainer: {
        marginHorizontal: 15,
        alignItems: "center",
        justifyContent: "center",
        height: "10%",
        marginBottom: 10,
        marginTop: 10
    }

});

export default ConfirmationModal;
