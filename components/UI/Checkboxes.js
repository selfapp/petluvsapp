import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";
import ModalDropdown from "react-native-modal-dropdown";
import RadioButton from "../../components/RadioButton";
import CheckBox from "react-native-checkbox";

const Checkboxes = props => {
  const {
    input: { onChange, value },
    meta,
    ...inputProps
  } = props;
  const formStates = [
    "active",
    "autofilled",
    "asyncValidating",
    "dirty",
    "invalid",
    "pristine",
    "submitting",
    "touched",
    "valid",
    "visited"
  ];

  return (
    <View>
      <CheckBox
        label="AKC Certificate Image"
        checkedImage={props.checkedImage}
        uncheckedImage={props.uncheckedImage}
        onChange={checked => props.checkStatus(checked)}
      />
      {/*{props.isChecked?props.checkStatus}*/}
    </View>
  );
};

export default Checkboxes;
