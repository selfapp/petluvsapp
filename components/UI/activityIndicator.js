import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

const Spinner = () => (
  <View style={styles.container}>
    {/*<ActivityIndicator size="large" color="#0000ff" />;*/}
    <ActivityIndicator
      animating
      color="#0000ff" // color of your choice
      size="large"
      style={styles.activityIndicator}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "pink"
  }
});

export default Spinner;
