import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import ModalDropdown from "react-native-modal-dropdown";

const Dropdowns = props => {
  const {
    input: { onChange, value },
    meta,
    ...inputProps
  } = props;

  const { invalid, error } = meta;

  return (
    <View>
    <View style={styles.ViewShadow}>
      <ModalDropdown
        {...inputProps}
        onSelect={ value => onChange(value)}
        value={value}
        dropdownStyle={props.dropdownStyle}
        dropdownTextStyle={props.dropdownTextStyle}
        textStyle={props.textStyle}
      />
      <Image
        style={styles.dropDown}
        source={require("./../../assets/dropdown.png")}
      />
    </View>
      <View style={{ marginTop: 5, marginLeft: 20 }}>
        {invalid ? (
          <View>
            <Text style={styles.validation}>{error}</Text>
          </View>
        ) : null}
      </View>

    </View>
  );
};

const styles = StyleSheet.create({
  ViewShadow: {
    marginHorizontal: 15,
    borderColor: "#000",
    justifyContent: "center",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 10,
    elevation: 5,
    shadowColor: "#e55595"
  },
  dropDown: {
    position: "absolute",
    right: 10,
    alignSelf: "center"
  },
  validation: {
    fontSize: 10,
    color: "red"
  }
});

export default Dropdowns;
