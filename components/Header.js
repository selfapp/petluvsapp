import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  Keyboard,
  View,
  Image,
  TouchableOpacity,
  Platform
} from "react-native";

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.header}>
         {this.props.isBackIcon === false ? (null) : (
        <TouchableOpacity
          style={styles.iconLeftStyle}
          onPress={() => {
            Keyboard.dismiss();
            // if (props.update) {
            //   props.navigation.state.params.pop();
            // }
            this.props.navigation.goBack();
          }}
        >
          <Image source={require("../assets/backIcon.png")} />
        </TouchableOpacity>
         )}
        <Text
          style={{
            height: 40,
            fontSize: 18,
            color: "#e55595",
            alignItems: "center"
          }}
        >
          {this.props.title}
        </Text>
        {!this.props.hideNotificationBell ? (
          <TouchableOpacity
            style={[styles.iconRightStyle, { bottom: 5 }]}
            onPress={() => {
              this.props.navigation.navigate("Notifications");
            }}
          >
            <Image source={require("../assets/notificationIcon.png")} />
          </TouchableOpacity>
        ) : null}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-end",
    height: 60,
    elevation: 5
  },
  title: {
    height: 40,
    fontSize: 20,
    color: "#e55595"
  },
  iconRightStyle: {
    position: "absolute",
    right: 10,
    bottom: Platform.OS === "ios" ? 2 : 0, //-3:0
    width: 50,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  iconLeftStyle: {
    position: "absolute",
    left: 0,
    bottom: 5,
    zIndex: 2,
    width: 50,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  }
});
