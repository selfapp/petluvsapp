import React from "react";
import { View, ScrollView } from "react-native";
import SearchListingCategoryItem from "../components/SearchListingCategoryItem";

class SearchListingCategories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCategory: "Animal Rescue Services"
    };
  }

  onCategorySelection = async category => {
    await this.setState({
      ...this.state,
      selectedCategory: category.Name
    });
    this.props.categorySelectionHandler(this.state.selectedCategory);
  };

  render() {
    return (
      <View>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{
            overflow: "visible",
            paddingBottom: 20,
            paddingRight: 60
          }}
        >
          <SearchListingCategoryItem
            Name="Animal Rescue Services"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Boarding"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Daycare"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Fosters"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Friendly Restaurants"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Groomers"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Sitters"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
          <SearchListingCategoryItem
            Name="Pet Trainers"
            selectedCategory={this.state.selectedCategory}
            categorySelectionHandler={category =>
              this.onCategorySelection(category)
            }
          />
        </ScrollView>
      </View>
    );
  }
}

export default SearchListingCategories;
