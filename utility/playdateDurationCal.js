/* eslint-disable prettier/prettier */
/* eslint-disable prefer-const */
import moment from "moment";
import momentTz from "moment-timezone";

export const playdateDurationCalc = (startDate, endDate) => {
    // console.log("/////////////////////")
    // console.log("startDate", startDate);
    // console.log("endDate", endDate)
    // console.log("/////////////////////'")

    let duration = "";

    const userTimezone = momentTz.tz.guess();

    let endTconvertToUsrTZ = moment(endDate);
    let startTconvertToUsrTZ = moment(startDate);

    let now = moment();

    // console.log("utz", userTimezone);
    // console.log("now", now.format('LLLL'));
    // console.log("startDate", startTconvertToUsrTZ.format('LLLL'))
    // console.log("endDate", endTconvertToUsrTZ.format('LLLL'))

    let dDiffToStartD = startTconvertToUsrTZ.diff(now, 'days');
    let hDiffToStartD = startTconvertToUsrTZ.diff(now, 'hours');
    let mDiffToStartD = startTconvertToUsrTZ.diff(now, 'minutes');
    
    let dDiff = endTconvertToUsrTZ.diff(now, 'days');
    let hDiff = endTconvertToUsrTZ.diff(now, 'hours');
    let mDiff = endTconvertToUsrTZ.diff(now, 'minutes');

    // console.log("startDat: ", dDiffToStartD, hDiffToStartD, mDiffToStartD);
    // console.log("EndDate: ", dDiff, hDiff, mDiff);

    if (dDiffToStartD > 0) {
        duration = `${dDiffToStartD} d left`;
    } else if (dDiffToStartD <= 0 && hDiffToStartD > 0) {
        duration = `${hDiffToStartD} h left`;
    } else if (mDiffToStartD > 0 && hDiffToStartD <= 0 && dDiffToStartD <= 0) {
        duration = `${mDiffToStartD} m left`;
    } else if ((dDiffToStartD <= 0 || hDiffToStartD <= 0 || mDiffToStartD <= 0) && mDiff >= 0) {
        duration = `Started`;
    } else if ((dDiffToStartD <= 0 || hDiffToStartD <= 0 || mDiffToStartD <= 0) && mDiff <= 0) {
        duration = `Completed`;
    }

    return duration;
}