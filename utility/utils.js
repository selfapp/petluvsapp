export const PETLUVS_TOKEN = "@petLuvsStore:token";
export const ENABLE_SERVICES = "@petLuvsStore:enableServices";
export const GOOGLE_API_KEY = "AIzaSyA2mlROpVOxhnKCjHDd2L7_Y-_IzQJ_zJk";
export const GOOGLE_PHOTOS_BASE_URL = "https://maps.googleapis.com/maps/api/place/photo";
export const HOTSPOT_CATEGORIES = [
    { title: "Pet Friendly Restaurants", icon: require("../assets/marker_sitter.png") },
    { title: "Dog Parks", icon: require("../assets/marker_trainer.png") },
];
export const PETLUVS_DEFAULT_IMAGE = require('../assets/petluvs_default.png');