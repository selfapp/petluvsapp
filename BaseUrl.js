import { AsyncStorage } from "react-native";

export const baseurl = "http://petluvsapi-env.qawwmrm5bk.us-east-1.elasticbeanstalk.com/api";
// export const baseurl = "http://192.168.86.100:3000/api"

export const Token = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch(token);
  return token;
};
export const token = Token();
