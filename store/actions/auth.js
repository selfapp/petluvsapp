import {AsyncStorage} from "react-native";
import * as Facebook from "expo-facebook";
import {Google} from "expo";
import jwtDecode from "jwt-decode";
import axios from "axios";
import {baseurl} from "./../../BaseUrl";
import * as petluvsConst from "../../utility/utils";
import {
  AUTH_FAILED,
  AUTH_SUCCESS,
  AUTH_START,
  AUTH_LOGOUT
} from "./actionTypes";

export const facebookLogin = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");

  if (token) {
    dispatch({type: AUTH_SUCCESS, token});
  } else {
    return doFacebookLogin(dispatch);
  }
};

const doFacebookLogin = async dispatch => {
  const {type, token} = await Facebook.logInWithReadPermissionsAsync(
    "634004726931460",
    {
      permissions: ["public_profile", "email"],
      behavior: "web"
    }
  );

  if (type === "cancel") {
    return dispatch({type: AUTH_FAILED});
  }

  // facebookLocalLogin
  dispatch({type: AUTH_START});
  const data = {
    access_token: token
  };
  try {
    const response = await fetch(`${baseurl}/auth/facebook/token`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    console.log(responseJson);
    if (response.status === 200) {
      let token = responseJson.data.token;
      // JwtTokenDecode(responseJson.user.token);
      AsyncStorage.setItem("token", token);
      AsyncStorage.setItem("loginType", "facebook");
      dispatch(authSuccess(token));
      return responseJson;
    } else {
      const errors = "Error while facebook login";
      dispatch(authFail(errors));
      return responseJson;
    }
  } catch (e) {
    const errorMsg = "Network Error! please try again";
    dispatch(authFail(errors));
    return { success: false, message: errorMsg, data: null};
  }
};

// Token decode
const JwtTokenDecode = token => {
  console.log("Token Decoding...");
  const decoded = jwtDecode(token);
  AsyncStorage.setItem(
    petluvsConst.ENABLE_SERVICES,
    JSON.stringify(decoded.enableServices)
  );
  return decoded;
};

// Auto Login
export const autoLogin = token => dispatch => {
  const p = new Promise(async (resolve, reject) => {
    try {
      //  const loginType = AsyncStorage.getItem('loginType');
      dispatch(authSuccess(token));
      resolve({ success: true });
    } catch (e) {
      reject("error");
    }
  });
  return p;
};

//Signup
export const signup = data => async dispatch => {
  dispatch({type: AUTH_START});
  try {
    // console.log(`${baseurl}/auth/signup`);
    const response = await fetch(`${baseurl}/auth/signup`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      let token = responseJson.data.token;
      AsyncStorage.setItem("token", token);
      AsyncStorage.setItem("loginType", "local");
      dispatch(authSuccess(token));
      return responseJson;
    } else {
      const errorMsg = responseJson.message;
      dispatch(authFail(errorMsg));
      return responseJson;
    }
  } catch (e) {
    const errorMsg = "Network Error!. Please try again.";
    dispatch(authFail(errorMsg));
    return { success: false, message: errorMsg, data: null};
  }
};

//Signin
export const signin = data => async dispatch => {
  dispatch({type: AUTH_START});
  try {
    const response = await fetch(`${baseurl}/auth/signin`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      let token = responseJson.data.token;
      dispatch(authSuccess(token));
      AsyncStorage.setItem("token", token);
      AsyncStorage.setItem("loginType", "local");
      return responseJson;
    } else {
      const errorMsg = responseJson.message;
      dispatch(authFail(errorMsg));
      return responseJson;
    }
  } catch (e) {
    console.log(e);
    const errorMsg = "Network Error! Please try again.";
    dispatch(authFail(errorMsg));
    return { success: false, message: errorMsg, data: null};
  }
};

// Logout funcionality
export const logout = () => {
  const p = new Promise(async (resolve, reject) => {
    try {
      // await AsyncStorage.clear();
      await AsyncStorage.removeItem('token');
      await AsyncStorage.removeItem('@petLuvsStore:isBreeder');
      resolve("success");
    } catch (e) {
      console.log("Error while logout");
      reject("error");
    }
  });
  return p;
};

//Success Authentication
export const authSuccess = (token) => {
  const authInfo = JwtTokenDecode(token);
  return {
    type: AUTH_SUCCESS, 
    token, 
    authInfo
  };
};

//Fail Authenticate
export const authFail = errorMsg => ({
  type: AUTH_FAILED,
  errorMsg
});

// Gmail Login
export const gmailLogin = () => async dispatch => {
  try {
    const result = await Google.logInAsync({
      androidClientId:
        "833456763323-ig9ndr0tbvb62jv4ddn6j8pos3a49m35.apps.googleusercontent.com",
      //iosClientId: YOUR_CLIENT_ID_HERE,  <-- if you use iOS
      iosClientId:
        "246167928829-f3o30ljc9lvioefcqkruunodmj7hqsr4.apps.googleusercontent.com",
      scopes: ["profile", "email"]
    });
    if (result.type === "success") {
      dispatch({type: AUTH_START});
      const token = result.accessToken;
      try {
        const response = await fetch(`${baseurl}/auth/google/token`, {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({access_token: token})
        });

        const responseJson = await response.json();
        if (response.status === 200) {
          let token = responseJson.data.token;
          // JwtTokenDecode(responseJson.user.token);
          AsyncStorage.setItem("token", token);
          // AsyncStorage.setItem("loginType", "gmail");
          dispatch(authSuccess(token));
          return responseJson;
        } else {
          const errorMsg = responseJson.message;;
          dispatch(authFail(errorMsg));
          return responseJson;
        }
      } catch (e) {
        const errorMsg = "Error while gmail login";
        dispatch(authFail(errors));
        return { success: false, message: errorMsg, data: null};
      }
    } else {
      const errorMsg = "Error while gmail login";
      dispatch(authFail(errorMsg));
      return { success: false, message: errorMsg, data: null};
    }
  } catch (e) {
    const errorMsg = "Network error! Please try again.";
    dispatch(authFail(errorMsg));
    return { success: false, message: errorMsg, data: null};
  }
};

// Reset password 
export const resetPassword = async (data) => {
  let headers = {
    'Accept': "application/json",
    'Content-Type': 'application/json'
  }

  try {
    let res = await axios.post(`${baseurl}/auth/resetpassword`, data, { headers: headers });
    return res.data;

  } catch (e) {
    console.log("Error while calling resetPassword route", e);
    return e.response.data;
  }
}




  // return axios
  //   .post(
  //     "https://graph.facebook.com/me?access_token=${token}&fields=id,name,birthday,picture.type(large)",
  //     { access_token: token }
  //   )
  //   .then(async response => {
  //     console.log(response.data.picture.data.url);
  //     await AsyncStorage.setItem("name", response.data.name);
  //     await AsyncStorage.setItem("picture", response.data.picture.data.url);
  //     await AsyncStorage.setItem("token", token);

  //     dispatch({ type: AUTH_SUCCESS, token });
  //   })
  //   .catch(error => {
  //     console.log(`Error High Lavel ${error}`);
  //     dispatch({ type: AUTH_FAILED });
  //   });