import { AsyncStorage } from "react-native";
import { baseurl } from "./../../BaseUrl";

import * as userActionTypes from "./actionTypes";
import {
  FETCH_SINGLE_PLAYDATE_START,
  FETCH_SINGLE_PLAYDATE_SUCCESS,
  FETCH_SINGLE_PLAYDATE_FAILED,
  NOTIFICATION_LIST_START,
  NOTIFICATION_LIST_SUCCESS,
  NOTIFICATION_LIST_FAILED,
  DELETE_NOTIFICATION_LIST_START,
  DELETE_NOTIFICATION_LIST_SUCCESS,
  DELETE_NOTIFICATION_LIST_FAILED,
  READ_NOTIFICATION_LIST_START,
  READ_NOTIFICATION_LIST_SUCCESS,
  READ_NOTIFICATION_LIST_FAILED,
  SERVICE_REQUEST_START
} from "./actionTypes";

const fetchProfileSuccess = user =>
  //if (user.loginType === "gmail")
  // console.log(
  // "<---Using gmail user from local app. There will not be any backend token. Gmail has not implement with backend yet. --->"
  // );
  // else console.log("<---User fetched success --->");
  // console.log("<---UserInfo on fetchProfileSuccess action--->", user);
  ({
    type: userActionTypes.FETCH_USER_SUCCESS,
    user
  });

const fetchProfileFailed = errors => {
  console.error(`<---User fetched failed --->${errors}`);
  return {
    type: userActionTypes.FETCH_USER_FAILED,
    errors
  };
};

export const userProfile = userId => async dispatch => {
  dispatch({ type: userActionTypes.FETCH_USER_PROFILE_START });

  const token = await AsyncStorage.getItem("token");
  const loginType = await AsyncStorage.getItem("loginType");
  const tempObj = {
    social: {
      gmail: {}
    }
  };
  if (loginType === "gmail") {
    const userName = await AsyncStorage.getItem("userName");
    const photo = await AsyncStorage.getItem("photo");
    tempObj.loginType = loginType;

    tempObj.social.gmail.name = userName;
    tempObj.social.gmail.photo = photo;
  }

  try {
    const response = await fetch(`${baseurl}/users/${userId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      const user = responseJson;
      const isBreeder = String(user.accountType.breeder.isServiceProvider);
      await AsyncStorage.setItem("@petLuvsStore:isBreeder", isBreeder);
      user.loginType = loginType;
      dispatch(fetchProfileSuccess(user));
    } else if (loginType === "gmail") dispatch(fetchProfileSuccess(tempObj));
    else dispatch(fetchProfileFailed(responseJson.errors));
  } catch (e) {
    const errors = "Request Failed";
    if (loginType === "gmail") dispatch(fetchProfileSuccess(tempObj));
    else dispatch(fetchProfileFailed(errors));
  }
};

// Update user profile
export const updateUserProfile = user => async dispatch => {
  dispatch({ type: userActionTypes.UPDATE_USER_PROFILE_START });
  const token = await AsyncStorage.getItem("token");

  try {
    const response = await fetch(`${baseurl}/users/profile`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(user)
    });
    // console.log("updated user profile", response);
    const responseJson = await response.json();
    // console.log("updated user profile2", responseJson);
    if (response.status === 200) {
      dispatch(updateUserProfileSuccess(responseJson));
    } else {
      dispatch(updateUserProfileFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(updateUserProfileFailed(errors));
  }
};

// Update user profile with file upload as formata. Everything remains same
export const updateUserProfileV2 = (user, serviceType, fileObj) => async dispatch => {
  dispatch({ type: userActionTypes.UPDATE_USER_PROFILE_START });
  const token = await AsyncStorage.getItem("token");

  if (fileObj) {
    let imageUrl = null;
    let res = await upload(fileObj);
    if (res && res.url !== null) {
      imageUrl = res.url;
    }

    if (serviceType === "breeder") {
      user["accountType"]["breeder"]["akcImage"] = imageUrl;
    } else if (serviceType === "profilePic") {
      user["profilePic"] = imageUrl;
    }else if(serviceType === "businessDetails") {
      user["photoId"] = imageUrl;
    }
    else {
      user["accountType"][serviceType]["businessImages"] = imageUrl;
    }
    //   user["accountType"]["petSitter"]["businessImages"] = imageUrl;
    // } else if (type === "petGrommer") {
    //   user["accountType"]["petGrommer"]["businessImages"] = imageUrl;
    // } else if (type === "petTrainer") {
    //   user["accountType"]["petTrainer"]["businessImages"] = imageUrl;
    // } else if (type === "dogWalker") {
    //   user["accountType"]["dogWalker"]["businessImages"] = imageUrl;
    // }
  }

  try {
    const response = await fetch(`${baseurl}/users/profile`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(user)
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      dispatch(updateUserProfileSuccess(responseJson));
    } else {
      dispatch(updateUserProfileFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(updateUserProfileFailed(errors));
  }
};

const updateUserProfileSuccess = user =>
  // console.log("Update userProfile success");
  ({
    type: userActionTypes.UPDATE_USER_PROFILE_SUCCESS,
    user
  });

const updateUserProfileFailed = user => {
  return {
    type: userActionTypes.UPDATE_USER_PROFILE_FAILED,
    user
  };
};

// notificationList failed
export const notificationList = notificationRecieved => async dispatch => {
  // const token = await AsyncStorage.getItem("token");
  dispatch({
    type: NOTIFICATION_LIST_START
  });

  try {
    // const response = await fetch(`${baseurl}/playdates`, {
    //   method: "GET",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json",
    //     Authorization: `Bearer ${token}`
    //   }
    //   // body: JSON.stringify(reviewData)
    // });
    // console.log("jjgjhkjjhkasdasdasdas");

    // const responseJson = await response.json();
    // console.log("jjgjhkjjhk in notification list", notificationRecieved);

    if (notificationRecieved) {
      // console.log("entered succes state in notificationList", notificationRecieved);
      dispatch(notificationListSuccess(notificationRecieved));
    } else {
      dispatch(notificationListFailed(notificationRecieved.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when update notificationList";
    dispatch(
      notificationListFailed({
        errors
      })
    );
  }
};

export const notificationListSuccess = notifications =>
  // console.log("notificationList success", notifications);
  ({
    type: NOTIFICATION_LIST_SUCCESS,
    notifications
  });

// notificationList failed
export const notificationListFailed = errors => ({
  type: NOTIFICATION_LIST_FAILED,
  errors
});

// notificationList failed
export const getNotificationList = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: NOTIFICATION_LIST_START
  });
  // console.log("FETCH_SINGLE_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/notifications`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });

    const responseJson = await response.json();

    if (response.status === 200) {
      dispatch(getNotificationListSuccess(responseJson.notifications));
    } else {
      dispatch(getNotificationListFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when update notificationList";
    dispatch(
      getNotificationListFailed({
        errors
      })
    );
  }
};

export const getNotificationListSuccess = notifications => ({
  type: NOTIFICATION_LIST_SUCCESS,
  notifications
});

// notificationList failed
export const getNotificationListFailed = errors => ({
  type: NOTIFICATION_LIST_FAILED,
  errors
});

// notificationList failed
export const deleteNotificationList = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: DELETE_NOTIFICATION_LIST_START
  });
  // console.log("FETCH_SINGLE_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/notifications`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas");

    const responseJson = await response.json();
    // console.log("notification in delete notification", responseJson);

    if (response.status === 200) {
      dispatch(deleteNotificationListSuccess(responseJson));
    } else {
      dispatch(deleteNotificationListFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed when update notificationList";
    dispatch(
      deleteNotificationListFailed({
        errors
      })
    );
  }
};

export const deleteNotificationListSuccess = status =>
  // console.log("notificationList success", playdates);
  ({
    type: DELETE_NOTIFICATION_LIST_SUCCESS,
    status
  });

// notificationList failed
export const deleteNotificationListFailed = errors => ({
  type: DELETE_NOTIFICATION_LIST_FAILED,
  errors
});
// notificationList failed
export const readNotificationList = id => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: READ_NOTIFICATION_LIST_START,
    data: { id }
  });
  // console.log("FETCH_SINGLE_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/notifications/read/${id}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas");

    const responseJson = await response.json();
    // console.log("notification in delete notification", responseJson);

    if (response.status === 200) {
      dispatch(deleteNotificationListSuccess(responseJson));
    } else {
      dispatch(deleteNotificationListFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed when update notificationList";
    dispatch(
      deleteNotificationListFailed({
        errors
      })
    );
  }
};

export const readNotificationListSuccess = status =>
  // console.log("notificationList success", playdates);
  ({
    type: READ_NOTIFICATION_LIST_SUCCESS,
    status
  });

// notificationList failed
export const readNotificationListFailed = errors => ({
  type: READ_NOTIFICATION_LIST_FAILED,
  errors
});


// Service Request

export const serviceRequestModel = status => ({
  type: SERVICE_REQUEST_START
});

const upload = async fileObj => {
  const token = await AsyncStorage.getItem("token");

  const promise = new Promise(async (resolve, reject) => {
    if (fileObj) {
      const formData = new FormData();
      formData.append("file", fileObj);

      try {
        const response = await fetch(`${baseurl}/users/profile/upload`, {
          method: "PUT",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          },
          body: formData
        });
        const responseJson = await response.json();
        if (response.status === 200) {
          resolve({ url:  responseJson.response.url });
        }else {
          resolve({ url:  null });
        }

      } catch (e) {
        const errors = "Request Failed";
        resolve({ url:  null });
      }
    }
  })
  return promise;
}
