import { AsyncStorage } from "react-native";
import * as petActionTypes from "./actionTypes";

import { baseurl } from "../../BaseUrl";

export const addPet = petData => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  const { name, image, breed, attributes, birthday, species } = petData;

  dispatch({
    type: petActionTypes.ADD_PET_START
  });
  try {
    const formData = new FormData();
    if (image !== null) {
      formData.append("file", {
        uri: image,
        type: "image/jpeg",
        name: `${name}.jpg`
      });
    }

    formData.append("name", name);
    formData.append("breed", breed);
    formData.append("species", species);
    formData.append("birthday", birthday);
    formData.append("insured", attributes.insured);
    formData.append("fixed", attributes.fixed);
    formData.append("fullyVaccinated", attributes.fullyVaccinated);
    const response = await fetch(`${baseurl}/pets`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`
      },
      body: formData
    });
    const responseJson = await response.json();
    // console.log(responseJson);

    if (response.status === 200) {
      dispatch(addPetSuccess(responseJson));
      // console.log(`after succes${responseJson}`);
    } else {
      dispatch(addPetFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(
      addPetFailed({
        errors
      })
    );
  }
};

export const addPetSuccess = pet => {
  // console.log("success => 78 =>");
  return {
    type: petActionTypes.ADD_PET_SUCCESS,
    pet,
    playateAdded: true
  };
};

export const addPetFailed = errors => {
  // console.log("error => 86 =>");
  return {
    type: petActionTypes.ADD_PET_FAILED,
    errors,
    playateAdded: false
  };
};

export const fetchAllPets = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: petActionTypes.FETCH_PETS_START
  });

  try {
    const response = await fetch(`${baseurl}/pets`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) {

      // console.log("Fetch pet Success on petAction file => 110 =>", responseJson);
      dispatch(fetchPetsSuccess(responseJson));
    } else {
      dispatch(fetchPetsFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(
      fetchPetsFailed({
        errors
      })
    );
  }
};

export const fetchPetsSuccess = pets =>
  // console.log("Total pets: ", pets.length);
  (
    {
    type: petActionTypes.FETCH_PETS_SUCCESS,
    pets
  });

export const fetchPetsFailed = errors => ({
  type: petActionTypes.FETCH_PETS_FAILED,
  errors
});

// To upate a pet
export const updatePet = (petId, data) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: petActionTypes.UPDATE_PET_START
  });

  try {
    const response = await fetch(`${baseurl}/pets/${petId}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(data)
    });

    const responseJson = await response.json();

    if (response.status === 200) {
      dispatch(updatePetSuccess(responseJson));
    } else {
      // console.log(responseJson.errors);
      dispatch(updatePetFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(
      updatePetFailed({
        errors
      })
    );
  }
};

const updatePetSuccess = pet => {
  // console.log("Pet updation successfull", pet);
  return {
    type: petActionTypes.UPDATE_PET_SUCCESS,
    pet
  };
};

const updatePetFailed = errors => {
  return {
    type: petActionTypes.UPDATE_PET_SUCCESS,
    errors
  };
};

// Fetch pet with id
export const fetchPetWithId = petId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: petActionTypes.FETCH_PET_WITH_ID_START
  });

  try {
    const response = await fetch(`${baseurl}/pets/${petId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });

    const responseJson = await response.json();

    if (response.status === 200) {
      dispatch(fetchPetWithIdSuccess(responseJson));
    } else {
      // console.log(responseJson.errors);
      dispatch(fetchPetWithIdFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(
      fetchPetWithIdFailed({
        errors
      })
    );
  }
};

const fetchPetWithIdSuccess = pet => {
  // console.log(`Pet fetch success with =>${JSON.stringify(pet)}`);
  return {
    type: petActionTypes.FETCH_PET_WITH_ID_SUCCESS,
    pet
  };
};

const fetchPetWithIdFailed = errors => {
  return {
    type: petActionTypes.FETCH_PET_WITH_ID_FAILED,
    errors
  };
};

// Upload media for pet
export const uploadPetMedia = (petId, fileObj) => async dispatch => {
  if (fileObj == null) {
    return;
  }

  const formData = new FormData();
  if (fileObj !== null) {
    formData.append("file", fileObj);
  }

  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: petActionTypes.UPDATE_PET_START
  });

  try {
    const response = await fetch(`${baseurl}/pets/${petId}/uploadmedia`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: formData
    });

    const responseJson = await response.json();

    if (response.status === 200) {
      dispatch(fetchPetWithIdSuccess(responseJson));
    } else {
      // console.log(responseJson.errors);
      dispatch(fetchPetWithIdFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(
      fetchPetWithIdFailed({
        errors
      })
    );
  }
};
