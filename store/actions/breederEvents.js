import { AsyncStorage } from 'react-native';
import { baseurl, token } from '../../BaseUrl';
import{
    CREATE_BREEDEREVENT_START ,UPDATE_BREEDEREVENT_SUCCESS
} from './../actions/actionTypes';




// Creating a playdate
export const createBreederEvent = (breederEventData) => async dispatch => {

    let {dateStart,timeStart,dateEnd,timeEnd,date1,date2,address,akc}=breederEventData;
    // console.log(breederEventData+'hello');
    // dispatch({type: CREATE_BREEDEREVENT_START});

    var formdata=new FormData();

    formdata.append('dateStart',dateStart);
    formdata.append('timeStart',timeStart);
    formdata.append('dateEnd',dateEnd);
    formdata.append('timeEnd',timeEnd);
    formdata.append('date1',date1);
    formdata.append('date2',date2);
    formdata.append('address',address);
    formdata.append('AKCRegistrationNumber',akc.AKCRegistrationNumber);
    formdata.append('AKCCertificateImage',akc.AKCCertificateImage);
    // alert('hello')
    // console.log(breederEventData+'hello1');

};

// Updating a playdate
export const updateBreederEvent = () => async dispatch => {
     dispatch({type:UPDATE_BREEDEREVENT_SUCCESS});
};

// Delete a playdate
export const deleteBreederEvent = (breederEventId) => async dispatch => {
    // console.log(breederEventId);
};

// Fetch All Playdates
export const fetchBreederEvents = () => async dispatch => {
    // console.log('');
};

// Fetch one Playdate with id
export const fetchBreederEvent = () => async dispatch => {
    // console.log('');
};