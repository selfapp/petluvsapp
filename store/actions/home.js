import {AsyncStorage} from "react-native";
import {baseurl} from "./../../BaseUrl";

import * as actionTypes from "./actionTypes";


export const homeData = (page, refresh) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  const perPage = 10;

  dispatch({type: actionTypes.FETCH_HOME_DATA_START, refreshing: refresh});
  try {
    const response = await fetch(
      `${baseurl}/home/feeds?page=${page}&perPage=${perPage}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      }
    );
    const responseJson = await response.json();
    if (response.status === 200) {
      dispatch(fetchDataSuccess(responseJson, refresh));
      return true;
    } else {
      dispatch(fetchDataFailed(responseJson.errors));
      return false;
    }
  } catch (e) {
     console.log("failed to make request", e);
    const errors = "Request Failed";
    dispatch(fetchDataFailed(errors));
    return false;
  }
};

const fetchDataSuccess = (response, refreshing) => ({
  type: actionTypes.FETCH_HOME_DATA_SUCCESS,
  data: response['data'],
  refreshing,
  pages: response.pages
});

const fetchDataFailed = errors => {
  // console.log(errors);
  return {
    type: actionTypes.FETCH_HOME_DATA_FAILED,
    errors
  };
};

export const enableBackButton = () => {
  return {
    type: actionTypes.BACK_BUTTON_REQUIRED
  };
};

export const disableBackButton = () => {
  return {
    type: actionTypes.BACK_BUTTON_NOT_REQUIRED
  };
};

export const setHeaderTitle = (title) => async dispatch => {
  dispatch({
    type: actionTypes.SET_HEADER_TITLE,
    title
  });
};

export const removeHeaderTitle = () => {
  return {
    type: actionTypes.REMOVE_HEADER_TITLE
  };
};

export const incrementNotificationsCount = () => {
  return {
    type: actionTypes.INCREMENT_NOTIFICATIONS_COUNT
  }
};

export const resetNotificationsCount = () => {
  return {
    type: actionTypes.RESET_NOTIFICATIONS_COUNT
  }
};
