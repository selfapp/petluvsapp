import { AsyncStorage } from "react-native";

import {
  SEARCH_SERVICE_START,
  SEARCH_SERVICE_SUCCESS,
  SEARCH_SERVICE_FAILED
} from "./actionTypes";

import { baseurl } from "../../BaseUrl";

// searchService
export const searchService = details => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: SEARCH_SERVICE_START,
    data: details
  });

  try {
    const response = await fetch(`${baseurl}/services/search`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(details)
    });

    const responseJson = await response.json();

    if (response.status === 200) {
      dispatch(searchServiceSuccess(responseJson));
    } else {
      dispatch(searchServiceFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed when search service";
    dispatch(
      searchServiceFailed({
        errors
      })
    );
  }
};

// searchService success
export const searchServiceSuccess = data =>
  ({
    type: SEARCH_SERVICE_SUCCESS,
    data
  });

// searchService failed
export const searchServiceFailed = errors => {
  return {
    type: SEARCH_SERVICE_FAILED,
    errors
  };
};
