import {AsyncStorage} from "react-native";

import {
  CREATE_PLAYDATE_START,
  CREATE_PLAYDATE_SUCCESS,
  CREATE_PLAYDATE_FAILED,
  UPDATE_PLAYDATE_START,
  UPDATE_PLAYDATE_SUCCESS,
  UPDATE_PLAYDATE_FAILED,
  FETCH_ALL_PLAYDATES_SUCCESS,
  FETCH_ALL_PLAYDATES_FAILED,
  FETCH_ALL_PLAYDATES_START,
  JOIN_PLAYDATE_START,
  JOIN_PLAYDATE_SUCCESS,
  JOIN_PLAYDATE_FAILED,
  LEAVE_PLAYDATE_START,
  LEAVE_PLAYDATE_SUCCESS,
  LEAVE_PLAYDATE_FAILED,
  FETCH_PLAYDATE_START,
  FETCH_PLAYDATE_SUCCESS,
  FETCH_PLAYDATE_FAILED,
  REVIEW_PLAYDATE_START,
  REVIEW_PLAYDATE_SUCCESS,
  REVIEW_PLAYDATE_FAILED,
  FETCH_REVIEW_PLAYDATE_START,
  FETCH_REVIEW_PLAYDATE_SUCCESS,
  FETCH_REVIEW_PLAYDATE_FAILED,
  FETCH_USER_REVIEWS_START,
  FETCH_USER_REVIEWS_SUCCESS,
  FETCH_USER_REVIEWS_FAILED,
  ACCEPT_REVIEW_PLAYDATE_START,
  ACCEPT_REVIEW_PLAYDATE_SUCCESS,
  ACCEPT_REVIEW_PLAYDATE_FAILED,
  DECLINE_REVIEW_PLAYDATE_START,
  DECLINE_REVIEW_PLAYDATE_SUCCESS,
  DECLINE_REVIEW_PLAYDATE_FAILED,
  FETCH_SINGLE_PLAYDATE_START,
  FETCH_SINGLE_PLAYDATE_SUCCESS,
  FETCH_SINGLE_PLAYDATE_FAILED,
  SHARE_REVIEW_START,
  SHARE_REVIEW_SUCCESS,
  SHARE_REVIEW_FAILED,
  SPECIES_AND_BREED_START,
  SPECIES_AND_BREED_SUCCESS,
  SPECIES_AND_BREED_FAILED
} from "./actionTypes";
import {baseurl} from "../../BaseUrl";

// Creating a playdate
export const createPlaydate = playDateData => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: CREATE_PLAYDATE_START,
    addedPlaydate: playDateData
  });

  try {
    const response = await fetch(`${baseurl}/playdates`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(playDateData)
    });

    const responseJson = await response.json();

    if (response.status === 200) {
      dispatch(createPlaydateSuccess(responseJson.playdate));
      return true;
    } else {
      dispatch(createPlaydateFailed(responseJson.errors));
      return false;
    }
  } catch (e) {
    const errors = "Request Failed when creating playdate";
    dispatch(
      createPlaydateFailed({
        errors
      })
    );
    return false;
  }
};

// Create playdate success
export const createPlaydateSuccess = playdate =>
  // console.log("success => 58 =>", playdate);
  ({
    type: CREATE_PLAYDATE_SUCCESS,
    playdate
  });

// Create playdate failed
export const createPlaydateFailed = errors => {
  return {
    type: CREATE_PLAYDATE_FAILED,
    errors
  };
};

// // Updating a playdate
// export const updatePlaydate = playDateData => async dispatch => {
//   // console.log(playDateData);
// };

// Delete a playdate
export const deletePlaydate = playDateData => async dispatch => {
  // console.log(playDateData);
};

// Fetch All Playdates
export const fetchAllPlaydates = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: FETCH_ALL_PLAYDATES_START
  });

  try {
    const response = await fetch(`${baseurl}/playdates`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status == 200) {
      dispatch(fetchAllPlaydatesSuccess(responseJson));
    } else {
      dispatch(fetchAllPlaydatesFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(fetchAllPlaydatesFailed({errors}));
  }
};

// fetch playdate success
export const fetchAllPlaydatesSuccess = playdates => ({
  type: FETCH_ALL_PLAYDATES_SUCCESS,
  playdates
});

// Crefetchate playdate failed
export const fetchAllPlaydatesFailed = errors => ({
  type: FETCH_ALL_PLAYDATES_FAILED,
  errors
});

// <------Fetch one Playdate with id ----->
export const fetchPlaydate = playdateId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: FETCH_PLAYDATE_START
  });
  // console.log("inside fetch playdate", playdateId);

  try {
    const response = await fetch(`${baseurl}/playdates/${playdateId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    // console.log("inside fetchplaydate", responseJson);
    if (response.status === 200) {
      dispatch(fetchPlaydateSuccess(responseJson));
      return 'success';
    } else {
      dispatch(fetchPlaydateFailed(responseJson.errors));
      return 'error';
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(fetchPlaydateFailed({errors}));
    return 'error';
  }
};

// Fetch playdate success
export const fetchPlaydateSuccess = playdate =>
  // console.log("Fetch playate success", playdate);
  ({
    type: FETCH_PLAYDATE_SUCCESS,
    playdate
  });

// Fetch playdate failed
export const fetchPlaydateFailed = errors => ({
  type: FETCH_PLAYDATE_FAILED,
  errors
});


// TO DO
// <------update one Playdate with id ----->
export const updatePlaydate = (playdateId, playDateData) =>  async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: UPDATE_PLAYDATE_START,
    addedPlaydate: playDateData
  });
  // console.log("inside fetch playdate");

  try {
    const response = await fetch(`${baseurl}/playdates/${playdateId}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(playDateData)
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      dispatch(updatePlaydateSuccess(responseJson));
      return true;
    } else {
      dispatch(updatePlaydateFailed(responseJson.errors));
      return false;
    }
  } catch (e) {
    const errors = "Request Failed";
    dispatch(updatePlaydateFailed({errors}));
    return false;
  }
};

// update playdate success
export const updatePlaydateSuccess = playdate =>
  // console.log("Fetch playate success", playdate);
  ({
    type: UPDATE_PLAYDATE_SUCCESS,
    playdate
  });

// update playdate failed
export const updatePlaydateFailed = errors => ({
  type: UPDATE_PLAYDATE_FAILED,
  errors
});


// <----- Join a playdate ------>
export const joinPlaydate = (playdateId, pets) => async (
  dispatch,
  getState
) => {
  const token = await AsyncStorage.getItem("token");
  // const { data } = getState().home;
  // const playDateArray = data.map(el => el._id);
  dispatch({
    type: JOIN_PLAYDATE_START
  });

  const data = {
    pets
  };

  try {
    const response = await fetch(`${baseurl}/playdates/join/${playdateId}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(data)
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      dispatch(joinPlaydateSuccess(responseJson));
    }
  } catch (e) {
    // console.log("failed to make request");
    const errors = "Request Failed";
    dispatch(joinPlaydateFailed({errors}));
  }
};

// Join playdate success
export const joinPlaydateSuccess = playdate =>
  // console.log("join playdate success", playdate);
  ({
    type: JOIN_PLAYDATE_SUCCESS,
    playdate
  });

// Join playdate failed
export const joinPlaydateFailed = errors => ({
  type: JOIN_PLAYDATE_FAILED,
  errors
});

// Leave a playdate
export const leavePlaydate = playdateId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: LEAVE_PLAYDATE_START
  });

  try {
    const response = await fetch(`${baseurl}/playdates/leave/${playdateId}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    const responseJson = await response.json();
    if (response.status === 200) {
      dispatch(leavePlaydateSuccess(responseJson));
    } else {
      dispatch(leavePlaydateFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log("failed to make request");
    const errors = "Request Failed";
    dispatch(leavePlaydateFailed({errors}));
  }
};

// Join playdate success
export const leavePlaydateSuccess = playdate =>
  // console.log("Leave playdate success", playdate);
  ({
    type: LEAVE_PLAYDATE_SUCCESS,
    playdate
  });

// Join playdate failed
export const leavePlaydateFailed = errors => ({
  type: LEAVE_PLAYDATE_FAILED,
  errors
});

export const reviewPlaydate = (reviewData, playdateId) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: REVIEW_PLAYDATE_START
  });

  try {
    const response = await fetch(`${baseurl}/reviews`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas", reviewData, playdateId);

    const responseJson = await response.json();
    // console.log("jjgjhkjjhk", responseJson);

    if (response.status === 200) {
      dispatch(reviewPlaydateSuccess(responseJson));
    } else {
      dispatch(reviewPlaydateFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when reviewing playdate";
    dispatch(
      reviewPlaydateFailed({
        errors
      })
    );
  }
};

export const reviewPlaydateSuccess = reviewData =>
  // console.log("review playdate success", reviewData);
  ({
    type: REVIEW_PLAYDATE_SUCCESS,
    reviewData
  });

// review playdate failed
export const reviewPlaydateFailed = errors => ({
  type: REVIEW_PLAYDATE_FAILED,
  errors
});

export const fetchReviewPlaydate = playdateId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: FETCH_REVIEW_PLAYDATE_START
  });

  try {
    const response = await fetch(`${baseurl}/reviews/${playdateId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas", playdateId);

    const responseJson = await response.json();
    // console.log("jjgjhkjjhk", responseJson);

    if (response.status === 200) {
      dispatch(fetchReviewPlaydateSuccess(responseJson));
    } else {
      // console.log("failed");
      dispatch(fetchReviewPlaydateFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when reviewing playdate";
    dispatch(
      fetchReviewPlaydateFailed({
        errors
      })
    );
  }
};

export const fetchReviewPlaydateSuccess = reviews =>
  // console.log("fetch review playdate success", reviews);
  ({
    type: FETCH_REVIEW_PLAYDATE_SUCCESS,
    reviews: reviews.reviews
  });

// fetch review playdate failed
export const fetchReviewPlaydateFailed = errors => ({
  type: FETCH_REVIEW_PLAYDATE_FAILED,
  errors
});

// // Fetch All user Reviews
// export const fetchUserReviews = () => async dispatch => {
//   const token = await AsyncStorage.getItem("token");
//   dispatch({type: FETCH_USER_REVIEWS_START});

//   try {
//     const response = await fetch(`${baseurl}/reviews`, {
//       method: "GET",
//       headers: {
//         Accept: "application/json",
//         "Content-Type": "application/json",
//         Authorization: `Bearer ${token}`
//       }
//     });

//     const responseJson = await response.json();

//     if (response.status === 200) {
//       dispatch(fetchUserReviewsSuccess(responseJson));
//     } else {
//       dispatch(fetchUserReviewsFailed(responseJson.errors));
//     }
//   } catch (e) {
//     const errors = "Request Failed when reviewing playdate";
//     dispatch(
//       fetchUserReviewsFailed({
//         errors
//       })
//     );
//   }
// };

// Fetch All user ReviewsLates
export const fetchUserReviews = (receiverId) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({type: FETCH_USER_REVIEWS_START});

  try {
    const response = await fetch(`${baseurl}/reviews?receiverId=${receiverId}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });

    const responseJson = await response.json();
    // console.log(responseJson);
    if (response.status === 200) {
      dispatch(fetchUserReviewsSuccess(responseJson.data));
    } else {
      dispatch(fetchUserReviewsFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed when reviewing playdate";
    dispatch(
      fetchUserReviewsFailed({
        errors
      })
    );
  }
};

export const fetchUserReviewsSuccess = reviews =>
  // console.log("fetch review playdate success", reviews);
  ({
    type: FETCH_USER_REVIEWS_SUCCESS,
    userReviews: reviews.reviews,
    starcount: reviews.avgStarCount
  });

// fetch review playdate failed
export const fetchUserReviewsFailed = errors => ({
  type: FETCH_USER_REVIEWS_FAILED,
  errors
});

export const acceptReviewPlaydate = reviewId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: ACCEPT_REVIEW_PLAYDATE_START
  });
  // console.log("ACCEPT_REVIEW_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/reviews/accept/${reviewId}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas", reviewId);

    const responseJson = await response.json();
    // console.log("jjgjhkjjhk", responseJson);

    if (response.status === 200) {
      // console.log("entered succes state", responseJson);
      dispatch(acceptReviewPlaydateSuccess(responseJson));
    } else {
      // console.log("failed");
      dispatch(acceptReviewPlaydateFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when reviewing playdate";
    dispatch(
      acceptReviewPlaydateFailed({
        errors
      })
    );
  }
};

export const acceptReviewPlaydateSuccess = reviews =>
  // console.log("fetch review playdate success", reviews);
  ({
    type: ACCEPT_REVIEW_PLAYDATE_SUCCESS,
    acceptedReviews: reviews.review
  });

// accept review playdate failed
export const acceptReviewPlaydateFailed = errors => ({
  type: ACCEPT_REVIEW_PLAYDATE_FAILED,
  errors
});

// fetch single playdate failed
export const declineReviewPlaydate = reviewId => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: DECLINE_REVIEW_PLAYDATE_START
  });
  // console.log("DECLINE_REVIEW_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/reviews/decline/${reviewId}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas", reviewId);

    const responseJson = await response.json();
    // console.log("jjgjhkjjhk", responseJson);

    if (response.status === 200) {
      // console.log("entered succes state", responseJson);
      dispatch(declineReviewPlaydateSuccess(responseJson));
    } else {
      // console.log("failed");
      dispatch(declineReviewPlaydateFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when fetch single playdate";
    dispatch(
      declineReviewPlaydateFailed({
        errors
      })
    );
  }
};

export const declineReviewPlaydateSuccess = reviews =>
  // console.log("fetch single playdate success", reviews);
  ({
    type: DECLINE_REVIEW_PLAYDATE_SUCCESS,
    declinedReviews: reviews.review
  });

// accept review playdate failed
export const declineReviewPlaydateFailed = errors => ({
  type: DECLINE_REVIEW_PLAYDATE_FAILED,
  errors
});

// fetch single playdate failed
export const fetchSinglePlaydate = () => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: FETCH_SINGLE_PLAYDATE_START
  });
  // console.log("FETCH_SINGLE_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/playdates`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas");

    const responseJson = await response.json();
    // console.log("jjgjhkjjhk", responseJson);

    if (response.status === 200) {
      // console.log("entered succes state in userPlaydates", responseJson);
      dispatch(fetchSinglePlaydateSuccess(responseJson));
    } else {
      dispatch(fetchSinglePlaydateFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when fetch single playdate";
    dispatch(
      fetchSinglePlaydateFailed({
        errors
      })
    );
  }
};

export const fetchSinglePlaydateSuccess = playdates =>
  // console.log("fetch single playdate success", playdates);
  ({
    type: FETCH_SINGLE_PLAYDATE_SUCCESS,
    userPlaydate: playdates
  });

// fetch single playdate failed
export const fetchSinglePlaydateFailed = errors => ({
  type: FETCH_SINGLE_PLAYDATE_FAILED,
  errors
});
// share review failed
export const shareReview = id => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: SHARE_REVIEW_START
  });
  // console.log("FETCH_SINGLE_PLAYDATE_START");

  try {
    const response = await fetch(`${baseurl}/reviews/share/${id}`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
      // body: JSON.stringify(reviewData)
    });
    // console.log("jjgjhkjjhkasdasdasdas");

    const responseJson = await response.json();

    if (response.status === 200) {
      // console.log("entered succes state in userPlaydates", responseJson);
      dispatch(shareReviewSuccess(responseJson));
    } else {
      dispatch(shareReviewFailed(responseJson.errors));
    }
  } catch (e) {
    // console.log(`failed to make request${e}`);
    const errors = "Request Failed when fetch single playdate";
    dispatch(
      shareReviewFailed({
        errors
      })
    );
  }
};

export const shareReviewSuccess = playdates =>
  // console.log("fetch single playdate success", playdates);
  ({
    type: SHARE_REVIEW_SUCCESS,
    userPlaydate: playdates
  });

// fetch single playdate failed
export const shareReviewFailed = errors => ({
  type: SHARE_REVIEW_FAILED,
  errors
});

// share review failed
export const petSpeciesBreed = (cb = () => {}) => async dispatch => {
  const token = await AsyncStorage.getItem("token");
  dispatch({
    type: SPECIES_AND_BREED_START
  });

  try {
    // console.log("pet species entered ", responseJson);
    const response = await fetch(`${baseurl}/pet-breeds`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    // console.log("jjgjhkjjhkasdasdasdas");

    const responseJson = await response.json();
    // console.log("pet species and breed ", responseJson);

    if (response.status === 200) {
      // console.log("entered succes state in petSpeciesBreed", responseJson);
      dispatch(petSpeciesBreedSuccess(responseJson));
      cb();
    } else {
      dispatch(petSpeciesBreedFailed(responseJson.errors));
    }
  } catch (e) {
    const errors = "Request Failed when fetch petSpeciesBreed";
    dispatch(
      petSpeciesBreedFailed({
        errors
      })
    );
  }
};

export const petSpeciesBreedSuccess = speciesAndBreeds =>
  // console.log("fetch petSpeciesBreed success", playdates);
  ({
    type: SPECIES_AND_BREED_SUCCESS,
    data: speciesAndBreeds
  });

// fetch petSpeciesBreed failed
export const petSpeciesBreedFailed = errors => ({
  type: SPECIES_AND_BREED_FAILED,
  errors
});
