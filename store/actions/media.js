import {AsyncStorage} from "react-native";

import {baseurl} from "../../BaseUrl";

export const shareMedia = async (mediaId) => {

  const token = await AsyncStorage.getItem("token");
  const data = { mediaId: mediaId }

  try {
    const response = await fetch(`${baseurl}/media/share`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(data)
    });

    const responseJson = await response.json();

    if (response.status === 200) {
    //   console.log(responseJson);
      return true;
    } else {
      return false;
    }
  } catch (e) {
    const errors = "Request Failed when creating playdate";
    return false;
  }

}