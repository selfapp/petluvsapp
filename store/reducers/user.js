import * as actionTypes from "../actions/actionTypes";

const initialState = {
  loading: false,
  user: null,
  errors: null,
  notifications: [],
  serviceRequestModel:false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USER_PROFILE_START:
      return { ...state, loading: true, errors: null };

    case actionTypes.FETCH_USER_SUCCESS:
      return { ...state, loading: false, errors: null, user: action.user };

    case actionTypes.FETCH_USER_FAILED:
      return { ...state, loading: false, errors: action.errors };

    case actionTypes.UPDATE_USER_PROFILE_START:
      return { ...state, loading: true, errors: null };

    case actionTypes.UPDATE_USER_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null,
        user: { ...state.user, ...action.user }
        //  user: action.user
      };

    case actionTypes.UPDATE_USER_PROFILE_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors
      };

    case actionTypes.NOTIFICATION_LIST_START:
      return { ...state, loading: true, errors: null };

    case actionTypes.NOTIFICATION_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null,
        notifications: action.notifications
      };

    case actionTypes.NOTIFICATION_LIST_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors
      };

    case actionTypes.DELETE_NOTIFICATION_LIST_START:
      return { ...state, loading: true, errors: null, notificationlist: [] };

    case actionTypes.DELETE_NOTIFICATION_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null
      };

    case actionTypes.DELETE_NOTIFICATION_LIST_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors
      };

    case actionTypes.READ_NOTIFICATION_LIST_START:
      const notificationsCopy = state.notifications;
      notificationsCopy.map(notification => {
        if (notification._id === action.data.id) {
          notification.read = true;
        }
        return notification;
      });

      return {
        ...state,
        notifications: notificationsCopy,
        loading: true,
        errors: null,
        readId: Math.random()
      };

    case actionTypes.READ_NOTIFICATION_LIST_SUCCESS:
      // let notification = state.notifications;
      // notification = notification.concat[action.notifications];
      // console.log("var notification", notification);
      return {
        ...state,
        loading: false,
        errors: null
        // notificationlist: action.notificationlist
      };
    case actionTypes.READ_NOTIFICATION_LIST_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors
      };
    case actionTypes.SERVICE_REQUEST_START:
      return{
        ...state,
        serviceRequestModel: !state.serviceRequestModel,
        errors: null
      }

    default:
      return state;
  }
};

export default reducer;
