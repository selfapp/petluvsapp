import{
    CREATE_BREEDEREVENT_START,UPDATE_BREEDEREVENT_SUCCESS
} from './../actions/actionTypes';


const initialState = {
events:[],
    errors:null,
    loading:false,
}

const reducer= (state=initialState,action)=>{
    switch (action.type){

        case CREATE_BREEDEREVENT_START:
            // console.log('hello2')
            return{...state,loading:true,errors:null};
        case UPDATE_BREEDEREVENT_SUCCESS:
            return{...state,loading:false,errors:null};
        default :
            return state;
    }

};

export default reducer;