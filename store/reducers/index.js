import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import auth from "./auth";
import user from "./user";
import home from "./home";
import pets from "./pet";
import events from "./breederEvents";
import playDates from "./playDates";
import services from "./services";

export default combineReducers({
  auth,
  user,
  home,
  pets,
  events,
  playDates,
  form,
  services
});
