import {
  CREATE_PLAYDATE_START,
  CREATE_PLAYDATE_SUCCESS,
  CREATE_PLAYDATE_FAILED,
  UPDATE_PLAYDATE_START,
  UPDATE_PLAYDATE_SUCCESS,
  UPDATE_PLAYDATE_FAILED,
  FETCH_ALL_PLAYDATES_START,
  FETCH_ALL_PLAYDATES_SUCCESS,
  FETCH_ALL_PLAYDATES_FAILED,
  FETCH_PLAYDATE_START,
  FETCH_PLAYDATE_SUCCESS,
  FETCH_PLAYDATE_FAILED,
  JOIN_PLAYDATE_START,
  JOIN_PLAYDATE_SUCCESS,
  JOIN_PLAYDATE_FAILED,
  LEAVE_PLAYDATE_START,
  LEAVE_PLAYDATE_SUCCESS,
  REVIEW_PLAYDATE_START,
  REVIEW_PLAYDATE_SUCCESS,
  REVIEW_PLAYDATE_FAILED,
  FETCH_REVIEW_PLAYDATE_START,
  FETCH_REVIEW_PLAYDATE_SUCCESS,
  FETCH_REVIEW_PLAYDATE_FAILED,
  ACCEPT_REVIEW_PLAYDATE_START,
  ACCEPT_REVIEW_PLAYDATE_SUCCESS,
  ACCEPT_REVIEW_PLAYDATE_FAILED,
  DECLINE_REVIEW_PLAYDATE_START,
  DECLINE_REVIEW_PLAYDATE_SUCCESS,
  DECLINE_REVIEW_PLAYDATE_FAILED,
  FETCH_SINGLE_PLAYDATE_START,
  FETCH_SINGLE_PLAYDATE_SUCCESS,
  FETCH_SINGLE_PLAYDATE_FAILED,
  FETCH_USER_REVIEWS_START,
  FETCH_USER_REVIEWS_SUCCESS,
  FETCH_USER_REVIEWS_FAILED,
  SHARE_REVIEW_START,
  SHARE_REVIEW_SUCCESS,
  SHARE_REVIEW_FAILED,
  SPECIES_AND_BREED_START,
  SPECIES_AND_BREED_SUCCESS,
  SPECIES_AND_BREED_FAILED
} from "../actions/actionTypes";

const initialState = {
  playdates: [],
  userPlaydates: [],
  errors: null,
  loading: false,
  playateAdded: false,
  playateUpdated: false,
  playdate: {},
  reviewData: "",
  reviews: [],
  acceptedReviews: [],
  declinedReviews: [],
  avgStarCount: 0,
  speciesAndBreeds: [],
  userReviews: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PLAYDATE_START:
      // const playdate = state.playdates.concat(action.addedPlaydate);
      // console.log('after create playdate start', action.addedPlaydate);
      // console.log
      return {
        ...state,
        loading: true,
        errors: null,
        playateAdded: false,
        // playdates: playdate
      };

    case CREATE_PLAYDATE_SUCCESS:
      const playdates = state.playdates.concat(action.playdate);
      const userPlaydates = state.userPlaydates.concat(action.playdate);
      return {
        ...state,
        loading: false,
        errors: null,
        playdates,
        playateAdded: true,
        userPlaydates
      };
    case CREATE_PLAYDATE_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors,
        playateAdded: false
      };

    case UPDATE_PLAYDATE_START:
      return {
        ...state,
        loading: true,
        errors: null
      }
    case UPDATE_PLAYDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        playateUpdated: true
      }
    case UPDATE_PLAYDATE_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors
      }

    case FETCH_ALL_PLAYDATES_START:
      return {...state, loading: true, errors: null};
    case FETCH_ALL_PLAYDATES_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null,
        playdates: action.playdates
      };
    case FETCH_ALL_PLAYDATES_FAILED:
      return {...state, loading: false, errors: action.errors};
    case FETCH_PLAYDATE_START:
      return {...state, loading: true, errors: null, playdate: {}};
    case FETCH_PLAYDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null,
        playdate: action.playdate
      };
    case FETCH_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};
    case JOIN_PLAYDATE_START:
      return {
        ...state,
        loading: true,
        errors: null,
        playdateArray: action.playdateArray
      };
    case JOIN_PLAYDATE_SUCCESS:
      const k = {
        ...state.playdate,
        userJoining: action.playdate.userJoining,
        pets: action.playdate.pets
      };
      return {
        ...state,
        loading: true,
        errors: null,
        playdate: k
      };

    case JOIN_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};

    case LEAVE_PLAYDATE_START:
      return {
        ...state,
        loading: true,
        errors: null
      };

    case LEAVE_PLAYDATE_SUCCESS:
      // playdate = state.playdates.filter(p => p._id === action.playdate._id);
      // console.log('In playdate reducer', playdate);
      const p = {
        ...state.playdate,
        userJoining: action.playdate.userJoining,
        pets: action.playdate.pets
      };
      return {
        ...state,
        loading: true,
        errors: null,
        playdate: p
      };

    case REVIEW_PLAYDATE_START:
      return {...state, loading: true};

    case REVIEW_PLAYDATE_SUCCESS:
      return {...state, loading: false, reviewData: action.reviewData};

    case REVIEW_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};

    case FETCH_REVIEW_PLAYDATE_START:
      return {...state, loading: true};

    case FETCH_REVIEW_PLAYDATE_SUCCESS:
      return {...state, loading: false, reviews: action.reviews};

    case FETCH_REVIEW_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};

    case FETCH_USER_REVIEWS_START:
      return {...state, userReviews: [], loading: true};

    case FETCH_USER_REVIEWS_SUCCESS:
      return {
        ...state,
        loading: false,
        userReviews: action.userReviews,
        avgStarCount: action.starcount
      };

    case FETCH_USER_REVIEWS_FAILED:
      return {...state, loading: false, errors: action.errors};

    case ACCEPT_REVIEW_PLAYDATE_START:
      return {...state, loading: true};

    case ACCEPT_REVIEW_PLAYDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        acceptedReviews: action.acceptedReviews
      };

    case ACCEPT_REVIEW_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};

    case DECLINE_REVIEW_PLAYDATE_START:
      return {...state, loading: true};

    case DECLINE_REVIEW_PLAYDATE_SUCCESS:
      return {...state, loading: false, reviews: action.declinedReviews};

    case DECLINE_REVIEW_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};

    case FETCH_SINGLE_PLAYDATE_START:
      return {...state, loading: true};

    case FETCH_SINGLE_PLAYDATE_SUCCESS:
      return {...state, loading: false, userPlaydates: action.userPlaydate};

    case FETCH_SINGLE_PLAYDATE_FAILED:
      return {...state, loading: false, errors: action.errors};

    case SHARE_REVIEW_START:
      return {...state, loading: true};

    case SHARE_REVIEW_SUCCESS:
      return {...state, loading: false,
        // userPlaydates: action.userPlaydate
      };

    case SHARE_REVIEW_FAILED:
      return {...state, loading: false, errors: action.errors};

    case SPECIES_AND_BREED_START:
      return {...state};

    case SPECIES_AND_BREED_SUCCESS:
      return {
        ...state,
        loading: false,
        speciesAndBreeds: action.data
      };

    case SPECIES_AND_BREED_FAILED:
      return {...state, loading: false, errors: action.errors};

    default:
      return state;
  }
};

export default reducer;



