import * as actionTypes from "../actions/actionTypes";

const initialState = {
  pets: [],
  errors: null,
  loading: false,
  pet: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_PET_START:
      return { ...state, loading: true, errors: null, added: false };
    case actionTypes.ADD_PET_SUCCESS:
      const pets = state.pets.concat(action.pet);
      return { ...state, pets, loading: false, errors: null, added: true };
    case actionTypes.ADD_PET_FAILED:
      return { ...state, errors: action.errors, loading: false, added: false };
    case actionTypes.FETCH_PETS_START:
      return { ...state, loading: true, errors: null, added: false };
    case actionTypes.FETCH_PETS_SUCCESS:
      return {
        ...state,
        pets: action.pets,
        errors: null,
        loading: false,
        added: false
      };
    case actionTypes.FETCH_PETS_FAILED:
      return { ...state, loading: false, errors: action.errors };
    case actionTypes.FETCH_PET_WITH_ID_START:
      return { ...state, loading: true, errors: null };
    case actionTypes.FETCH_PET_WITH_ID_SUCCESS:
      return { ...state, loading: false, erros: null, pet: action.pet };
    case actionTypes.FETCH_PET_WITH_ID_FAILED:
      return { ...state, loading: false, errors: action.error, pet: {} };
    case actionTypes.UPDATE_PET_START:
      return { ...state, loading: true, errors: null };
    case actionTypes.UPDATE_PET_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null,
        // pet: Object.assign(state.pet, action.pet),
        pet: { ...state.pet, ...action.pet }
      };
    case actionTypes.UPDATE_PET_FAILED:
      return { ...state, loading: false, errors: action.errors };
    default:
      return state;
  }
};

export default reducer;
