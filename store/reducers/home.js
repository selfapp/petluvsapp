import * as actionTypes from "../actions/actionTypes";

const initialState = {
  data: [],
  loading: false,
  refreshing: false,
  totalPage: 1,
  backButton: false,
  headerTitle: null,
  notificationsCount: 0
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_HOME_DATA_START:
      return {
        ...state,
        loading: !action.refreshing,
        errors: null,
        refreshing: action.refreshing,
        data: [...state.data]
      };

    case actionTypes.FETCH_HOME_DATA_SUCCESS:
      let fetchedData = state.data;
      if (action.refreshing) {
        fetchedData = []
        fetchedData = [...action.data]
      } else {
        fetchedData = [...state.data, ...action.data]
      }

      return {
        ...state,
        loading: false,
        errors: null,
        refreshing: false,
        totalPage: action.pages,
        data: fetchedData
      };

    case actionTypes.FETCH_HOME_DATA_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors,
        refreshing: false
      };

    case actionTypes.BACK_BUTTON_REQUIRED:
      return {
        ...state,
        backButton: true
      };

    case actionTypes.BACK_BUTTON_NOT_REQUIRED:
      return {
        ...state,
        backButton: false
      };

    case actionTypes.SET_HEADER_TITLE:
      return {
        ...state,
        headerTitle: action.title
      };

    case actionTypes.REMOVE_HEADER_TITLE:
      return {
        ...state,
        headerTitle: null
      };

    case actionTypes.INCREMENT_NOTIFICATIONS_COUNT:
      return {
        ...state,
        notificationsCount: state.notificationsCount + 1
      };

    case actionTypes.RESET_NOTIFICATIONS_COUNT:
      return {
        ...state,
        notificationsCount: 0
      };

    default:
      return state;
  }
};

export default reducer;
