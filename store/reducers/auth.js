import {
  AUTH_FAILED,
  AUTH_SUCCESS,
  AUTH_START,
  AUTH_LOGOUT
} from "../actions/actionTypes";

const initialState = {
  authInfo: null,
  errorMsg: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_START:
      return {
        ...state,
        token: null,
        errors: null,
        loading: true,
        authInfo: null,
        errorMsg: null
      };
    case AUTH_SUCCESS:
      return {
        ...state,
        token: action.token,
        errors: null,
        errorMsg: null,
        loading: false,
        authInfo: action.authInfo,
        confirmed: action.confirmed,
      };
    case AUTH_FAILED:
      return {
        ...state,
        token: null,
        errors: action.errorMsg,
        loading: false,
        authInfo: null,
        errorMsg: action.errorMsg
      };
    case AUTH_LOGOUT:
      return {};
    default:
      return state;
  }
};

export default reducer;
