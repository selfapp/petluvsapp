import {
  SEARCH_SERVICE_START,
  SEARCH_SERVICE_SUCCESS,
  SEARCH_SERVICE_FAILED
} from "../actions/actionTypes";

const initialState = {
  data: [],
  loading: true,
  errors: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_SERVICE_START:
      return {
        ...state,
        loading: true,
        errors: null,
      };

    case SEARCH_SERVICE_SUCCESS:
      return {
        ...state,
        loading: false,
        errors: null,
      };
    case SEARCH_SERVICE_FAILED:
      return {
        ...state,
        loading: false,
        errors: action.errors,
      };

    default:
      return state;
  }
};

export default reducer;
