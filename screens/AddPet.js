import React from "react";
import {connect} from "react-redux";
import {
  Text,
  View,
  ScrollView,
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Alert
} from "react-native";
import CheckBox from "react-native-checkbox";
import {ImagePicker, Permissions} from "expo";
import {reduxForm, Field} from "redux-form";
import {NavigationEvents} from "react-navigation";


import Header from "../components/Header";
import {addPet, petSpeciesBreed, fetchAllPets} from "../store/actions/index";
import {
  setHeaderTitle,
  enableBackButton,
  disableBackButton
} from "../store/actions";
import Spinner from "../components/UI/activityIndicator";
import MyTextInput from "./../components/UI/MyTextInput";
import Dropdowns from "./../components/UI/Dropdowns";
import MyDatePicker from "./../components/UI/MyDatePicker";

const {height} = Dimensions.get("window");

class AddPet extends React.Component {
  // static navigationOptions = {
  //   header: null,
  //   gesturesEnabled: false,
  //   tabBarVisible: true
  // };
  speciesName;

  componentWillReceiveProps(nextProps) {
    const {errors, added} = nextProps;
    if (added) {
      this.props.fetchAllPets();
      this.props.navigation.navigate("Profile");
    }
    if (Array.isArray(errors) && errors.length > 0) {
      // Need to show erro message in here
      Alert.alert("Error ", errors );
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      petName: false,
      petSpecies: false,
      petBreed: false,
      petAttributes: {
        insured: false,
        fixed: false,
        fullyVaccinated: false
      },
      date: null,
      image: null,
      loading: false,
      modalVisible: false,
      species: [],
      breeds: []
    };
  }

  componentDidMount() {
    if (this.props.speciesAndBreeds.length === 0) {
      this.props.petSpeciesBreed(this.makeArrayOfSpecies.bind(this));
    } else {
      this.makeArrayOfSpecies();
    }
  }

  breeds = oValue => {
    const breedsArray = [];
    this.props.speciesAndBreeds.map(value => {
      if(value.name === oValue) {
        value.children.forEach((breed) => breedsArray.push(breed.name));
      }
    });

    this.setState({breeds: breedsArray});
  };

  makeArrayOfSpecies() {
    const species = [];
    this.props.speciesAndBreeds.map(item => {
      species.push(item.name);
      return null;
    });

    this.setState({species});
  }

  _pickImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3]
      // base64: true
    });

    if (!result.cancelled) {
      this.setState({image: result.uri});
    }
  };

  dateHandler = date => {
    this.setState({date});
  };

  renderHeader = () => (
    <Header navigation={this.props.navigation} title={"Add New Pet"}/>
  );

  render() {
    let isValid = false;
    const {petName, petSpecies, petBreed, date} = this.state;
    if ((petName && petSpecies, petBreed, date)) {
      isValid = true;
    }

    const minValue = min => value =>
      value && value < min ? `Must be at least ${min}` : undefined;
    const minValue2 = minValue(2);

    const isFalse = value =>
      value && value.length < 1
        ? "You do not meet the minimum age requirement!"
        : undefined;

    if (this.props.loading === true) {
      return <Spinner/>;
    }
    return (
      <View style={{flex: 1, backgroundColor: "#fff"}}>
        {/*{this.renderHeader()}*/}

        <NavigationEvents
          onWillFocus={payload => {
            this.props.setHeaderTitle('Add New Pet');
            setTimeout(() => {
              this.props.enableBackButton();
            }, 300);
          }}
          onWillBlur={() => this.props.disableBackButton()}
        />
        <View style={styles.seperator}/>

        <ScrollView
          style={{flex: 1}}
          showsVerticalScrollIndicator={false}
          removeClippedSubviews
        >
          <View>
            <Text style={styles.textLabel}>Pet's Name</Text>
            <Field
              name={"name"}
              component={MyTextInput}
              fontSize={14}
              style={styles.TextInput}
              placeholder="Bella, Oliver, Spot etc."
              placeholderTextColor="gray"
              autoCorrect={false}
              showWarning
              autoCapitalize="none"
              selectionColor="gray"
              underlineColorAndroid="transparent"
              onChange={value =>
                this.setState({...this.state, petName: value})
              }
            />
          </View>
          <View>
            <Text style={styles.textLabel}>Pet Species</Text>

            <Field
              name={"speciesOptions"}
              component={Dropdowns}
              style={{
                width: "100%",
                backgroundColor: "white",
                height: height * 0.07,
                justifyContent: "center"
              }}
              options={this.state.species}
              dropdownStyle={{
                width: "90%",
                justifyContent: "center",
                marginTop: 10
              }}
              dropdownTextStyle={{fontSize: 14}}
              textStyle={{paddingLeft: 10, color: "grey", fontSize: 14}}
              enableEmptySections
              type={"species"}
              onChange={value => {
                this.setState({
                  petSpecies: this.state.species[value]
                });
                this.breeds(this.state.species[value]);
              }}
              defaultValue={"Choose one"}
            />
            <View>
              <Text style={styles.textLabel}>Pet Breed</Text>
              <Field
                name={"breedOptions"}
                component={Dropdowns}
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  height: height * 0.07,
                  justifyContent: "center"
                }}
                options={this.state.breeds}
                dropdownStyle={{
                  width: "90%",
                  justifyContent: "center",
                  marginTop: 10
                }}
                dropdownTextStyle={{fontSize: 14}}
                textStyle={{paddingLeft: 10, color: "grey", fontSize: 14}}
                type={"breed"}
                onChange={i =>
                  this.setState({
                    ...this.state,
                    petBreed: this.state.breeds[i]
                  })
                }
                defaultValue={"Must choose species first"}
              />
            </View>
            <View>
              <Text style={styles.textLabel}>Pet Birthdate</Text>
              <Field
                name={"datetime1"}
                component={MyDatePicker}
                style={{backgroundColor: "white", width: "100%"}}
                date={this.state.date}
                mode="date"
                dateHandler={this.dateHandler}
                placeholder="select date"
                minDate="1990-01-01"
                maxDate="2019-01-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                iconSource={require("../assets/startTimeFieldIcon.png")}
                customStyles={{
                  dateIcon: {
                    position: "absolute",
                    left: 10,
                    width: 18,
                    height: 18,
                    marginLeft: 0,
                    alignItems: "center"
                  },
                  dateInput: {
                    backgroundColor: "white",
                    borderColor: "white",
                    paddingRight: "50%"
                  }
                }}
              />
            </View>
            <View>
              <Text style={{marginTop: 15, marginLeft: 15}}>
                Pet Attributes
              </Text>
              <Text style={{fontSize: 10, marginLeft: 15, opacity: 0.5}}>
                Check all that apply
              </Text>
              <View
                style={{
                  marginHorizontal: 15,
                  marginTop: 10,
                  marginBottom: 10
                }}
              >
                <CheckBox
                  label="Insured"
                  checkedImage={require("../assets/check.png")}
                  uncheckedImage={require("../assets/uncheck.png")}
                  onChange={checked =>
                    this.setState({
                      petAttributes: {
                        ...this.state.petAttributes,
                        insured: checked
                      }
                    })
                  }
                />

                <CheckBox
                  label="Fixed"
                  checkedImage={require("../assets/check.png")}
                  uncheckedImage={require("../assets/uncheck.png")}
                  onChange={checked =>
                    this.setState({
                      petAttributes: {
                        ...this.state.petAttributes,
                        fixed: checked
                      }
                    })
                  }
                />
                <CheckBox
                  label="Fully vaccinated"
                  checkedImage={require("../assets/check.png")}
                  uncheckedImage={require("../assets/uncheck.png")}
                  onChange={checked =>
                    this.setState({
                      petAttributes: {
                        ...this.state.petAttributes,
                        fullyVaccinated: checked
                      }
                    })
                  }
                />
              </View>
            </View>
            {this.state.image ? (
              <View style={{alignItems: "center"}}>
                <Image
                  style={{width: 100, height: 100}}
                  source={{uri: this.state.image}}
                />
              </View>
            ) : null}
            <View>
              <TouchableOpacity onPress={this._pickImage}>
                <Image
                  style={styles.profileImage}
                  source={require("../assets/group2.png")}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              disabled={!isValid}
              onPress={() => this.addPethandler()}
            >
              <View style={[styles.ButtonView]} opacity={!isValid ? 0.5 : 1}>
                <Text style={{color: "#FFFFFF", fontSize: 16}}>Add Pet</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }

  // componentWillMount(){
  //   AsyncStorage.clear();
  // }
  //need to pass the data
  addPethandler = () => {
    const newPet = {
      name: this.state.petName,
      species: this.state.petSpecies,
      breed: this.state.petBreed,
      birthday: this.state.date,
      attributes: this.state.petAttributes,
      image: this.state.image
    };
    if (
      this.state.petName &&
      this.state.petSpecies &&
      this.state.petBreed &&
      this.state.date
    ) {
      this.props.addPet(newPet);
    }
  };

  _pickImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3]
      // base64: true
    });

    if (!result.cancelled) {
      this.setState({image: result.uri});
    }
  };

  dateHandler = date => {
    this.setState({date});
  };

  renderHeader = () => (
    <Header navigation={this.props.navigation} title={"Add New Pet"}/>
  );
}

const mapStateToProps = state => ({
  loading: state.pets.loading,
  errors: state.pets.errors,
  added: state.pets.added,
  speciesAndBreeds: state.playDates.speciesAndBreeds
});

const AddPetForm = reduxForm({
  form: "AddPetForm",
  validate: values => {
    const errors = {};

    if (!values.name) {
      errors.name = "Name field is required";
    } else if (values.name.length < 3) {
      errors.name = "Name field must be at least 3 characters long.";
    }

    errors.datetime1 = !values.datetime1
      ? "Please specify a date and time"
      : undefined;
    errors.speciesOptions = !values.speciesOptions
      ? "Please choose one of the pet species"
      : undefined;
    errors.breedOptions = !values.breedOptions
      ? "Please choose one of the pet breed"
      : undefined;
    return errors;
  }
})(AddPet);
export default connect(
  mapStateToProps,
  {addPet, petSpeciesBreed, fetchAllPets,setHeaderTitle,
    enableBackButton,
    disableBackButton}
)(AddPetForm);

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    height: "100%",
    width: "100%"
  },
  textLabel: {
    margin: 15
  },
  validation: {
    fontSize: 10
  },
  dropDown: {
    position: "absolute",
    right: 10,
    alignSelf: "center"
  },
  TextInput: {
    flex: 1,
    height: height * 0.07,
    backgroundColor: "#FFFFFF",
    color: "gray",
    paddingLeft: 10
  },
  profileImage: {
    flex: 1,
    resizeMode: "contain",
    width: "85%",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  ButtonView: {
    backgroundColor: "#e55595",
    height: height * 0.07,
    borderRadius: 5,
    marginBottom: 10,
    marginHorizontal: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10
  }
});
