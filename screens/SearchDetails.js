import React, {Component} from "react";
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet,
    Keyboard,
    FlatList,
    ImageBackground,
    AsyncStorage,
    Dimensions,
    ScrollView,
    SafeAreaView,
    Platform,
    Linking
} from "react-native";
import ImageSlider from "react-native-image-slider";
import StarRating from "react-native-star-rating";
import MaterialTabs from "react-native-material-tabs";
import SearchImages from "../components/SearchImages";
import {baseurl} from "./../BaseUrl";
import {PlaceDetailsModel} from "../model/placeDetails";

const PLACE_DETAILS = `${baseurl}/places/details`;
const SERVICE_PROVIDER_DETAILS_URL = `${baseurl}/users/service-provider-details`;

var {height} = Dimensions.get("window");
var height1 = (((height / 2 / height) * 100 + 3) / 100) * height;

export default class SearchDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            distance: "1.2 km",
            closingTime: "8 pm",
            starCount: 0,
            selectedTab: 0,
            businessName: "",
            servicesOffered: [],
            address: "",
            description: "",
            venueDetails: {
                name: "",
                description: "",
                venuePhotos: [],
                category: "",
                formattedAddress: "",
                tipsList: [],
                formattedPhone: "",
                canonicalUrl: "",
                count: 0,
                status: "",
                categories: []
            },
            isdataReady: false
        };
    }

    static navigationOptions = {
        header: null,
        gesturesEnabled: false
    };

    onStarRatingPress(rating) {
        this.setState({
            starCount: rating
        });
    }

    setTab(tab) {
        this.setState({selectedTab: tab});
    }

    componentDidMount() {
        this.loadData();
    }

    // Loading initial data
    loadData = async () => {
        const userToken = await AsyncStorage.getItem("token");
        const {navigation} = this.props;
        const placeId = navigation.getParam("placeId");
        const serviceProviderId = navigation.getParam("serviceProviderId");
        const inAppServiceProvider = navigation.getParam("inAppServiceProvider");
        const searchKey = navigation.getParam("searchKey");
        const distance = navigation.getParam("distance");
        await this.setState({
            ...this.state,
            distance: distance
        })

        let URL = `${PLACE_DETAILS}/${placeId}`;
        if (inAppServiceProvider) {
            URL = `${SERVICE_PROVIDER_DETAILS_URL}/${serviceProviderId}?serviceType=${searchKey}`;
        }

        try {
            const response = await fetch(URL, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${userToken}`
                }
            });
            const responseJson = await response.json();

            if (response.status == 200) {
                const placeDetailsObj = new PlaceDetailsModel(responseJson.response.data);
                const venueDetails = placeDetailsObj.venueDetails;
                await this.setState({
                    ...this.state,
                    venueDetails: {
                        ...this.state.venueDetails,
                        ...venueDetails
                    }
                });

                this.isdataReady = true;
            }
        } catch (e) {
            // console.log("Error while fetching details data", e);
        }
    };

    tabView = () => {
        let {
            description,
            categories,
            tipsList,
            formattedAddress,
            formattedPhone,
            canonicalUrl
        } = this.state.venueDetails;

        if (this.state.selectedTab === 0) {
            return (
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={false}
                    style={{width: "100%"}}
                >
                    <View style={styles.playdatehost}>
                        <Text style={{fontSize: 11, color: "black"}}>{description}</Text>
                    </View>
                    <Text style={{marginLeft: 20, marginTop: 20, color: "grey"}}>
                        Services Offered
                    </Text>

                    {categories.map((item, key) => (
                        <View key={key}>
                            <TouchableOpacity style={{flexDirection: "row"}}>
                                <View style={styles.bringPetsSrollview}>
                                    <Text
                                        style={{
                                            // color: item.color,
                                            fontSize: 10,
                                            marginLeft: 10
                                        }}
                                    >
                                        {item.shortName}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    ))}

                </ScrollView>
            );
        }
        if (this.state.selectedTab === 2) {
            return (
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={false}
                    style={{width: "100%"}}
                >
                    <FlatList
                        data={tipsList}
                        renderItem={({item, index}) => (
                            <View
                                style={{
                                    marginTop: 10
                                }}
                            >
                                <View
                                    style={{
                                        marginTop: 10,
                                        marginHorizontal: 20,
                                        marginRight: 10,
                                        flexDirection: "row"
                                    }}
                                >
                                    <View style={{width: "70%"}}>
                                        <Text style={{fontSize: 12, textAlign: "left"}}>
                                            {item.author_name}
                                        </Text>
                                    </View>
                                    <View style={{width: "30%"}}>
                                        <StarRating
                                            disabled={false}
                                            maxStars={5}
                                            rating={item.rating}
                                            selectedStar={rating => this.onStarRatingPress(rating)}
                                            starSize={15}
                                            starColor={"#fff"}
                                            fullStarColor={"#e55595"}
                                        />
                                    </View>
                                </View>
                                <View style={{marginTop: 10, marginHorizontal: 20}}>
                                    <Text style={{fontSize: 11, color: "grey"}}>
                                        {item.text}
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        width: "100%",
                                        height: 2,
                                        marginTop: 10,
                                        backgroundColor: "grey",
                                        opacity: 0.2
                                    }}
                                />
                            </View>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </ScrollView>
            );
        }
        if (this.state.selectedTab === 3) {
            return (
                <ScrollView showsHorizontalScrollIndicator={false} horizontal={false}>
                    <SearchImages/>
                </ScrollView>
            );
        }
        if (this.state.selectedTab === 1) {
            return (
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    horizontal={false}
                    style={{width: "100%"}}
                >
                    <View
                        style={{
                            marginHorizontal: 20,
                            height: 70,
                            flexDirection: "row",
                            alignItems: "center"
                        }}
                    >
                        <Image source={require("../assets/addressIcon.png")}/>
                        <Text
                            style={{
                                fontSize: 14,
                                color: "#e55595",
                                alignItems: "center",
                                marginLeft: 40
                            }}
                        >
                            {formattedAddress}
                        </Text>
                    </View>
                    <View
                        style={{
                            width: "100%",
                            height: 2,
                            backgroundColor: "grey",
                            opacity: 0.2
                        }}
                    />
                    <View
                        style={{
                            marginHorizontal: 20,
                            height: 70,
                            flexDirection: "row",
                            alignItems: "center"
                        }}
                    >
                        <Image source={require("../assets/phoneIcon.png")}/>
                    <TouchableOpacity
                        onPress={()=> {Linking.openURL('tel:' + formattedPhone);}}
                     >
                        <Text
                            style={{
                                fontSize: 14,
                                color: "#e55595",
                                alignItems: "center",
                                marginLeft: 40
                            }}
                        >
                            {formattedPhone}
                        </Text>
                     </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            width: "100%",
                            height: 2,
                            backgroundColor: "grey",
                            opacity: 0.2
                        }}
                    />
                    <View
                        style={{
                            marginHorizontal: 20,
                            height: 70,
                            flexDirection: "row",
                            alignItems: "center"
                        }}
                    >
                        <Image source={require("../assets/websiteIcon.png")}/>
                        <TouchableOpacity
                            onPress={()=> {Linking.openURL('' + canonicalUrl);}}
                        >
                        <Text
                            style={{
                                fontSize: 14,
                                color: "#e55595",
                                alignItems: "center",
                                marginLeft: 40
                            }}
                        >
                            {canonicalUrl}
                        </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            width: "100%",
                            height: 2,
                            backgroundColor: "grey",
                            opacity: 0.2
                        }}
                    />
                    <View
                        style={{
                            width: "100%",
                            height: 2,
                            backgroundColor: "grey",
                            opacity: 0.2
                        }}
                    />
                </ScrollView>
            );
        }
    };

    render() {
        let {
            venuePhotos,
            name,
            category,
            status,
            count
        } = this.state.venueDetails;
        const {goBack} = this.props.navigation;

        if (this.isdataReady === false) {
            return null;
        } else {
            return (
                <SafeAreaView style={styles.container}>
                    <View style={styles.imagesliderContainer}>
                        <ImageSlider
                            loopBothSides
                            images={venuePhotos}
                            customSlide={({index, item, style, width}) => (
                                // It's important to put style here because it's got offset inside
                                <View key={index} style={[style, styles.customSlide]}>
                                    <ImageBackground
                                        source={{uri: item, cache: "force-cache"}}
                                        style={styles.customImage}
                                    />
                                </View>
                            )}
                        />
                        <View style={styles.header}>
                            <TouchableOpacity
                                style={styles.iconLeftStyle}
                                onPress={() => {
                                    Keyboard.dismiss();
                                    goBack();
                                }}
                            >
                                <Image
                                    style={styles.backButton}
                                    source={require("../assets/backIcon.png")}
                                />
                            </TouchableOpacity>
                            <Text
                                style={{
                                    height: 40,
                                    fontSize: 18,
                                    color: "#e55595",
                                    alignItems: "center"
                                }}
                            />
                            <TouchableOpacity style={[styles.iconRightStyle, {bottom: 5}]}>
                                <Image source={require("../assets/notificationIcon.png")}/>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.content2}>
                        <View style={{flex: 3, paddingLeft: 20}}>
                            <Text style={{color: "#fff", fontSize: 12}}>{name}</Text>
                            <View style={{flexDirection: "row"}}>
                                <Text style={{color: "#fff", fontSize: 10, marginTop: 10}}>
                                    {category}
                                </Text>
                                <Text
                                    style={{
                                        color: "#fff",
                                        fontSize: 10,
                                        marginTop: 10,
                                        marginLeft: 15
                                    }}
                                >
                                    {this.state.distance}
                                </Text>
                                <Text
                                    style={{
                                        color: "#fff",
                                        fontSize: 10,
                                        marginTop: 10,
                                        marginLeft: 15
                                    }}
                                >
                                    {status}
                                </Text>
                            </View>
                        </View>
                        <View style={{flex: 1}}>
                            <Text
                                style={{
                                    color: "#fff",
                                    fontSize: 10,
                                    marginTop: 10,
                                    marginLeft: 15
                                }}
                            >
                                {count}
                                .0
                            </Text>
                            <View style={{marginTop: 10, marginRight: 10}}>
                                <StarRating
                                    disabled
                                    maxStars={5}
                                    rating={count}
                                    // selectedStar={rating => this.onStarRatingPress(rating)}
                                    starSize={15}
                                    // emptyStarColor={"white"}
                                    starColor={"white"}
                                    fullStarColor={"#56C1EF"}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={styles.Tabs}>
                        <MaterialTabs
                            // items={['Details','Contacts', 'Reviews', 'Photos']}
                            items={["Details", "Contacts", "Reviews"]}
                            selectedIndex={this.state.selectedTab}
                            onChange={this.setTab.bind(this)}
                            barColor="white"
                            textStyle={{fontSize: 10}}
                            indicatorColor="#EF5595"
                            activeTextColor="#EF5595"
                            inactiveTextColor="#33212D"
                        />
                    </View>
                    <View style={{flex: 1}}>{this.tabView()}</View>
                </SafeAreaView>
            );
        }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F5FCFF"
    },
    imagesliderContainer: {
        height: "45%"
    },
    slider: {backgroundColor: "#000", height: 350},
    content1: {
        width: "100%",
        height: 50,
        marginBottom: 10,
        backgroundColor: "#000",
        justifyContent: "center",
        alignItems: "center"
    },
    content2: {
        width: "100%",
        height: "10%",
        // marginTop: 10,
        backgroundColor: "#e55595",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    contentText: {color: "#fff"},
    buttons: {
        zIndex: 1,
        height: 15,
        // marginTop: -25,
        marginBottom: 10,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    button: {
        margin: 3,
        width: 15,
        height: 15,
        opacity: 0.9,
        alignItems: "center",
        justifyContent: "center"
    },
    buttonSelected: {
        opacity: 1,
        color: "red"
    },
    customSlide: {
        backgroundColor: "green",
        alignItems: "center",
        justifyContent: "center"
    },
    customImage: {
        width: "100%",
        height: "100%"
    },
    Tabs: {
        borderTopColor: "rgba(217, 214, 216, .2)",
        borderTopWidth: 2,
        borderStyle: "solid",
        zIndex: 9999,
        marginTop: -2
    },
    header: {
        position: "absolute",
        alignItems: "center",

        // justifyContent: 'flex-end',
        height: 60,
        elevation: 5,
        top: 10,
        left: 10,
        backgroundColor: "red"
    },
    backButton: {
        height: "50%",
        width: "15%"
    },
    title: {
        height: 40,
        fontSize: 20,
        color: "#e55595"
    },
    iconRightStyle: {
        position: "absolute",
        right: 10,
        bottom: Platform.OS === "ios" ? 2 : 0, //-3:0
        width: 50,
        height: 40,
        alignItems: "center",
        justifyContent: "center"
    },
    iconLeftStyle: {
        position: "absolute",
        left: 0,
        bottom: 5,
        zIndex: 2,
        width: 50,
        height: 40,
        alignItems: "center",
        justifyContent: "center"
    },
    bringPetsSrollview: {
        marginLeft: 20,
        flexDirection: "row",
        marginTop: 10
    },
    playdatehost: {
        flexDirection: "row",
        marginHorizontal: 20,
        marginTop: 10
    },
    addPicture: {
        position: "absolute",
        right: 10,
        backgroundColor: "#fff",
        height: 30,
        width: 30,
        borderWidth: 30,
        borderRadius: 30,
        borderColor: "#fff",
        bottom: height1
    }
});
