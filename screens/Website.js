import React, {Component} from "react";
import {View, WebView, Text, TouchableOpacity} from "react-native";
import {StackActions} from "react-navigation";

const popAction = StackActions.pop({
  n: 1
});

export default class Website extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={{height: 10}}>
          <TouchableOpacity
            onPress={() => this.props.navigation.dispatch(popAction)}
            style={{justifyContent: "center"}}
          >
            <Text style={{fontSize: 10, marginLeft: 10}}>Go Back</Text>
          </TouchableOpacity>
        </View>
        <WebView
          source={{uri: "https://www.petluvs.com/"}}
          style={{marginTop: 10}}
        />
      </View>
    );
  }
}
