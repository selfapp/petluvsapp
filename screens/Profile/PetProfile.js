import React from "react";
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from "react-native";
import MaterialTabs from "react-native-material-tabs";
import CheckBox from "react-native-checkbox";
import {Camera, ImagePicker, Permissions} from "expo";
import {reduxForm, Field} from "redux-form";
import connect from "react-redux/es/connect/connect";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import {NavigationEvents} from "react-navigation";
import moment from "moment";

import {
  fetchPetWithId,
  updatePet,
  petSpeciesBreed,
  uploadPetMedia,
  fetchAllPets,
  enableBackButton,
  setHeaderTitle,
  disableBackButton,
  removeHeaderTitle
} from "../../store/actions";
import SearchImages from "../../components/SearchImages";
import MyDatePicker from "./../../components/UI/MyDatePicker";
import Spinner from "../../components/UI/activityIndicator";
import ModalSpinner from "../../components/UI/ModalActivityIndicator";

import {PETLUVS_DEFAULT_IMAGE} from "../../utility/utils";


const speciesOptions = ["Cat", "Dog", "Bird", "Rabbit"];

const {height} = Dimensions.get("window");

class PetProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: this.props.navigation.state.params.tab ? this.props.navigation.state.params.tab : 0,
      name: this.props.pet.name,
      breed: this.props.pet.breed,
      species: this.props.navigation.state.params.species,
      petSpecies: false,
      birthday: this.props.pet.birthday,
      petBreed: [this.props.pet.breed],
      attributes: {
        insured: this.props.pet.attributes
          ? this.props.pet.attributes.insured
          : false,
        fixed: this.props.pet.attributes
          ? this.props.pet.attributes.fixed
          : false,
        fullyVaccinated: this.props.pet.attributes
          ? this.props.pet.attributes.fullyVaccinated
          : false
      },
      multiSpecies: [],
      image: null,
      medioToUpload: null
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.loading === false && Object.keys(props.pet).length > 0) {
      return {
        image:
          props.pet.imagePath[0] && props.pet.imagePath[0].url !== null
            ? props.pet.imagePath[0].url
            : null
      };
    }
    return null;
  }


  componentDidMount() {
    this.loadData();
  }

  loadData = async () => {
    if (this.props.speciesAndBreeds.length === 0) {
      this.props.petSpeciesBreed(this.makeArrayOfSpeciesAndBreeds.bind(this));
    } else {
      this.makeArrayOfSpeciesAndBreeds();
    }
    this.props.navigation.state.params.tab
      ? this.setState({selectedTab: this.props.navigation.state.params.tab})
      : console.log("tab${this.props.navigation.state.params.tab}");
    this.props.fetchPetWithId(this.props.navigation.state.params._id);
  }

  updatePropsData() {
    this.setState({
      name: this.props.pet.name,
      breed: this.props.pet.breed,
      species: this.props.pet.species,
      birthday: this.props.pet.birthday,
      petBreed: [this.props.pet.breed],
      attributes: {
        insured: this.props.pet.attributes
          ? this.props.pet.attributes.insured
          : false,
        fixed: this.props.pet.attributes
          ? this.props.pet.attributes.fixed
          : false,
        fullyVaccinated: this.props.pet.attributes
          ? this.props.pet.attributes.fullyVaccinated
          : false
      },
    })
  }


  setTab(tab) {
    this.updatePropsData();
    this.setState({selectedTab: tab});
  }

  addPet = () => {
    const {navigate} = this.props.navigation;
    navigate("AddPet");
  };

  breeds = value => {
    const breedsArray = [];
    breedsArray.push(
      this.props.speciesAndBreeds[value].breeds.map(value => {
        breedsArray.push(value);
      })
    );

    this.setState({breeds: breedsArray});
  };

  makeArrayOfSpeciesAndBreeds() {
    const result = [];
    this.props.speciesAndBreeds.map(item => {
      if (this.state.species === item.name) {
        item.children.map(breed => {
          result.push({
            name: breed.name,
            id: breed.name
          });
        });
      }
    });

    this.setState({multiSpecies: result});
  }

  pickProfileImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [1, 1]
    });

    if (!result.cancelled) {
      console.log(result);
      // this.setState({image: result.uri});
    }
  }

  _pickImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: "All",
      allowsEditing: true,
      aspect: [1, 1]
    });

    if (!result.cancelled) {
      const ext = result.uri.split(".").pop();
      let mediaType = `image/jpeg`;
      if (ext === "mov") {
        mediaType = `video/quicktime`;
      }

      const index = result.uri.lastIndexOf("/") + 1;
      const filename = result.uri.substr(index);

      const mediaToUpload = {
        uri: result.uri,
        type: mediaType,
        name: filename
      };

      await this.setState({
        ...this.state,
        mediaToUpload
      });

      // this.function will upload pet media.
      this.props.uploadPetMedia(this.props.navigation.state.params._id, mediaToUpload);
    }
  };
  dateHandler = date => {
    this.setState({birthday: date});
  };

  saveValueinState = text => {
    this.setState({petName: text});
  };

  // Save final pet updation data
  updatePetHandler = () => {
    const data = {
      name: this.state.name,
      species: this.state.petSpecies,
      breed: this.state.petBreed,
      birthday: this.state.birthday,
      attributes: this.state.attributes
    };
    const petId = this.props.pet._id;
    this.props.updatePet(petId, data);
  };

  onChangeOptionHandler = async i => {
    await this.setState({species: speciesOptions[i]});
  };

  selectedTab = () => {
    if (this.state.selectedTab === 0) {
      let canShare = true;
      if (this.props.navigation.state.params.static)
        canShare = false;
      return (
        <View style={{flex: 1}}>
          {this.props.navigation.state.params.static !== true ?
            (<TouchableOpacity
              style={{
                height: 40,
                flexDirection: "row",
                justifyContent: "center"
              }}
              onPress={this._pickImage}
            >
              <Image
                source={require("../../assets/addPhotoIcon.png")}
              />
              <Text
                style={{color: "#EF5595", fontSize: 16, marginLeft: 10}}
              >
                Add Photo or Video
              </Text>
            </TouchableOpacity>) : (null)}

          <ScrollView
            showsVerticalScrollIndicator={false}
            removeClippedSubviews
          >
            <SearchImages
              media={(this.props.pet.media && this.props.pet.media.length) ? this.props.pet.media : null}
              canShare={canShare}
            />
          </ScrollView>
        </View>
      );
    }
    if (this.state.selectedTab === 1) {
      return (
        <View style={{flex: 1, backgroundColor: "#fff"}}>
          <ScrollView
            style={{flex: 1}}
            showsVerticalScrollIndicator={false}
            removeClippedSubviews
          >
            <View>
              <Text style={styles.textLabel}>Pet Species</Text>
              {this.props.navigation.state.params.static !== true ? (
                <View style={[styles.ViewShadow]}>
                  <SectionedMultiSelect
                    single
                    items={this.state.multiSpecies}
                    uniqueKey="id"
                    // subKey={"children"}
                    showCancelButton
                    modalWithSafeAreaView
                    modalWithTouchable
                    showChips
                    styles={{button: {backgroundColor: "#e55595"}}}
                    // showDropDowns
                    // readOnlyHeadings
                    expandDropDowns={true}
                    onSelectedItemsChange={selectedItem =>
                      this.setState({
                        petBreed: selectedItem,
                        breed: selectedItem
                      })
                    }
                    selectedItems={this.state.petBreed}
                  />
                </View>) : (
                <View style={styles.ViewShadow}>
                  <Text style={{marginLeft: 10, fontSize: 15, color: "grey"}}> {this.state.breed}</Text>
                </View>
              )}

              <View>
                <Text style={styles.textLabel}>Pet Birthdate</Text>
                {this.props.navigation.state.params.static !== true ? (
                  <Field
                    name={"datetime1"}
                    component={MyDatePicker}
                    style={{backgroundColor: "white", width: "100%"}}
                    date={this.state.birthday}
                    mode="date"
                    dateHandler={this.dateHandler}
                    placeholder="select date"
                    // format="YYYY-MM-DD"
                    minDate="1990-01-01"
                    maxDate="2019-01-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    iconSource={require("../../assets/startTimeFieldIcon.png")}
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 10,
                        width: 18,
                        height: 18,
                        marginLeft: 0,
                        alignItems: "center"
                      },
                      dateInput: {
                        backgroundColor: "white",
                        borderColor: "white",
                        paddingRight: "50%"
                      }
                    }}
                  />
                ) : (
                  <View style={styles.ViewShadow}>
                    <Text
                      style={{marginLeft: 10, fontSize: 15, color: "grey"}}
                    >
                      {moment(this.state.birthday).format("MM/DD/YYYY")}
                    </Text>
                  </View>
                )}
              </View>
              <View>
                <Text style={{marginTop: 15, marginLeft: 15}}>
                  Pet Attributes
                </Text>
                {this.props.navigation.state.params.static !== true ? (
                  <Text style={{fontSize: 10, marginLeft: 15, opacity: 0.5}}>
                    Check all that apply
                  </Text>
                ) : null}
                <View
                  style={{
                    marginHorizontal: 15,
                    marginTop: 10,
                    marginBottom: 10
                  }}
                >
                  <CheckBox
                    label="Insured"
                    checked={this.state.attributes.insured}
                    checkedImage={require("../../assets/check.png")}
                    uncheckedImage={require("../../assets/uncheck.png")}
                    onChange={checked =>
                      this.props.navigation.state.params.static !== true
                        ? this.setState({
                          attributes: {
                            ...this.state.attributes,
                            insured: !this.state.attributes.insured
                          }
                        })
                        : null
                    }
                  />

                  <CheckBox
                    label="Fixed"
                    checked={this.state.attributes.fixed}
                    checkedImage={require("../../assets/check.png")}
                    uncheckedImage={require("../../assets/uncheck.png")}
                    onChange={checked =>
                      this.props.navigation.state.params.static !== true
                        ? this.setState({
                          attributes: {
                            ...this.state.attributes,
                            fixed: !this.state.attributes.fixed
                          }
                        })
                        : null
                    }
                  />

                  <CheckBox
                    label="Fully vaccinated"
                    checked={this.state.attributes.fullyVaccinated}
                    checkedImage={require("../../assets/check.png")}
                    uncheckedImage={require("../../assets/uncheck.png")}
                    onChange={checked =>
                      this.props.navigation.state.params.static !== true
                        ? this.setState({
                          attributes: {
                            ...this.state.attributes,
                            fullyVaccinated: !this.state.attributes
                              .fullyVaccinated
                          }
                        })
                        : null
                    }
                  />
                </View>
              </View>
              {/* <TouchableOpacity onPress={() => this.props.navigation.goBack()}> */}
              {this.props.navigation.state.params.static !== true ? (
                <TouchableOpacity onPress={() => this.updatePetHandler()}>
                  <View style={styles.ButtonView}>
                    <Text style={{color: "#FFFFFF", fontSize: 16}}>
                      Save Pet
                    </Text>
                  </View>
                </TouchableOpacity>
              ) : null}
            </View>
          </ScrollView>
        </View>
      );
    }
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: "white"}}>
        <NavigationEvents
          onWillFocus={() => {
            this.props.enableBackButton();
            this.props.setHeaderTitle(this.props.pet.name);
          }}
          onWillBlur={() => {
            this.props.disableBackButton();
            this.props.removeHeaderTitle();
          }}
        />
        <View
          style={{
            alignItems: "center",
            paddingBottom: 10,
            backgroundColor: "white",
            zIndex: 999,
            marginTop: 20
          }}
        >
          <TouchableOpacity style={styles.Circle} onPress={() => this.pickProfileImage() }>
            { 
              this.state.image ? <Image style={{width: 100, height: 100}} source={{uri: this.state.image}} />
              : <Image source={PETLUVS_DEFAULT_IMAGE} />
            }
          </TouchableOpacity>
          {/* <Text
              style={{fontSize: 18, color: "#e55595"}}
            >
              Change Profile Photo
          </Text> */}
          <Text style={{fontSize: 16, color: "#33212D"}}>
            {this.state.name}
          </Text>
          <Text style={{fontSize: 14, color: "#33212D"}}>
            {this.state.breed}
          </Text>
        </View>
        <SafeAreaView style={styles.Tabs}>
          <MaterialTabs
            items={["Photos & Videos", "Details"]}
            selectedIndex={this.state.selectedTab}
            onChange={this.setTab.bind(this)}
            barColor="white"
            indicatorColor="#EF5595"
            activeTextColor="#EF5595"
            inactiveTextColor="#33212D"
          />
        </SafeAreaView>
        <View
          style={{
            backgroundColor: "#F7F9FF",
            padding: 10,
            paddingBottom: 0,
            flex: 1,
            overflow: "visible"
          }}
        >
          {this.selectedTab()}
        </View>
        <ModalSpinner visible={this.props.loading}/>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  pet: state.pets.pet,
  loading: state.pets.loading,
  speciesAndBreeds: state.playDates.speciesAndBreeds,
});

const PetProfileForm = reduxForm({
  form: "AddPetForm",
  validate: values => {
    // const emailReg = /\S+@\S+\.\S+/g;
    const errors = {};
    errors.name = !values.name
      ? "name field is required"
      : // : !emailReg.test(values.email)
        //   ? "Email format is invalid"
      undefined;

    errors.datetime1 = !values.datetime1
      ? "Please specify a date and time"
      : undefined;
    errors.speciesOptions = !values.speciesOptions ? "" : undefined;
    errors.breedOptions = !values.breedOptions ? "" : undefined;
    return errors;
  }
})(PetProfile);

export default connect(
  mapStateToProps,
  {
    fetchPetWithId,
    updatePet,
    petSpeciesBreed,
    uploadPetMedia,
    fetchAllPets,
    enableBackButton,
    setHeaderTitle,
    disableBackButton,
    removeHeaderTitle
  }
)(PetProfileForm);

const styles = StyleSheet.create({
  Circle: {
    height: 100,
    width: 100,
    borderRadius: 50,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  Tabs: {
    borderTopColor: "rgba(217, 214, 216, .2)",
    borderTopWidth: 2,
    borderStyle: "solid",
    zIndex: 9999,
    marginTop: -2
  },
  textLabel: {
     margin: 15
  },
  ViewShadow: {
    // flexDirection: "row",
    marginHorizontal: 15,
    borderColor: "#000",
    justifyContent: "center",
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.18,
    shadowRadius: 1,
    elevation: 1,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    shadowColor: '#451B2D',
  },
  ButtonView: {
    backgroundColor: "#e55595",
    height: height * 0.07,
    borderRadius: 5,
    marginBottom: 10,
    marginHorizontal: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10
  }
});
