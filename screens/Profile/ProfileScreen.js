import React from "react";
import {connect} from "react-redux";
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  ScrollView,
  AsyncStorage,
  FlatList,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  RefreshControl, Modal, TouchableWithoutFeedback
} from "react-native";
import MaterialTabs from "react-native-material-tabs";
import StarRating from "react-native-star-rating";
import {reduxForm} from "redux-form";
import {ImagePicker, Permissions} from "expo";
import { NavigationEvents } from 'react-navigation';

import GeneralStyles from "../../Styles/General";
import Header from "../../components/Header";
import PlaydateItem from "../../components/PlaydateItem";
import ReviewItem from "../../components/ReviewItem";
import {
  fetchAllPets,
  fetchAllPlaydates,
  fetchReviewPlaydate,
  userProfile,
  fetchPlaydate,
  fetchSinglePlaydate,
  fetchUserReviews,
  acceptReviewPlaydate,
  declineReviewPlaydate,
  setHeaderTitle, shareReview, logout
} from "../../store/actions";
import {dummyPets} from "../../Data/pets";
import Spinner from "../../components/UI/activityIndicator";

const {height, width} = Dimensions.get("window");
const height1 = (((height / 2 / height) * 100 + 3) / 100) * height;


class ProfileScreen extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  static getDerivedStateFromProps(props, state) {
    if (props.loading === false && props.user !== null) {
      return {
        picture: props.user.profilePic,
        name: props.user.userName
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 0,
      name: "PetLuvs",
      picture: "https://petluvsstore.s3.amazonaws.com/pets/default/petluvs_default_pet.jpg",
      showEditReview: false,
      review: "",
      playdates: [],
      refreshing: false,
      showShareModal: false,
      shareReviewid:''

    };
  }

  componentDidMount() {
    this.loadData();
  }

  loadData = async () => {
    let userId = this.props.authInfo.id;

    this.props.userProfile(userId);
    this.props.fetchAllPets();
    this.props.fetchAllPlaydates();
    this.props.fetchSinglePlaydate();
    this.props.fetchUserReviews(userId);
    await this.props.fetchReviewPlaydate(this.state.playdateId);
    this.getItem();
    await this.setState({ refreshing : false });
  };

  // Logout
  async clearStorage() {
    try {
      // await AsyncStorage.clear();
      await this.props.logou
      this.props.navigation.navigate("LoginScreen");
    } catch (error) {
      console.log(error);
    }
  }

  updateData = () => {
    this.props.userProfile();
    this.forceUpdate();
  };

  updatePetData = () => {
    this.props.fetchAllPets();
  };

  handleRefresh = () => {
    this.loadData();
  }

  // Reture true or false flag if cmp need to update
  shouldComponentUpdate(nextProps) {
    const {loading, user} = nextProps;
    if (loading === false && user !== null) {
      return true;
    }
    return false;
  }

  addPet = () => {
    const {navigate} = this.props.navigation;
    navigate("AddPet")
  };

  setTab = selectedTab => {
    this.setState({selectedTab});
  };

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  _pickImage = async () => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [1, 1]
    });

    if (!result.cancelled) {
      this.setState({image: result.uri});
    }
  };

  getItem = async () => {
    try {
      const myname = await AsyncStorage.getItem("name");
      const profileImage = await AsyncStorage.getItem("picture");
      this.setState({name: myname});
      this.setState({picture: !profileImage ? photo : profileImage});
    } catch (error) {
      // console.log("Error", error);
    }
  };

  playdatesEmptyComponent = () => (
    <TouchableOpacity
      style={[
        GeneralStyles.Card,
        {
          marginHorizontal: 15,

          height: 40,
          // backgroundColor: "#ffffff",

          alignItems: "center",
          justifyContent: "center",
          marginBottom: 10
        }
      ]}
      onPress={() => this.props.navigation.navigate("PlaydateCreation")}
    >
      <Text style={{fontSize: 15}}>Create a Pet Play Date</Text>
    </TouchableOpacity>
  );

  addPet = () => {
    const {navigate} = this.props.navigation;
    navigate("AddPet");
  };

  selectedTab() {
    if (this.state.selectedTab === 0) {
      return (
        <View style={{flex: 1}}>
          {this.props.pets.length > 0 ? (
            <View
              style={{
                flexDirection: "row",
                flex: 1,
                flexWrap: "wrap",
                overflow: "visible",
                justifyContent: "space-between"
              }}
            >
              <FlatList
                // data={this.props.pets} /* only when server available */
                data={
                  this.props.pets ? this.props.pets : dummyPets
                } /* Use this if you don't have server access*/
                numColumns={2}
                renderItem={({item}) => (
                  <View style={styles.MainView}>
                    <TouchableOpacity
                      onPress={() =>
                        this.props.navigation.navigate("PetProfile", {
                          name: item.name,
                          type: item.breed,
                          species: item.species,
                          _id: item._id,
                          onGoBack: () => this.updatePetData()
                        })
                      }
                    >
                      <ImageBackground
                        resizeMode="cover"
                        style={styles.PetImage}
                        source={{
                          uri:
                            item.imagePath[0] && item.imagePath[0].url !== null
                              ? item.imagePath[0].url
                              : "https://petluvsstore.s3.amazonaws.com/Petluvs-defaults/petluvs.png"
                        }}
                      >
                        <Text style={styles.PetName}>{item.name}</Text>
                        <Text style={styles.PetType}>{item.breed}</Text>
                      </ImageBackground>
                    </TouchableOpacity>
                  </View>
                )}
                keyExtractor={(item, index) => index.toString()}
              /></View>
          ) : (
            <View>
              <TouchableOpacity onPress={() => this.addPet()}>
                <View
                  style={{
                    paddingLeft: 10,
                    paddingRight: 10,
                    paddingBottom: 10
                  }}
                >
                  <Image
                    source={require("../../assets/addNewPet.png")}
                    style={{width: 40, height: 40, borderRadius: 40 / 2}}
                  />
                  <Text
                    style={{color: "#EF5595", fontSize: 12, marginLeft: 4}}
                  >
                    {/*{item.name}*/}
                    Add Pet
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>

      );
    }
    if (this.state.selectedTab === 1) {
      return (
        <FlatList
          data={this.props.userPlaydates}
          keyExtractor={(item, i) => `key-${i}`}
          ListEmptyComponent={this.playdatesEmptyComponent()}
          renderItem={({item}) => (
            <View style={{width: width - 40, alignSelf: 'center'}}>
              <PlaydateItem
                playdateId={item.record._id}
                Name={
                  item.record.pets.length > 0
                    ? item.record.pets[0].name
                    : "Some error"
                }
                StartTime={item.record.startTime}
                EndTime={item.record.endTime}
                Location={
                  item !== null &&
                  item.record.location !== undefined &&
                  item.record.place !== undefined
                    ? item.record.place
                    : item.record.place
                }
                MilesAway={
                  item !== null &&
                  item.record.location !== undefined &&
                  item.record.location.distance !== undefined
                    ? item.record.location.distance
                    : "5.6"
                }
                userJoining={item.userJoining}
                item={item}
                locPhotos={
                  item !== null &&
                  item.record.location !== undefined &&
                  item.record.location.photos !== undefined
                    ? item.record.location.photos
                    : []
                }
                photo={
                  item.record.pets.length > 0
                    ? item.record.pets[0].imagePath[0].url
                    : "https://petluvsstore.s3.amazonaws.com/Petluvs-defaults/petluvs.png"
                }
                playdateOwnerId={item.record.userId._id}
                // fromHomeScreen={false}
              />
            </View>
          )}
          ListFooterComponent={() =>
            this.props.loading === true ? (
              <ActivityIndicator animating size="large"/>
            ) : null
          }
          ListHeaderComponent={() => <View style={{height: 20}}/>}
        />
      );
    }
    if (this.state.selectedTab === 2) {
      return (
        <ScrollView showsVerticalScrollIndicator={false} removeClippedSubviews>
          <View
            style={{
              flexDirection: "row",
              marginTop: 10,
              marginRight: 10,
              marginBottom: 10,
              // width: width * 0.5,
              alignSelf: "center"
            }}
          >
            <StarRating
              disabled
              maxStars={5}
              rating={this.props.avgStarCount}
              selectedStar={rating => this.onStarRatingPress(rating)}
              starSize={35}
              starColor={"#fff"}
              fullStarColor={"#EF5595"}
            />
            {/*<Text>{this.props.userReviews}</Text>*/}
          </View>

          <FlatList
            style={GeneralStyles.ScreenContainer}
            data={this.props.userReviews}
            keyExtractor={(item, index) => index.toString()}
            extraData={(this.props.userReviews, this.props.reviews)}
            renderItem={({item}) => (
            <View style={{marginBottom: 14, width: width - 40, alignSelf: 'center'}}>
              <View style={[GeneralStyles.Card, {paddingHorizontal:10, paddingVertical:10}]}>

              <ReviewItem
                key={item._id}
                id={item._id}
                Name={item.reviewerId && item.reviewerId.userName ? item.reviewerId.userName : "Petluvs User"}
                ProfilePic={item.reviewerId && item.reviewerId.profilePic ? item.reviewerId.profilePic : "https://petluvsstore.s3.amazonaws.com/Petluvs-defaults/petluvs.png"}
                BusinessName={item.reviewFor}
                ReviewText={item.description}
                starCount={item.starCount}
                showverification
                // showverification={item.verified}
                // accept={this.props.acceptReviewPlaydate}
                // decline={this.props.declineReviewPlaydate}
                fetchReviews={() => this.props.fetchUserReviews()}
              />
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    height: 30,
                    marginRight: 5,
                    paddingHorizontal:10,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#e55595",
                    borderRadius: 5,
                    flexDirection: "row"
                  }}
                  onPress={() =>
                    this.setState({
                      showShareModal: !this.state.showShareModal,
                      shareReviewid: item._id
                    })
                  }
                >
                  <Image
                    source={require("./../../assets/share.png")}
                    style={{tintColor: "#ffffff"}}
                  />
                  <Text
                    style={{
                      color: "#fff",
                      marginLeft: 10
                    }}
                  >
                    Share
                  </Text>
                </TouchableOpacity>

              </View>
            </View>
              // <Text>
              //   {this.props.userReviews}
              // </Text>
            )}
          />

        </ScrollView>
      );
    }
  }

  // Logout
  async clearStorage() {
    try {
      let res = await logout();
      if (res === "success") {
        this.props.navigation.navigate("LoginScreen");
      } else {
        alert("Could not logout. Please try again");
      }
    } catch (error) {
      console.log(error);
    }
  }

  updateData = () => {
    this.props.userProfile();
    this.forceUpdate();
  };

  updatePetData = () => {
    this.props.fetchAllPets();
  };

  render() {
    if (this.props.loading === true) {
      return <Spinner/>;
    }

    return (
      <View style={{flex: 1, backgroundColor: "white"}}>
        <NavigationEvents
          onWillFocus={payload => setTimeout(() => this.props.setHeaderTitle('Personal Profile'), 10)}
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          removeClippedSubviews
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.handleRefresh}
            />
          }
        >
          <View style={{height: 20}} />
          <View
            style={{
              alignItems: "center",
              paddingBottom: 20,
              backgroundColor: "white",
              zIndex: 999
            }}
          >
            <TouchableOpacity
              onPress={() => {

                this.props.navigation.navigate("EditProfile", {
                  user: this.props.user,
                  onGoBack: () => this.updateData()
                })
              }
              }
            >
              <View style={styles.Circle}>
                <Image
                  style={{width: 100, height: 100}}
                  source={{uri: this.state.picture}}
                />
              </View>
            </TouchableOpacity>
            <Text style={{fontSize: 18, color: "#33212D"}}>
              {this.state.name}
            </Text>
            <TouchableOpacity
              style={{width: 50, height: 20}}
              onPress={() => this.clearStorage()}
            >
              <Text>Logout</Text>
            </TouchableOpacity>
          </View>

          <SafeAreaView style={styles.Tabs}>
            <MaterialTabs
              items={["Pets", "Past Playdates", "Reviews"]}
              selectedIndex={this.state.selectedTab}
              onChange={this.setTab}
              barColor="white"
              indicatorColor="#EF5595"
              activeTextColor="#EF5595"
              inactiveTextColor="#33212D"
            />

            {this.state.showShareModal === true ? (
              <Modal
                animationType="fade"
                transparent
                visible={this.state.showShareModal}
              >
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({
                      showShareModal: !this.state.showShareModal
                    });
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "#00000030",
                      alignItems: "center",
                      justifyContent: "center",
                      flex: 1
                    }}
                  >
                    <TouchableWithoutFeedback
                      onPress={() => {
                        this.setState({
                          showShareModal: !this.state.showShareModal
                        });
                      }}
                      style={{
                        position: "absolute",
                        top: 0,
                        right: 0,
                        height: height1,
                        width,
                        backgroundColor: "#00000050",
                        alignItems: "center",
                        justifyContent: "flex-start",
                        flex: 1
                      }}
                    >
                      <TouchableWithoutFeedback>
                      <View
                        style={{
                          height: 250,
                          backgroundColor: "#ffffff",
                          width: width - 100,
                          borderRadius: 15,
                          marginHorizontal: 50
                        }}
                      >
                        <View>
                          <View
                            style={{
                                alignItems: "center",
                                justifyContent: "center",
                                marginTop: 10,
                                height: 80,
                                backgroundColor: "#FFFFFF",
                                color: "gray",
                                paddingLeft: 10,
                                marginBottom: 30
                              }}
                          >
                            <Text
                              style={{fontSize: 15}}
                            >
                              Are you sure you want to share this review in Home
                              page.
                            </Text>
                          </View>
                          <TouchableOpacity
                            style={[
                              styles.saveButton,
                              {alignSelf: "center", width: 200}
                            ]}
                            onPress={() => {
                              this.setState({
                                showShareModal: !this.state.showShareModal
                              });
                              this.props.shareReview(this.state.shareReviewid);
                            }}
                          >
                            <Text style={{fontSize: 15, color: "#fff"}}>Yes</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={{marginBottom: 5, marginTop: 10}}
                            onPress={() => {
                              this.setState({
                                showShareModal: !this.state.showShareModal
                              });
                            }}
                          >
                            <View style={{alignSelf: "center"}}>
                              <Text style={{fontSize: 15, color: "#000"}}>
                                Cancel
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      </TouchableWithoutFeedback>
                    </TouchableWithoutFeedback>
                  </View>
                </TouchableWithoutFeedback>
              </Modal>
            ) : null}

          </SafeAreaView>

          <View style={GeneralStyles.ScreenContainer}>
            {this.selectedTab()}
          </View>

        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  pets: state.pets.pets,
  playDates: state.playDates.playdates,
  userPlaydates: state.playDates.userPlaydates,
  userReviews: state.playDates.userReviews,
  avgStarCount: state.playDates.avgStarCount,
  reviews: state.playDates.reviews,
  authInfo: state.auth.authInfo,
  loading: state.playDates.loading || state.pets.loading || state.user.loading
});
const ProfileScreenForm = reduxForm({
  form: "ProfileScreen form",
  validate: values => {
    const errors = {};
    if (!values.UserReview) {
      errors.UserReview = "Please enter your review";
      // console.log(`errors.userreview if not${errors.UserReview}`);
    } else if (values.UserReview.length < 3) {
      errors.UserReview = "Please Give your review in some brief";
      // console.log(`errors.userreview length not${errors.UserReview}`);
    } else {
      errors.UserReview = undefined;
      // console.log(`errors.userreview${errors.UserReview}`);
    }
  }
})(ProfileScreen);
export default connect(
  mapStateToProps,
  {
    fetchAllPets,
    fetchAllPlaydates,
    userProfile,
    fetchReviewPlaydate,
    fetchPlaydate,
    fetchSinglePlaydate,
    fetchUserReviews,
    acceptReviewPlaydate,
    declineReviewPlaydate,
    shareReview,
    setHeaderTitle
  }
)(ProfileScreenForm);

const styles = StyleSheet.create({
  Circle: {
    height: 100,
    width: 100,
    borderRadius: 50,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20
  },
  MainView: {
    height: width / 2.3,
    width: width / 2.3,
    borderRadius: 6,
    margin: 4,
    overflow: "hidden"
  },
  Tabs: {
    borderTopColor: "rgba(217, 214, 216, .2)",
    borderTopWidth: 2,
    borderStyle: "solid",
    zIndex: 9999,
    marginTop: -2
  },
  PetName: {
    fontSize: 18,
    color: "white"
  },
  PetType: {
    fontSize: 12,
    color: "white",
    opacity: 0.75,
    marginBottom: 5
  },
  PetImage: {
    width: 170,
    height: 170,
    justifyContent: "flex-end",
    padding: 10
  },
  TextInput: {
    flex: 1,
    height: height * 0.21,
    backgroundColor: "#FFFFFF",
    color: "gray",
    paddingLeft: 10,
    alignItems: "flex-start",
    marginBottom: 30
  },
  saveButton: {
    height: 50,
    width: "90%",
    borderRadius: 10,
    backgroundColor: "#e55595",
    opacity: 0.9,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center"
  }
});
