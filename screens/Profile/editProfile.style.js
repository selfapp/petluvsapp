import { Dimensions, StyleSheet } from "react-native";

const { height } = Dimensions.get("window");

export const styles = StyleSheet.create({
  textLabel: {
    margin: 10,
    marginTop: 10,
    fontSize: 14,
    color: "#33212D"
  },
  Circle: {
    height: 100,
    width: 100,
    borderRadius: 50,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20
  },
  text1: { color: "#8F858C", marginLeft: "5%", fontSize: 14, width: "80%" },
  text2: {
    width: "30%"
  },
  FlipToggleButton: {
    height: 30,
    marginLeft: "5%",
    marginTop: 10
  },
  seperator: {
    height: 2,
    backgroundColor: "grey",
    opacity: 0.3
  },
  button: { backgroundColor: "#e55595"},
  subMenu: {
    marginLeft: 20,
    marginRight: 20,
    paddingRight: 10,
    backgroundColor: "#fff",
    // opacity:.3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#e55595",
    shadowOpacity: 0.5,
    marginBottom: 18,
    // paddingTop:8,
    borderRadius: 7
  },
  emailAndNumbercontainer: {
    // flexDirection:'row',
    marginTop: 15,
    marginLeft: 15,
    marginBottom: 15,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 5
  },
  clubMemberTextInput: {
    width: "95%",
    marginTop: 5,
    height: 50,
    padding: 10,
    fontSize: 20,
    color: "#999",
    backgroundColor: "#fff",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#e55595",
    shadowOpacity: 0.5
  },

  ButtonView: {
    backgroundColor: "#e55595",
    height: height * 0.07,
    borderRadius: 2,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  },
  addressContainer: {
    height: 50,
    width: "95%",
    borderWidth: 2,
    borderColor: "#fff",
    backgroundColor: "#fff",
    shadowOffset: { width: 0, height: 2 },
    shadowColor: "#e55595",
    shadowRadius: 5,
    shadowOpacity: 0.2,
    elevation: 5,
    // alignItems:'center',
    justifyContent: "center",
    paddingLeft: 10,
    marginBottom: 10,
    marginLeft: 2
  },
  findMe: {
    position: "absolute",
    height: 30,
    width: "20%",
    right: 20,
    bottom: 10,
    backgroundColor: "#e55595",
    // alignItems:'center',
    justifyContent: "center",
    borderRadius: 10
  },
  textLabel1: {
    margin: 15,
    fontSize: 14
  },
  ViewShadow: {
    flexDirection: "row",
    marginHorizontal: 15,
    borderColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 5,
    shadowColor: "#e55595"
  },

  dropDown: {
    position: "absolute",
    right: 10,
    alignSelf: "center"
  },

  TextInput: {
    height: height * 0.07,
    backgroundColor: "#FFFFFF",
    color: "gray",
    paddingLeft: 10
  },
  TextInput2: {
    width: "80%"
  },
  switchButtonDesc: { color: "#8F858C", marginLeft: "5%", fontSize: 14 },
  InformationHeader: { marginLeft: 25, marginTop: 15 },
  profileImage: {
    // flex: 1,
    // resizeMode: 'contain',
    flexDirection: 'row',
    borderColor: "#e55595",
    borderStyle: "dashed",
    width: "85%",
    height: 60,
    borderRadius: 10,
    alignItems: "center",
    borderWidth: 1,
    marginTop: 30,
    justifyContent: "center",
    alignSelf: "center"
  },
  saveButton: {
    height: 50,
    width: "100%",
    borderRadius: 10,
    backgroundColor: "#e55595",
    margin: 10
  },
  saveButtonContainer: {
    // alignSelf: "flex-end",
    marginHorizontal: 15,
    alignItems: "center",
    justifyContent: "center",
    height: "10%",
    marginBottom: 10,
    // width: "100%",
    marginTop: 10
  },
  sectionHeaderStyle: {
    shadowColor: "#e55595",
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5
  }
});
