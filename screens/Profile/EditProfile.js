/* eslint-disable prettier/prettier */
import React from "react";
import * as ImageManipulator from 'expo-image-manipulator';
import connect from "react-redux/es/connect/connect";
import {ImagePicker, Permissions} from "expo";
import {Field, reduxForm} from "redux-form";
import {
  AsyncStorage,
  Dimensions,
  Image,
  Modal,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ActivityIndicator,
  TouchableWithoutFeedback
} from "react-native";
import CheckBox from "react-native-checkbox";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import Icon from "react-native-vector-icons/FontAwesome";
import Collapsible from 'react-native-collapsible';
import {NavigationEvents} from "react-navigation";

import Header from "../../components/Header";
import FlipToggle from "react-native-flip-toggle-button";
import MyTextInput from "./../../components/UI/MyTextInput";
import {styles} from "./editProfile.style";
import {
  fetchAllPets,
  updateUserProfile,
  userProfile,
  updateUserProfileV2,
  setHeaderTitle,
  enableBackButton,
  disableBackButton
} from "../../store/actions";

const {height, width} = Dimensions.get("window");

class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    const image = `https://s3.amazonaws.com/petluvs/leo.jpg`;
    const businessEmail = props.user.businessEmail ? props.user.businessEmail : "";
    const phone_number = props.user.phoneNumber ? props.user.phoneNumber : "";
    const profilePic = props.user.profilePic && props.user.profilePic != "" ? props.user.profilePic : "https://s3.amazonaws.com/petluvs/leo.jpg";
    const SSN = props.user.ssn ? props.user.ssn.toString() : "";
    const userName = props.user.local ? props.user.local.userName : "PetLuvs User";
    const ID_proof = props.user.idImage ? props.user.idImage : null;
    const businessUrl = props.user.businessUrl ? props.user.businessUrl : "";

    this.state = {
      businessUrl,
      profilePic,
      profilePicB64: null,
      SSN,
      ID_proof,
      ID_proofB64: null,
      reEnteredSSN: "",
      backgroundCheck: false,
      businessEmail,
      phone_number,
      tnc: false,
      loading: false,
      isCollapsed: false,
      showConfirmatonModal: false,
      showBreederModal: false,
      showPetTrainerModal: false,
      showPetGroomerModal: false,
      showDogWalkerModal: false,
      showFosterPetsModal: false,
      showDaycareAndBoardersModal: false,
      showRescueAnimalsModal: false,
      showPetSitterModal: false,
      akcRegistration: false,
      businessImagesBreeder: null,
      businessImagesPetSitter: null,
      businessImagesPetGroomer: null,
      businessImagesPetTrainer: null,
      businessImagesFosterPets: null,
      businessImagesRescueAnimals: null,
      businessImagesDogWalker: null,
      address: "udyog vihar phase 2",
      date: "2016-05-15",
      image,
      imge64format: image,
      isSwitch4On: false,
      flag: false,
      businessDescription: "",
      businessPhone: "",
      selectedSpecies: [],
      selectedBreeds: [],
      akcNumber: "",
      description: "",
      category1: false,
      category2: false,
      category3: false,
      category4: false,
      category5: false,
      category6: false,
      category7: false,
      breeder: true,
      latitude: "",
      longitude: "",
      species: this.props.speciesAndBreeds,
      breeds: [],
      name: userName,
      speciesAndBreeds: [],
      petSpeciesPetDaycareAndBoarder: false,
      petSpeciesPetGroomer: false,
      petSpeciesPetTrainer: false,
      petSpeciesFosterPets: false,
      petSpeciesRescueAnimals: false,
      petSpeciesDogWalker: false,
      petSpeciesPetSitter: false,
      petBreedDogWalker: false,
      petBreedDaycareAndBoarder: false,
      petBreedPetGroomer: false,
      petBreedPetTrainer: false,
      petBreedPetSitter: false,
      petBreedFosterPets: false,
      petBreedRescueAnimals: false,
      modalVisible: false,
      enableServices: [],
      shouldServiceEnable: true,
      serviceArray: this.props.user.accountType,
      url: "https://maps.googleapis.com/maps/api/geocode/json?latlng=",
      items: {
        category1: {
          data: {
            data: "I am an Pet Breeder",
            subdata: "Animal Breeder Information"
          }
        },
        category2: {
          data: {
            data: "I provide DayCare/boarding",
            subdata: "DayCare/boarding Information"
          }
        },
        category3: {
          data: {
            data: "I am a dog breeder",
            subdata: "Dog Breeder Information"
          }
        },
        category4: {
          data: {
            data: "I am a Pet Groomer",
            subdata: "Pet Groomer Information"
          }
        },
        category5: {
          data: {
            data: "I am a Pet Trainer",
            subdata: "Pet Trainer Information"
          }
        },
        category6: {
          data: {
            data: "I want to Foster Pets",
            subdata: "Pet Foster Information"
          }
        },
        category7: {
          data: {
            data: "I want to Rescue Animals",
            subdata: "Animal Rescue Information"
          }
        },
        category8: {
          data: {
            data: "I am a Dog Walker",
            subdata: "Dog Walker Information"
          }
        },
        category9: {
          data: {
            data: "I run a Veterinary Clinics",
            subdata: "Veterinary Clinics Information"
          }
        },
        category10: {
          data: {
            data: "I run Dog Parks",
            subdata: "Dog Parks Information"
          }
        },
        category11: {
          data: {
            data: "I run Pet Friendly Restaurants",
            subdata: "Pet Friendly Restaurants Information"
          }
        },
        category12: {
          data: {
            data: "I am a Pet Sitter",
            subdata: "Pet Sitter Information"
          }
        }
      },
      listDetailArray: [
        {name: "Category1", file: require("../../assets/onboarding1.png")},
        {name: "Category2", file: require("../../assets/onboarding1.png")},
        {name: "Category3", file: require("../../assets/onboarding1.png")},
        {name: "Category4", file: require("../../assets/onboarding1.png")},
        {name: "Category5", file: require("../../assets/onboarding1.png")}
      ],
      categoryArray: [
        {name: "Category1"},
        {name: "Category2"},
        {name: "Category3"},
        {name: "Category4"},
        {name: "Category5"}
      ],
      multiSpecies: this.props.speciesAndBreeds,
      dogBreeds: []
    };
  }

  componentDidMount = () => {
    this.setDogBreeds();
    this.setState({
      isCollapsed: true,
      serviceArray: {
        ...this.state.serviceArray,
        ...this.props.user.accountType
      }
    });
    this.forceUpdate();
  };

  componentWillReceiveProps(nextProps) {
    this.hideModels(nextProps);
  }

  hideModels(props) {
    if (props.loading === false) {
      this.setState({
        showConfirmatonModal: false,
        showBreederModal: false,
        showPetTrainerModal: false,
        showPetGroomerModal: false,
        showDogWalkerModal: false,
        showFosterPetsModal: false,
        showDaycareAndBoardersModal: false,
        showRescueAnimalsModal: false,
        showPetSitterModal: false
      })
    }
  }

  setDogBreeds() {
    this.props.speciesAndBreeds.forEach((species) => {
      if (species.name === 'Dog') {
        this.setState({
          dogBreeds: species.children
        });
      }
    });
  }

  //For upload profile image
  uploadProfileImage = async (type) => {
    let fileObj = null;

    const user = {
      profilePic: this.state.profilePic
    };

    if (type === "profilePic" && this.state.profilePic && !this.isUrl(this.state.profilePic)) {
      fileObj = this.makeUploadableFileObj(this.state.profilePic)
    }
    this.props.updateUserProfileV2(user, type, fileObj);
  };

  onUpdateUserName = async () => {
    const user = {
      userName: this.state.name
    };
    this.props.updateUserProfileV2(user);

  }

  // Saving business details section saperately
  onSaveBusinessDetails = async (type) => {
    let fileObj = null;

    const user = {
      businessEmail: this.state.businessEmail,
      phoneNumber: this.state.phone_number,
      accountType: this.state.serviceArray,
      userName: this.state.name,
      businessUrl: this.state.businessUrl,
      ssn: this.state.SSN,
      photoId: this.state.ID_proof
    };

    if (type === "businessDetails" && this.state.ID_proof && !this.isUrl(this.state.ID_proof)) {
      fileObj = this.makeUploadableFileObj(this.state.ID_proof)
    }

    this.props.updateUserProfileV2(user, type, fileObj);
  };

  // Saving service provider info to the backend saperately
  onSaveServiceProviderInfo = (type) => {
    // console.log("type", type);
    const user = {
      accountType: this.state.serviceArray
    }
    const {breeder, petGroomer, dogWalker, petTrainer, petSitter} = this.state.serviceArray;

    let fileObj = null;

    if (type === "breeder" && !this.isUrl(breeder.akcImage)) {
      const {akcImage} = breeder;
      if (akcImage) {
        fileObj = this.makeUploadableFileObj(akcImage);
      }
    } else if (type === "petGroomer" && !this.isUrl(petGroomer.businessImages)) {
      const {businessImages} = petGroomer;
      if (businessImages) {
        fileObj = this.makeUploadableFileObj(businessImages);
      }
    } else if (type === "dogWalker" && !this.isUrl(dogWalker.businessImages)) {
      const {businessImages} = dogWalker;
      if (businessImages) {
        fileObj = this.makeUploadableFileObj(businessImages);
      }
    } else if (type === "petTrainer" && !this.isUrl(petTrainer.businessImages)) {
      const {businessImages} = petTrainer;
      if (businessImages) {
        fileObj = this.makeUploadableFileObj(businessImages);
      }
    } else if (type === "petSitter" && !this.isUrl(petSitter.businessImages)) {
      const {businessImages} = petSitter;
      if (businessImages) {
        fileObj = this.makeUploadableFileObj(businessImages);
      }
    }

    // console.log("fileObj", fileObj);

    // this.props.updateUserProfile(user);
    this.props.updateUserProfileV2(user, type, fileObj);
  }

  // Helper function to make file obj from uri
  makeUploadableFileObj = uri => {
    let fileObj = null;

    const ext = uri.split(".").pop();
    const mediaType = `image/${ext}`;
    const index = uri.lastIndexOf("/") + 1;
    const filename = uri.substr(index);
    fileObj = {
      uri,
      type: mediaType,
      name: filename
    };

    return fileObj;
  }

  // Helper => urlcheaker
  isUrl = url => {
    let RegExp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return RegExp.test(url);
  }

  // On Cancel Service provider info
  onCanceleServiceProviderInfo = type => {
    // console.log("onCancelServiceprovider");
    // console.log("suerServiceArr", this.props.user.accountType);
    if (type === "breeder") {
      this.setState({
        showBreederModal: !this.state.showBreederModal,
        serviceArray: {
          ...this.state.serviceArray,
          breeder: {
            ...this.state.serviceArray.breeder,
            isServiceProvider: this.props.user.accountType.breeder.isServiceProvider
          }
        }
      })
    } else if (type === "petGroomer") {
      this.setState({
        showPetGroomerModal: !this.state.showPetGroomerModal,
        serviceArray: {
          ...this.state.serviceArray,
          petGroomer: {
            ...this.state.serviceArray.petGroomer,
            isServiceProvider: this.props.user.accountType.petGroomer.isServiceProvider
          }
        }
      })
    } else if (type === "petTrainer") {
      this.setState({
        showPetTrainerModal: !this.state.showPetTrainerModal,
        serviceArray: {
          ...this.state.serviceArray,
          petTrainer: {
            ...this.state.serviceArray.petTrainer,
            isServiceProvider: this.props.user.accountType.petTrainer.isServiceProvider
          }
        }
      })
    } else if (type === "dogWalker") {
      this.setState({
        showDogWalkerModal: !this.state.showDogWalkerModal,
        serviceArray: {
          ...this.state.serviceArray,
          dogWalker: {
            ...this.state.serviceArray.dogWalker,
            isServiceProvider: this.props.user.accountType.dogWalker.isServiceProvider
          }
        }
      })
    } else if (type === "sitter") {
      this.setState({
        showPetSitterModal: !this.state.showPetSitterModal,
        serviceArray: {
          ...this.state.serviceArray,
          petSitter: {
            ...this.state.serviceArray.petSitter,
            isServiceProvider: this.props.user.accountType.petSitter.isServiceProvider
          }
        }
      })
    }
  };

  renderHeader() {
    return (
      <Header
        navigation={this.props.navigation}
        update
        title={"Edit Profile"}
      />
    );
  }

  breeds = value => {
    const breedsArray = [];
    breedsArray.push(
      this.props.speciesAndBreeds[value].breeds.map(value => {
        breedsArray.push(value);
      })
    );


    this.setState({breeds: breedsArray});
  };

  addPet = () => {
    const {navigate} = this.props.navigation;
    navigate("AddPet");
  };

  // Pick Image handler for all the component.
  _pickImage = async imageFor => {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });

    const mainpResult = await ImageManipulator.manipulateAsync(
      result.uri,
      [{resize: {width: 300, height: 300}}],
      {format: ImageManipulator.SaveFormat.JPEG, base64: true, compress: 1}
    );

    if (!result.cancelled) {
      // this.setState({ [datatype]: result.uri });
      // this.setState({ [`imageBase64${datatype}`]: result.base64 });
      if (imageFor === "profilePic") {
        this.setState({profilePic: mainpResult.uri});
        this.setState({profilePicB64: mainpResult.base64});
        this.uploadProfileImage("profilePic");
      } else if (imageFor === "ID_proof") {
        this.setState({ID_proof: mainpResult.uri});
        this.setState({ID_proofB64: mainpResult.base64});
      } else if (imageFor === "akcRegistration") {
        this.setState({
          ...this.state,
          serviceArray: {
            ...this.state.serviceArray,
            breeder: {
              ...this.state.serviceArray.breeder,
              akcImage: mainpResult.uri
            }
          }
        })
      } else if (imageFor === "businessImagesPetGroomer") {
        this.setState({
          ...this.state,
          serviceArray: {
            ...this.state.serviceArray,
            petGroomer: {
              ...this.state.serviceArray.petGroomer,
              businessImages: mainpResult.uri
            }
          }
        })
      } else if (imageFor === "businessImagesDogWalker") {
        this.setState({
          ...this.state,
          serviceArray: {
            ...this.state.serviceArray,
            dogWalker: {
              ...this.state.serviceArray.dogWalker,
              businessImages: mainpResult.uri
            }
          }
        })
      } else if (imageFor === "businessImagesPetSitter") {
        this.setState({
          ...this.state,
          serviceArray: {
            ...this.state.serviceArray,
            petSitter: {
              ...this.state.serviceArray.petSitter,
              businessImages: mainpResult.uri
            }
          }
        })
      } else if (imageFor === "businessImagesPetTrainner") {
        this.setState({
          ...this.state,
          serviceArray: {
            ...this.state.serviceArray,
            petTrainer: {
              ...this.state.serviceArray.petTrainer,
              businessImages: mainpResult.uri
            }
          }
        })
      }
    }
  };

  saveLocation = async () => {
    const _this = this;
    if (this.state.latitude && this.state.longitude) {
      // console.log(
      //   `this.state.latitude ${this.state.latitude}this.state.longitude${
      //     this.state.longitude
      //     }`
      // );
      this.setState({loading: true});
      const url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
      const response = await this.getLocationRequest(
        `${url + this.state.latitude},${
          this.state.longitude
          }&key=AIzaSyBMr6Ee1JudyIZH0Q2yvh8aoqFA6kmHS0Q`
      );

      if (response.status === 200) {
        try {
          response.json().then(function (data) {
            _this.setState({
              loading: false,
              address: data.results[0].formatted_address
            });
          });
        } catch (error) {
          this.setState({loading: false});
          alert("There was an error saving your contract.");
        }
      } else {
        // console.log(`saveLocation failed${response}`);
      }
    } else {
      _this.getLatLong();
      // alert('Please wait trying to fetch your location.' + this.state.address+'hello')
    }
  };

  getLocationRequest(url, method, header) {
    return fetch(url, {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: header
      }
    });
  }

  getLatLong() {
    const _this = this;
    navigator.geolocation.getCurrentPosition(
      position => {
        _this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
        _this.saveLocation();
      },
      error => {
        // console.log(error);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000}
    );
  }

  services = async () => {
    this.setState({
      serviceArray: this.props.user.accountType
    });
  };

  servicesDetails = async (service, selectedItem, key = 'species') => {
    await this.setState({
      serviceArray: {
        ...this.state.serviceArray,
        [service]: {
          ...this.state.serviceArray[service],
          [key]: selectedItem
        }
      }
    });

    // console.log(this.state.serviceArray);
  };

  servicesSwitch = async (service, value) => {
    await this.setState({
      serviceArray: {
        ...this.state.serviceArray,
        [service]: {
          ...this.state.serviceArray[service],
          isServiceProvider: value
        }
      }
    });

    if (!value) {
      await this.setState({...this.state, showConfirmatonModal: true})
    }
  };

  saveValueinState = text => {
    const emailReg = /\S+@\S+\.\S+/g;

    if (isNaN(text)) {
      if (emailReg.test(text)) {
        this.setState({emailValid: true, phone_number: false});
      } else if (!emailReg.test(text) && isNaN(text)) {
        this.setState({emailValid: false, phone_number: false});
      }
    } else if (!isNaN(text)) {
      this.setState({phone_number: text});
    }
  };

  ////Selected Service without breeder ////
  onSpeciesSelectedInService = async (service, value) => {
    // console.log("onSpeciesSelectedInService");
    await this.setState({
      serviceArray: {
        ...this.state.serviceArray,
        [service]: {
          ...this.state.serviceArray[service],
          species: {
            ...this.state.serviceArray[service].species,
            value
          }
        }
      }
    });
  };

  onConfirmSpeciesSelection = (service) => {
    // console.log("onConfirmSpeciesSelection");
    // console.log("services", service);
    // console.log("speciesArr", this.state.serviceArray);
  };

  saveBreederInfo = (data, details, base64) => {
    this.setState({[data]: details});
    const stateImage = `${data}Image`;
    this.setState({[stateImage]: base64});
  };

  showHideModal = dataType => {
    this.setState({[dataType]: !this.state[dataType]});
  };

  //onConfirm
  onConfirm = () => {
    // console.log("onConfirm")
    this.setState({showConfirmatonModal: false});
    this.onSaveServiceProviderInfo("confrim");
  };

  // OnDeclineConfrim
  onCancel = async () => {
    // console.log("onCancel");
    await this.setState({showConfirmatonModal: false});
    await this.setState({
      serviceArray: {
        ...this.state.serviceArray,
        ...this.props.user.accountType
      }
    })
  };

  /////////////////  UI SECTION START HERE ///////////////////
  // Confirmation Modal Start /////
  confirmationModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showConfirmatonModal}>
      <TouchableOpacity
        style={{
          backgroundColor: "#00000030",
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
        onPress={() => this.onCancel()}
      >
        <TouchableWithoutFeedback>

        <View
          style={{
            height: 250,
            width: 300,
            backgroundColor: "#fff",
            borderRadius: 10
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("../../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>Confirmation Modal</Text>
          </View>
          <View style={{padding: 10}}>
            <Text>Are you sure you want to disable service provider?</Text>
          </View>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                disabled={this.props.loading}
                onPress={() => this.onConfirm()}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>YES</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => this.onCancel()}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>NO</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </TouchableWithoutFeedback>

      </TouchableOpacity>
    </Modal>
  );
  /// ConfirmationModal end /////

  breederModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showBreederModal}
    >
      <TouchableOpacity
        style={{
          backgroundColor: "#00000030",
          // opacity: 0.3,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
        onPress={() => this.onCanceleServiceProviderInfo("breeder")}
      >
        <TouchableWithoutFeedback>
        <View
          style={{
            height: height - 250,
            width: width - 40,
            backgroundColor: "#fff",
            // marginTop: 20,
            borderRadius: 10
            // alignItems: "center"
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("../../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>Pet breeder details</Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              // flex: 1,
              width: width - 40,
              height: height + 140,
              marginBottom: 10
            }}
          >
            <View>
              <SectionedMultiSelect
                items={this.state.species}
                uniqueKey="id"
                subKey={"children"}
                showCancelButton
                showRemoveAll
                modalWithSafeAreaView
                modalWithTouchable
                styles={{button: {backgroundColor: "#e55595"}}}
                selectText="Which type of pets will you breed?"
                showDropDowns
                readOnlyHeadings
                onSelectedItemsChange={selectedItem =>
                  this.servicesDetails("breeder", selectedItem, 'breeds')
                }
                ref={ref => this.multiSelect = ref}
                selectedItems={this.state.serviceArray.breeder.breeds}
              />
            </View>

            <Text
              style={[
                styles.textLabel,
                {
                  // color: "#e55595",
                  marginBottom: 5,
                  fontSize: 15
                  // fontWeight: "bold"
                }
              ]}
            >
              AKC Registration Number
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      value={this.state.serviceArray.breeder.akcNumber}
                      onChangeText={value => {
                        this.setState({
                          serviceArray: {
                            ...this.state.serviceArray,
                            breeder: {
                              ...this.state.serviceArray.breeder,
                              akcNumber: value
                            }
                          }
                        });
                      }}
                      placeholder={"Please fill in the details here"}
                    />
                  </View>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this._pickImage("akcRegistration")}
              style={styles.profileImage}
            >
              {!this.state.serviceArray.breeder.akcImage ? null : (
                <Image
                  style={{height: 50, width: 50}}
                  source={{uri: this.state.serviceArray.breeder.akcImage}}
                />)}
              <Text
                style={[
                  styles.textLabel,
                  {
                    color: "#e55595",
                    fontSize: 15,
                    fontWeight: "bold"
                  }
                ]}
              >
                AKC Registration Certificate
                <Text e55595yle={{color: "#000", fontSize: 10}}>
                  (optional)
                </Text>
              </Text>
            </TouchableOpacity>
            <Text
              style={[
                styles.textLabel,
                {
                  // color: "#e55595",
                  marginBottom: 5,
                  fontSize: 15
                  // fontWeight: "bold"
                }
              ]}
            >
              Business description of your business
              <Text style={{color: "red", fontSize: 10}}>*</Text>
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      placeholder={"Please fill in the details here"}
                      value={
                        this.state.serviceArray.breeder.businessDescription
                      }
                      onChangeText={value => {
                        this.setState({
                          serviceArray: {
                            ...this.state.serviceArray,
                            breeder: {
                              ...this.state.serviceArray.breeder,
                              businessDescription: value
                            }
                          }
                        });
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={() => this.onSaveServiceProviderInfo("breeder")}
                disabled={this.props.loading}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => this.onCanceleServiceProviderInfo("breeder")}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </TouchableWithoutFeedback>

      </TouchableOpacity>
    </Modal>
  );

  //walkerjkl
  dogWalkerModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showDogWalkerModal}
    >
      <TouchableOpacity
        style={{
          backgroundColor: "#00000030",
          // opacity: 0.3,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
        onPress={() => this.onCanceleServiceProviderInfo("dogWalker")}
      >
        <TouchableWithoutFeedback>

        <View
          style={{
            height: height - 250,
            width: width - 40,
            backgroundColor: "#fff",
            // marginTop: 20,
            borderRadius: 10
            // alignItems: "center"
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("../../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>Dog walker details</Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              // flex: 1,
              width: width - 40,
              height: height + 140,
              marginBottom: 10
            }}
          >
            <SectionedMultiSelect
              items={this.state.dogBreeds}
              uniqueKey="id"
              showCancelButton
              modalWithSafeAreaView
              modalWithTouchable
              styles={{button: {backgroundColor: "#e55595"}}}
              selectText="Please choose from the list"
              onSelectedItemsChange={selectedItem => {
                this.servicesDetails("dogWalker", selectedItem, 'breeds')
              }}
              selectedItems={this.state.serviceArray.dogWalker.breeds}
            />
            <Text
              style={[styles.textLabel, {marginBottom: 5, fontSize: 15}]}
            >
              Business description
              <Text style={{color: "red", fontSize: 10}}>*</Text>
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      placeholder={"Please fill in the details here"}
                      value={
                        this.state.serviceArray.dogWalker.businessDescription
                      }
                      onChangeText={value => {
                        this.setState({
                          serviceArray: {
                            ...this.state.serviceArray,
                            dogWalker: {
                              ...this.state.serviceArray.dogWalker,
                              businessDescription: value
                            }
                          }
                        });
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this._pickImage("businessImagesDogWalker")}
              style={styles.profileImage}
            >
              <Image
                style={[{height: 50, width: 50}]}
                source={{uri: this.state.serviceArray.dogWalker.businessImages}}
              />
              <Text
                style={[
                  styles.textLabel,
                  {
                    color: "#e55595",
                    fontSize: 15,
                    fontWeight: "bold"
                  }
                ]}
              >
                Add Business Images
                <Text style={{color: "#e55595", fontSize: 10}}>
                  (optional)
                </Text>
              </Text>
            </TouchableOpacity>
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={() => this.onSaveServiceProviderInfo("dogWalker")}
                disabled={this.props.loading}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => this.onCanceleServiceProviderInfo("dogWalker")}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </TouchableWithoutFeedback>

        {/*</TouchableOpacity>*/}
      </TouchableOpacity>
    </Modal>
  );
  // groomerjkl
  petGroomerModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showPetGroomerModal}
    >
      <TouchableOpacity
        style={{
          backgroundColor: "#00000030",
          // opacity: 0.3,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
        onPress={() => this.onCanceleServiceProviderInfo("petGroomer")}
      >
        <TouchableWithoutFeedback>

        <View
          style={{
            height: height - 250,
            width: width - 40,
            backgroundColor: "#fff",
            borderRadius: 10
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("../../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>Pet groomer details</Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              // flex: 1,
              width: width - 40,
              height: height + 140,
              marginBottom: 10
            }}
          >
            <View>
              <SectionedMultiSelect
                items={this.state.species}
                uniqueKey="id"
                showCancelButton
                modalWithSafeAreaView
                modalWithTouchable
                styles={{button: {backgroundColor: "#e55595"}}}
                selectText="Which type of pets will you groom?"
                showDropDowns
                onSelectedItemsChange={(selectedItem) => {
                  this.setState({
                    serviceArray: {
                      ...this.state.serviceArray,
                      petGroomer: {
                        ...this.state.serviceArray.petGroomer,
                        species: selectedItem
                      }
                    }
                  });
                }}
                selectedItems={this.state.serviceArray.petGroomer.species}
              />
            </View>
            <View/>
            <Text
              style={[
                styles.textLabel,
                {
                  // color: "#e55595",
                  marginBottom: 5,
                  fontSize: 15
                  // fontWeight: "bold"
                }
              ]}
            >
              Business description
              <Text style={{color: "red", fontSize: 10}}>*</Text>
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      placeholder={"Please fill in the details here"}
                      value={
                        this.state.serviceArray.petGroomer.businessDescription
                      }
                      onChangeText={value => {
                        this.setState({
                          serviceArray: {
                            ...this.state.serviceArray,
                            petGroomer: {
                              ...this.state.serviceArray.petGroomer,
                              businessDescription: value
                            }
                          }
                        });
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            {!this.state.businessImagesPetGroomer ? (
              <TouchableOpacity
                onPress={() => this._pickImage("businessImagesPetGroomer")}
                style={styles.profileImage}
              >
                <Image
                  style={[{height: 50, width: 50}]}
                  source={{uri: this.state.serviceArray.petGroomer.businessImages}}
                />
                <Text
                  style={[
                    styles.textLabel,
                    {
                      color: "#e55595",
                      fontSize: 15,
                      fontWeight: "bold"
                    }
                  ]}
                >
                  Add Business Images
                  <Text style={{color: "#e55595", fontSize: 10}}>
                    (optional)
                  </Text>
                </Text>
              </TouchableOpacity>
            ) : (
              <Image
                style={[styles.profileImage, {height: 200, width: 200}]}
                source={{uri: this.state.businessImagesPetGroomer}}
              />
            )}
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={() => this.onSaveServiceProviderInfo("petGroomer")}
                disabled={this.props.loading}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => this.onCanceleServiceProviderInfo("petGroomer")}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </TouchableWithoutFeedback>
        {/*</TouchableOpacity>*/}
      </TouchableOpacity>
    </Modal>
  );
  //trainerjkl
  petTrainerModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showPetTrainerModal}
    >
      <TouchableOpacity
        style={{
          backgroundColor: "#00000030",
          // opacity: 0.3,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
        onPress={() => this.onCanceleServiceProviderInfo("petTrainer")}
      >
        <TouchableWithoutFeedback>

        <View
          style={{
            height: height - 250,
            width: width - 40,
            backgroundColor: "#fff",
            // marginTop: 20,
            borderRadius: 10
            // alignItems: "center"
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("../../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>Pet trainer details</Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              // flex: 1,
              width: width - 40,
              height: height + 140,
              marginBottom: 10
            }}
          >
            <View>
              <SectionedMultiSelect
                items={this.state.species}
                uniqueKey="id"
                showCancelButton
                modalWithSafeAreaView
                modalWithTouchable
                // primary="#e55595"
                // styles={styles.button}
                styles={{button: {backgroundColor: "#e55595"}}}
                // iconKey='icon'
                selectText="Which type of pets will you train?"
                onSelectedItemsChange={selectedItem =>
                  this.servicesDetails("petTrainer", selectedItem)
                }
                selectedItems={this.state.serviceArray.petTrainer.species}
                // selectedItems={this.state.speciesAndBreeds}
              />
            </View>
            <View/>
            <Text
              style={[
                styles.textLabel,
                {
                  // color: "#e55595",
                  marginBottom: 5,
                  fontSize: 15
                  // fontWeight: "bold"
                }
              ]}
            >
              Business description
              <Text style={{color: "red", fontSize: 10}}>*</Text>
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      placeholder={"Please fill in the details here"}
                      value={
                        this.state.serviceArray.petTrainer.businessDescription
                      }
                      onChangeText={value => {
                        this.setState({
                          serviceArray: {
                            ...this.state.serviceArray,
                            petTrainer: {
                              ...this.state.serviceArray.petTrainer,
                              businessDescription: value
                            }
                          }
                        });
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this._pickImage("businessImagesPetTrainner")}
              style={styles.profileImage}
            >
              <Image
                style={[{height: 50, width: 50}]}
                source={{uri: this.state.serviceArray.petTrainer.businessImages}}
              />
              <Text
                style={[
                  styles.textLabel,
                  {
                    color: "#e55595",
                    fontSize: 15,
                    fontWeight: "bold"
                  }
                ]}
              >

                Add Business Images
                <Text style={{color: "#e55595", fontSize: 10}}>
                  (optional)
                </Text>
              </Text>
            </TouchableOpacity>
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {
                  height: 60,
                  flex: 1,
                  marginHorizontal: 15,
                  paddingTop: 10
                }
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={() => this.onSaveServiceProviderInfo("petTrainer")}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {
                  height: 60,
                  flex: 1,
                  marginHorizontal: 15,
                  paddingTop: 10
                }
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => this.onCanceleServiceProviderInfo("petTrainer")}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </TouchableWithoutFeedback>
        {/*</TouchableOpacity>*/}
      </TouchableOpacity>
    </Modal>
  );

  // ------- Pet Sitter Modal Start ----- ////
  petSitterModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showPetSitterModal}
    >
      <TouchableOpacity
        style={{
          backgroundColor: "#00000030",
          // opacity: 0.3,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
        onPress={() => this.onCanceleServiceProviderInfo("sitter")}
      >
        <TouchableWithoutFeedback>

        <View
          style={{
            height: height - 250,
            width: width - 40,
            backgroundColor: "#fff",
            // marginTop: 20,
            borderRadius: 10
            // alignItems: "center"
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("../../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>Pet Sitter details</Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              // flex: 1,
              width: width - 40,
              height: height + 140,
              marginBottom: 10
            }}
          >
            <View>
              <SectionedMultiSelect
                items={this.state.species}
                uniqueKey="id"
                showCancelButton
                modalWithSafeAreaView
                modalWithTouchable
                styles={{button: {backgroundColor: "#e55595"}}}
                selectText="What type of pets will you sit?"
                onSelectedItemsChange={selectedItem =>
                  this.servicesDetails("petSitter", selectedItem)
                }
                selectedItems={this.state.serviceArray.petSitter.species}
              />
            </View>
            <Text
              style={[
                styles.textLabel,
                {
                  // color: "#e55595",
                  marginBottom: 5,
                  fontSize: 15
                  // fontWeight: "bold"
                }
              ]}
            >
              Business description
              <Text style={{color: "red", fontSize: 10}}>*</Text>
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      placeholder={"Please fill in the details here"}
                      value={this.state.serviceArray.petSitter.businessDescription}
                      onChangeText={value => {
                        this.setState({
                          serviceArray: {
                            ...this.state.serviceArray,
                            petSitter: {
                              ...this.state.serviceArray.petSitter,
                              businessDescription: value
                            }
                          }
                        });
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this._pickImage("businessImagesPetSitter")}
              style={styles.profileImage}
            >
              <Image
                style={[{height: 50, width: 50}]}
                source={{uri: this.state.serviceArray.petSitter.businessImages}}
              />
              <Text
                style={[
                  styles.textLabel,
                  {
                    color: "#e55595",
                    fontSize: 15,
                    fontWeight: "bold"
                  }
                ]}
              >

                Add Business Images
                <Text style={{color: "#e55595", fontSize: 10}}>
                  (optional)
                </Text>
              </Text>
            </TouchableOpacity>
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {
                  height: 60,
                  flex: 1,
                  marginHorizontal: 15,
                  paddingTop: 10
                }
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={() => this.onSaveServiceProviderInfo("petSitter")}
                disabled={this.props.loading}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {
                  height: 60,
                  flex: 1,
                  marginHorizontal: 15,
                  paddingTop: 10
                }
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => this.onCanceleServiceProviderInfo("sitter")}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </TouchableWithoutFeedback>
        {/*</TouchableOpacity>*/}
      </TouchableOpacity>
    </Modal>
  );
  //----Pet Sitter Modal End --- ///

  showOtherCategory = (data, dataType) => {
    AsyncStorage.setItem("breeder", "false");
    return (
      <View>
        <View style={{height: 30, marginTop: 20}}>
          <TouchableOpacity
            onPress={() => {
              this.showHideModal();
            }}
          >
            <Image
              source={require("../../assets/cross.png")}
              style={{
                left: "5%",
                top: "10%",
                height: 10,
                width: 10,
                position: "absolute"
              }}
              resizeMode="contain"
            />

            <Text style={{color: "gray", marginLeft: "10%"}}>
              Details Available
            </Text>
          </TouchableOpacity>
          {this.state.showPetTrainerModal ? this.petTrainerModal() : null}
          {this.state.showPetGroomerModal ? this.petGroomerModal() : null}
          {this.state.showDogWalkerModal ? this.dogWalkerModal() : null}
          {this.state.showBreederModal ? this.breederModal() : null}
          {this.state.showPetSitterModal ? this.petSitterModal() : null}
        </View>

        <TouchableHighlight
          style={{
            alignSelf: "flex-end",
            top: "15%",
            height: 30,
            width: 30,
            position: "absolute",
            marginRight: "5%"
          }}
          onPress={() =>
            this.showHideModal(dataType)
          }
        >
          <Image
            source={require("../../assets/edit.png")}
            resizeMode="contain"
          />
        </TouchableHighlight>
      </View>
    );
  };

  // ShowList start ////////////////////
  showList = item => (
    <View style={styles.subMenu}>
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate("SubProfile", {
            selectedCategory: item.category1.data.data
          })
        }
        style={styles.FlipToggleButton}
      >
        <View style={{flexDirection: "row", marginBottom: 20}}>
          <View style={{width: "80%", flexDirection: "row"}}>
            <Image
              style={{width: 24, height: 24}}
              source={require("../../assets/breeder.png")}
            />
            <Text style={styles.text1}>I am an Pet Breeder</Text>
          </View>
          <View style={{width: "20%"}}>
            <FlipToggle
              value={this.state.serviceArray.breeder.isServiceProvider}
              buttonWidth={44}
              buttonHeight={25}
              buttonRadius={50}
              buttonOffColor={"gray"}
              sliderOffColor={"#ffffff"}
              buttonOnColor={"#EF5595"}
              sliderOnColor={"#ffffff"}
              onToggle={value => {
                this.servicesSwitch("breeder", value);

                if (value === true) {
                  this.showHideModal("showBreederModal");
                }
              }}
              changeToggleStateOnLongPress={false}
            />
          </View>
        </View>
      </TouchableOpacity>
      {this.state.serviceArray.breeder.isServiceProvider === true ? (
        <View style={{marginLeft: 25}}>
          <Text style={styles.InformationHeader}>
            {item.category1.data.subdata}
          </Text>
          {this.showOtherCategory(
            item.category1.data.subdata,
            "showBreederModal"
          )}
        </View>
      ) : null}


      <TouchableOpacity
        style={styles.FlipToggleButton}
      >
        <View style={{flexDirection: "row"}}>
          <View style={{flexDirection: "row", width: "80%"}}>
            <Image
              style={{width: 24, height: 24}}
              source={require("../../assets/pet_groomer.png")}
            />
            <Text style={styles.text1}>{item.category4.data.data}</Text>
          </View>
          <View style={{width: "20%"}}>
            <FlipToggle
              value={this.state.serviceArray.petGroomer.isServiceProvider}
              buttonWidth={44}
              buttonHeight={25}
              buttonRadius={50}
              buttonOffColor={"gray"}
              sliderOffColor={"#ffffff"}
              buttonOnColor={"#EF5595"}
              sliderOnColor={"#ffffff"}
              onToggle={value => {
                this.servicesSwitch("petGroomer", value);
                if (value === true) {
                  this.showHideModal("showPetGroomerModal");
                }
              }}
              changeToggleStateOnLongPress={false}
            />
          </View>
        </View>
      </TouchableOpacity>

      {this.state.serviceArray.petGroomer.isServiceProvider === true ? (
        <View style={{marginLeft: 25}}>
          <Text style={styles.InformationHeader}>
            {item.category4.data.subdata}
          </Text>
          {this.showOtherCategory(
            item.category4.data.subdata,
            "showPetGroomerModal"
          )}
        </View>
      ) : null}
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate("SubProfile", {
            selectedCategory: item.category5.data.data
          })
        }
        style={styles.FlipToggleButton}
      >
        <View style={{flexDirection: "row"}}>
          <View style={{flexDirection: "row", width: "80%"}}>

            <Image
              style={{width: 24, height: 24}}
              source={require("../../assets/pet_trainer.png")}
            />
            <Text style={styles.text1}>{item.category5.data.data}</Text>
          </View>
          <View style={{width: "20%"}}>
            <FlipToggle
              value={this.state.serviceArray.petTrainer.isServiceProvider}
              buttonWidth={44}
              buttonHeight={25}
              buttonRadius={50}
              buttonOffColor={"gray"}
              sliderOffColor={"#ffffff"}
              buttonOnColor={"#EF5595"}
              sliderOnColor={"#ffffff"}
              onToggle={value => {
                this.servicesSwitch("petTrainer", value);
                if (value === true) {
                  this.showHideModal("showPetTrainerModal");
                }
              }}
              changeToggleStateOnLongPress={false}
            />
          </View>
        </View>
      </TouchableOpacity>
      {this.state.serviceArray.petTrainer.isServiceProvider === true ? (
        <View style={{marginLeft: 25}}>
          <Text style={styles.InformationHeader}>
            {item.category5.data.subdata}
          </Text>
          {this.showOtherCategory(
            item.category5.data.subdata,
            "showPetTrainerModal"
          )}
        </View>
      ) : null}

      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate("SubProfile", {
            selectedCategory: item.category8.data.data
          })
        }
        style={styles.FlipToggleButton}
      >
        <View style={{flexDirection: "row"}}>
          <View style={{flexDirection: "row", width: "80%"}}>

            <Image
              style={{width: 24, height: 24}}
              source={require("../../assets/walker.png")}
            />
            <Text style={styles.text1}>{item.category8.data.data}</Text>
          </View>
          <View style={{width: "20%"}}>
            <FlipToggle
              value={this.state.serviceArray.dogWalker.isServiceProvider}
              buttonWidth={44}
              buttonHeight={25}
              buttonRadius={50}
              buttonOffColor={"gray"}
              sliderOffColor={"#ffffff"}
              buttonOnColor={"#EF5595"}
              sliderOnColor={"#ffffff"}
              onToggle={value => {
                this.servicesSwitch("dogWalker", value);
                if (value === true) {
                  this.showHideModal("showDogWalkerModal");
                }
              }}
              changeToggleStateOnLongPress={false}
            />
          </View>
        </View>
      </TouchableOpacity>
      {this.state.serviceArray.dogWalker.isServiceProvider === true ? (
        <View style={{marginLeft: 25}}>
          <Text style={styles.InformationHeader}>
            {item.category8.data.subdata}
          </Text>
          {this.showOtherCategory(
            item.category8.data.subdata,
            "showDogWalkerModal"
          )}
        </View>
      ) : null}
      <TouchableOpacity
        onPress={() =>
          this.props.navigation.navigate("SubProfile", {
            selectedCategory: item.category12.data.data
          })
        }
        style={styles.FlipToggleButton}
      >
        <View style={{flexDirection: "row"}}>
          <View style={{flexDirection: "row", width: "80%"}}>

            <Image
              style={{width: 24, height: 24}}
              source={require("../../assets/pet_foster.png")}
            />
            <Text style={styles.text1}>{item.category12.data.data}</Text>
          </View>
          <View style={{width: "20%"}}>
            <FlipToggle
              value={this.state.serviceArray.petSitter.isServiceProvider}
              buttonWidth={44}
              buttonHeight={25}
              buttonRadius={50}
              buttonOffColor={"gray"}
              sliderOffColor={"#ffffff"}
              buttonOnColor={"#EF5595"}
              sliderOnColor={"#ffffff"}
              onToggle={value => {
                this.servicesSwitch("petSitter", value);
                if (value === true) {
                  this.showHideModal("showPetSitterModal");
                }
              }}
              changeToggleStateOnLongPress={false}
            />
          </View>
        </View>
      </TouchableOpacity>
      {this.state.serviceArray.petSitter.isServiceProvider === true ? (
        <View style={{marginLeft: 25}}>
          <Text style={styles.InformationHeader}>
            {item.category12.data.subdata}
          </Text>
          {this.showOtherCategory(
            item.category12.data.subdata,
            "showPetSitterModal"
          )}
        </View>
      ) : null}
    </View>
  );


  render() {
    // Checking if business save button should enable
    let shouldEnableBusinessSaveBtn = false;
    if (this.state.businessEmail.length > 0
      && this.state.phone_number.length > 0
      && this.state.businessUrl.length > 0
      && this.state.tnc && this.state.backgroundCheck) {

        // && this.state.SSN.length > 0
      // && this.state.reEnteredSSN.length > 0
      // && (this.state.SSN.toString() === this.state.reEnteredSSN.toString()

      shouldEnableBusinessSaveBtn = true;
    }

    return (
      <View style={{flex: 1, backgroundColor: "white"}}>
        <NavigationEvents
          onWillFocus={payload => {
            this.props.setHeaderTitle('Edit Profile');
            setTimeout(() => {
              this.props.enableBackButton();
            }, 300);
          }}
          onWillBlur={() => this.props.disableBackButton()}
        />
        <View style={styles.seperator}/>
        <ScrollView
          style={{paddingBottom: 10}}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{width,}}
        >
          <View
            style={{
              alignItems: "center",
              paddingBottom: 20,
              backgroundColor: "white",
              zIndex: 999,
              marginTop: 10
            }}
          >
            <View style={styles.Circle}>
              <Image
                style={{width: 100, height: 100}}
                source={{uri: this.state.profilePic}}
              />
            </View>
            <Text
              onPress={() => this._pickImage("profilePic")}
              style={{fontSize: 18, color: "#e55595"}}
            >
              Change Profile Photo
            </Text>

            <View
              style={[
                styles.radioButton1,
                {
                  flexDirection: "row",
                  marginHorizontal: 10,
                  alignItems: "center"
                }
              ]}
            >
              <Text
                onPress={this._pickImage}
                style={[styles.textLabel1, {color: "#e55595"}]}
              >
                Name
              </Text>
              <TextInput
                style={{fontSize: 18, color: "#33212D", width: 120}}
                value={this.state.name}
                returnKeyType="done"
                onChangeText={value => {
                  this.setState({
                    name: value
                  });
                }}
                onEndEditing={() => this.onUpdateUserName()}
                placeholder={"User name "}
              />
            </View>
          </View>
          <View style={styles.seperator}/>

          {/* ------------Your pets view start --------------*/}
          <Text style={styles.textLabel}>Your Pets</Text>
          <View
            style={{
              width: "100%",
              height: 100,
              marginVertical: 10,
              flex: 1,
              marginBottom: -40
            }}
          >
            <ScrollView
              showsHorizontalScrollIndicator={false}
              horizontal
              style={{
                width: "100%",
                height: 120
              }}
            >
              <View>
                <TouchableOpacity onPress={() => this.addPet()}>
                  <View
                    style={{
                      paddingLeft: 10,
                      paddingRight: 10,
                      paddingBottom: 10
                    }}
                  >
                    <Image
                      source={require("../../assets/addNewPet.png")}
                      style={{width: 40, height: 40, borderRadius: 40 / 2}}
                    />
                    <Text
                      style={{color: "#EF5595", fontSize: 12, marginLeft: 4}}
                    >
                      Add Pet
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

              {!this.props.pets ? null : this.props.pets.map((item, key) => (
                <View key={key}>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate("PetProfile", {
                        name: item.name,
                        type: item.breed,
                        species: item.species,
                        _id: item._id
                      })
                    }
                  >
                    <View
                      style={{
                        paddingLeft: 10,
                        paddingRight: 10,
                        paddingBottom: 10
                      }}
                    >
                      <Image
                        source={{
                          uri:
                            item.imagePath[0] && item.imagePath[0].url !== ""
                              ? item.imagePath[0].url
                              : "https://s3.amazonaws.com/petluvs/Leo.jpg"
                        }}
                        style={{width: 40, height: 40, borderRadius: 40 / 2}}
                      />
                      <Text
                        style={{
                          color: item.color,
                          fontSize: 12,
                          width: 40,
                          textAlign: "center"
                        }}
                        numberOfLines={1}
                      >
                        {item.name}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          </View>

          {/* ------------Your pets view start --------------*/}
          <View style={styles.seperator}/>
          <TouchableOpacity
            style={[styles.textLabel1, styles.sectionHeaderStyle, {
              backgroundColor: '#F8F8F8',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }]}
            onPress={() => {
              this.setState({isCollapsed: !this.state.isCollapsed})
            }}
          >
            <Text style={[styles.textLabel1, {fontSize: 15}]}>Business Details</Text>
            <Text style={{alignSelf: 'center', fontSize: 30, marginRight: 10}}>
              {this.state.isCollapsed ? <Icon name={'chevron-down'}></Icon> : <Icon name={'chevron-up'}></Icon>}
            </Text>
          </TouchableOpacity>

          <Collapsible collapsed={this.state.isCollapsed} align={"center"}>
            <View>
              <Text style={styles.textLabel1}>Business Email<Text style={{color: "red", fontSize: 10}}>*</Text></Text>
              <Field
                name={"businessEmail"}
                defaultValue={this.state.businessEmail}
                component={MyTextInput}
                fontSize={14}
                value={this.state.businessEmail}
                style={styles.TextInput}
                placeholder="john@gmail.com"
                placeholderTextColor="gray"
                autoCorrect={false}
                autoCapitalize="none"
                showWarning={"no"}
                address={this.state.address}
                saveValueinState={this.saveValueinState}
                selectionColor="gray"
                keyboardType="email-address"
                underlineColorAndroid="transparent"
                onChange={value => this.setState({...this.state, businessEmail: value})}
              />
            </View>
            <View>
              <Text style={styles.textLabel1}>Phone number<Text style={{color: "red", fontSize: 10}}>*</Text></Text>

              <Field
                name={"phoneNumber"}
                defaultValue={this.state.phone_number}
                component={MyTextInput}
                fontSize={14}
                style={styles.TextInput}
                placeholder="eg:1234567890"
                placeholderTextColor="gray"
                keyboardType={"numeric"}
                value={this.state.phone_number}
                autoCorrect={false}
                autoCapitalize="none"
                showWarning={"no"}
                address={this.state.address}
                saveValueinState={this.saveValueinState}
                selectionColor="gray"
                underlineColorAndroid="transparent"
                onChange={value =>
                  this.setState({...this.state, phone_number: value})
                }
              />
            </View>
            <View>
              <Text style={styles.textLabel1}>Business Url</Text>

              <Field
                name={"businessUrl"}
                defaultValue={this.state.businessUrl}
                component={MyTextInput}
                fontSize={14}
                style={styles.TextInput}
                placeholder="eg:www.petluvs.com"
                placeholderTextColor="gray"
                keyboardType={"default"}
                value={this.state.businessUrl}
                autoCorrect={false}
                autoCapitalize="none"
                showWarning={"no"}
                address={this.state.address}
                saveValueinState={this.saveValueinState}
                selectionColor="gray"
                underlineColorAndroid="transparent"
                onChange={value =>
                  this.setState({...this.state, businessUrl: value})
                }
              />
            </View>
            {/* <View>
              <Text style={styles.textLabel1}>Enter your SSN<Text style={{color: "red", fontSize: 10}}>*</Text></Text>

              <Field
                name={"SSN"}
                defaultValue={this.state.SSN}
                component={MyTextInput}
                fontSize={14}
                style={styles.TextInput}
                placeholder="eg:1234567890"
                placeholderTextColor="gray"
                keyboardType={"numeric"}
                value={this.state.SSN}
                autoCorrect={false}
                autoCapitalize="none"
                showWarning={"no"}
                // address={this.state.address}
                saveValueinState={this.saveValueinState}
                selectionColor="gray"
                underlineColorAndroid="transparent"
                onChange={value =>
                  this.setState({...this.state, SSN: value})
                }
              />
            </View> */}
            <View>
              {/* <Text style={styles.textLabel1}>Re-enter SSN<Text style={{color: "red", fontSize: 10}}>*</Text></Text> */}

              {/* <Field
                name={"reEnteredSSN"}
                defaultValue={this.state.reEnteredSSN}
                component={MyTextInput}
                fontSize={14}
                style={styles.TextInput}
                placeholder="eg:1234567890"
                placeholderTextColor="gray"
                keyboardType={"numeric"}
                value={this.state.reEnteredSSN}
                autoCorrect={false}
                autoCapitalize="none"
                showWarning={"no"}
                // address={this.state.address}
                saveValueinState={this.saveValueinState}
                selectionColor="gray"
                underlineColorAndroid="transparent"
                onChange={value =>
                  this.setState({...this.state, reEnteredSSN: value})
                }
              /> */}
              <TouchableOpacity
                onPress={() => this._pickImage("ID_proof")}
                style={styles.profileImage}
              >
                {!this.state.ID_proof ? null : (
                  <Image
                    style={[{height: 50, width: 50}]}
                    source={{uri: this.state.ID_proof}}
                  />
                )}
                <Text
                  style={[
                    styles.textLabel,
                    {
                      color: "#e55595",
                      fontSize: 15,
                      fontWeight: "bold"
                    }
                  ]}
                >
                  {this.state.ID_proof ? "Selected" : "Upload a valid ID"}
                  <Text e55595yle={{color: "red", fontSize: 10}}>
                    {!this.state.ID_proof ? '*' : null}
                  </Text>
                </Text>
              </TouchableOpacity>

              <View
                style={{
                  marginHorizontal: 15,
                  marginTop: 10,
                  marginBottom: 0
                }}
              >
                <View style={{flexDirection: "row"}}>
                  <CheckBox
                    label="I agree to a background check"
                    checkedImage={require("../../assets/check.png")}
                    uncheckedImage={require("../../assets/uncheck.png")}
                    onChange={checked =>
                      this.setState({
                        backgroundCheck: checked
                      })
                    }
                  />
                  <Text style={{color: "red", fontSize: 10}}>*</Text>
                </View>
                <View style={{flexDirection: "row"}}>
                  <CheckBox
                    label="I agree to Petluvs terms and conditions"
                    checkedImage={require("../../assets/check.png")}
                    uncheckedImage={require("../../assets/uncheck.png")}
                    onChange={checked =>
                      this.setState({
                        tnc: checked
                      })
                    }
                  />
                  <Text style={{color: "red", fontSize: 10}}>*</Text>
                </View>
              </View>

            </View>
            <View style={[styles.saveButtonContainer, {marginTop: 0}]} opacity={!shouldEnableBusinessSaveBtn ? 0.5 : 1}>
              <TouchableOpacity
                style={[styles.saveButton, {marginTop: 0,}]}
                onPress={() => this.onSaveBusinessDetails("businessDetails")}
                disabled={!shouldEnableBusinessSaveBtn || this.props.loading}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"

                  }}
                >
                  {this.props.loading ?
                    <ActivityIndicator size="small" color="#0000ff"/>
                    : <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                  }
                </View>
              </TouchableOpacity>
            </View>
          </Collapsible>

          <View style={styles.seperator}/>

          <View
            style={[styles.textLabel1, styles.sectionHeaderStyle, {
              backgroundColor: '#F8F8F8',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center'
            }]}
          >
            <Text style={[styles.textLabel1, {fontSize: 15}]}>Business Account Type</Text>
          </View>
          {this.showList(this.state.items)}
          <View style={{height: 40, marginBottom: 40}}/>
        </ScrollView>
        {this.confirmationModal()}
      </View>
    );
  }
}

const EditProfileForm = reduxForm({
  form: "editProfileForm",
  validate: values => {
    const errors = {};
    const emailReg = /\S+@\S+\.\S+/g;
    errors.email = !values.email
      ? "email not valid"
      : emailReg.test(values.email) === true
        ? undefined
        : "email format is invalid";
    const reg = /^[0-9]*$/;
    errors.phoneNumber = !values.phoneNumber
      ? "please enter a phone number"
      : !reg.test(values.phoneNumber)
        ? "not a number"
        : undefined;
    return errors;
  }
})(EditProfile);

const mapStateToProps = state => ({
  loading: state.user.loading || state.pets.loading,
  user: state.user.user,
  pets: state.pets.pets,
  speciesAndBreeds: state.playDates.speciesAndBreeds,
  errors: state.user.erros
});

export default connect(
  mapStateToProps,
  {
    userProfile,
    fetchAllPets,
    updateUserProfile,
    updateUserProfileV2,
    setHeaderTitle,
    enableBackButton,
    disableBackButton
  }
)(EditProfileForm);
