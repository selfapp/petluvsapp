import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  Circle: {
    height: 100,
    width: 100,
    borderRadius: 50,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20
  },
  MainView: {
    height: width / 2.3,
    width: width / 2.3,
    borderRadius: 6,
    margin: 4,
    overflow: "hidden"
  },
  Tabs: {
    borderTopColor: "rgba(217, 214, 216, .2)",
    borderTopWidth: 2,
    borderStyle: "solid",
    zIndex: 9999,
    marginTop: -2
  },
  PetName: {
    fontSize: 18,
    color: "white"
  },
  PetType: {
    fontSize: 12,
    color: "white",
    opacity: 0.75,
    marginBottom: 5
  },
  PetImage: {
    width: 170,
    height: 170,
    justifyContent: "flex-end",
    padding: 10
  }
});
