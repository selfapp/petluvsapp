import React from "react";
import {
  Text,
  View,
  FlatList,
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Modal,
  Dimensions,
  AsyncStorage
} from "react-native";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import connect from "react-redux/es/connect/connect";
import {Location, Permissions} from "expo";
import {NavigationEvents} from "react-navigation";

import {HOTSPOT_CATEGORIES} from "../utility/utils";
import HotSpotsMap from "../components/HotSpotsMap";
import {baseurl} from "../BaseUrl";
import {
  removeHeaderTitle
} from '../store/actions';

const foursquare = require("react-native-foursquare-api")({
  clientID: "REX11TZ1KY44X2MP2GKZEUUZQFIZWAUKIVPPSEWH3RLCSSKL",
  clientSecret: "DPOMAOH1LO54DSTZT3QSQQLRNLJ4CIQ1BQUR30WQCTVIFUFW",
  style: "foursquare",
  version: "20180806"
});

const {height, width} = Dimensions.get("window");

class HotSpotsScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  categories = [
    {title: "Dog Parks", icon: require("../assets/mapsMarker/petBreeder.png"), count: 0},
    {
      title: "Pet Friendly Restaurants",
      icon: require("../assets/mapsMarker/petTrainer.png"),
      count: 0
    },
    {
      title: "Pet Friendly Hotels",
      icon: require("../assets/mapsMarker/petSitter.png"),
      count: 0
    },
    {title: "Cat Cafes", icon: require("../assets/mapsMarker/catCafes.png"), count: 0},
    {title: "Veterinarians", icon: require("../assets/mapsMarker/veterinarians.png"), count: 0}
  ];

  constructor(props) {
    super(props);
    this.state = {
      // category: "",
      latitude: 0,
      longitude: 0,
      markers: [],
      items: [],
      markerIcon: "",
      showBreederModal: false,
      breeds: [],
      comments: "",
      multiSpecies: [],
      locationStatus: false
    };
  }

  fetchMarkerData = title => {
    const params = {
      ll: `${this.state.latitude.toString()},${this.state.longitude.toString()}`,
      query: title
    };
    foursquare.venues
      .getVenues(params)
      .then(venues => {
        this.setState({
          items: venues.response.venues
        });
      })
      .catch(err => {
        // console.log(err);
      });
  };

  // new function.
  fetchSearchData = async title => {
    const userToken = await AsyncStorage.getItem("token");
    const {latitude, longitude} = this.state;

    try {
      const response = await fetch(
        `${baseurl}/places/place-suggestions-google?query=${title}&lat=${latitude}&lng=${longitude}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${userToken}`
          }
        }
      );
      const responseJson = await response.json();

      await this.setState({
        items: responseJson.response.data
      });
    } catch (e) {
      // console.log(`Error Fetching search data${e}`);
    }
  };

  componentDidMount() {
    // this.makeArrayOfSpeciesAndBreeds();
    this.getLocationAsync();
    // this.getUserLocation();
  }

  getUserLocation = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          locationStatus: true
        });
        // console.log(
        //   "In hotspot screen",
        //   this.state.latitude,
        //   this.state.longitude
        // );
        const params = {
          ll: `${latitude},${longitude}`,
          query: this.state.searchText
        };
        this.setState({
          params,
          locationStatus: true
        });
        // this.onCategoryPress(
        //   "Animal Rescue Services",
        //   require("../assets/marker_rescue.png")
        // );
      },
      error => this.setState({error: error.message, locationStatus: false}),
      {enableHighAccuracy: true, timeout: 2000, maximumAge: 2000}
    );
  };

  getLocationAsync = async () => {
    if (!this.state.locationStatus) {
      const {status} = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== "granted") {
        this.setState({
          errorMessage: "Permission to access location was denied"
        });
      }

      const location = await Location.getCurrentPositionAsync({});
      await this.setState({location});
      const {latitude, longitude} = this.state.location.coords;

      const params = {
        ll: `${latitude},${longitude}`,
        query: this.state.searchText
      };
      await this.setState({
        ...this.state,
        latitude,
        longitude,
        params,
        locationStatus: true
      });
    }
  };
  onCategoryPress = async (title, icon) => {
    if (this.state.locationStatus) {
      this.setState({
        category: title,
        markerIcon: icon
      });
      // console.log("pill pressed", this.state.category, title, icon);
      // this.fetchMarkerData(title);
      // this.getUserLocation();
    }
    await this.getLocationAsync();
    this.fetchSearchData(title);
  };

  makeArrayOfSpeciesAndBreeds() {
    const result = [];
    this.props.speciesAndBreeds.map(item => {
      const species = {
        name: item.speciesName,
        id: item.speciesName,
        children: []
      };

      item.breeds.map(breed => {
        species.children.push({
          name: breed,
          id: breed
        });
      });

      result.push(JSON.parse(JSON.stringify(species)));
    });

    this.setState({multiSpecies: result});
  }

  servicesDetails = async (service, selectedItem) => {
    // const breederspecies = [ this.state.serviceArray.breeder.species];
    const selecteditem = [
      ...this.state.serviceArray[service].breeds,
      ...selectedItem
    ];
    const speciesArray = await Array.from(new Set(selecteditem));
    this.setState({
      serviceArray: {
        ...this.state.serviceArray,
        [service]: {
          ...this.state.serviceArray[service],
          // [serviceDetail]: value
          breeds: speciesArray
          // isServiceProvider: !this.state[service].isServiceProvider
        }
      }
    });
    // console.log(" in side service switch", this.state.serviceArray);
  };

  animalRescuerModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.showBreederModal}
    >
      <View
        style={{
          backgroundColor: "#00000030",
          // opacity: 0.3,
          alignItems: "center",
          justifyContent: "center",
          flex: 1
          // marginHorizontal: 10
        }}
      >
        <View
          style={{
            height: height - 300,
            width: width - 40,
            backgroundColor: "#fff",
            // marginTop: 20,
            borderRadius: 10
            // alignItems: "center",
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Text style={{fontSize: 17}}>
              I Am Looking For A {this.state.category}
            </Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              // flex: 1,
              width: width - 40,
              height: height + 140,
              marginBottom: 10,
              paddingHorizontal: 10
            }}
          >
            {/*{this.state.category !== "Dog walker" ? (*/}
            <View>
              <Text
                style={[
                  styles.textLabel,
                  {
                    // color: "#e55595",
                    marginBottom: 5,
                    fontSize: 15
                    // fontWeight: "bold"
                  }
                ]}
              >
                {this.state.category === "Pet Breeder"
                  ? "Type of Breed(s) I am Looking For"
                  : null}
                {this.state.category === "Pet Boarding" ? "My pet is a" : null}
                {this.state.category === "Pet Daycare" ? "My pet is a" : null}

                {this.state.category === "Pet Friendly Restaurant"
                  ? "Type of Breed(s) I am Looking For"
                  : null}
                {this.state.category === "Dog walker" ? "My dog is a " : null}
                {this.state.category === "Dog parks"
                  ? "Type of Breed(s) I am Looking For"
                  : null}
                {this.state.category === "Pet Trainer" ? "My pet is a" : null}
                {this.state.category === "Pet Groomers" ? "My pet is a" : null}
              </Text>

              <SectionedMultiSelect
                items={this.state.multiSpecies}
                uniqueKey="id"
                // items={this.state.multiSpecies}
                // uniqueKey="id"
                subKey={"children"}
                showCancelButton
                modalWithSafeAreaView
                modalWithTouchable
                // primary="#e55595"
                // styles={styles.button}
                styles={{button: {backgroundColor: "#e55595"}}}
                // iconKey='icon'
                selectText="Please choose from the list"
                showDropDowns
                readOnlyHeadings
                onSelectedItemsChange={selectedItem =>
                  this.setState({breeds: selectedItem})
                }
                selectedItems={this.state.breeds}
              />
            </View>
            // ) : null}
            <Text
              style={[
                styles.textLabel,
                {
                  // color: "#e55595",
                  marginBottom: 5,
                  fontSize: 15
                  // fontWeight: "bold"
                }
              ]}
            >
              Comments
            </Text>
            <View
              style={{
                height: 40,
                // width: '100%',
                marginHorizontal: 15,
                marginTop: 5,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    {/*<RadioButton isChecked={this.state.nofityFriends} />*/}
                    <TextInput
                      style={{
                        fontSize: 12,
                        // color: this.state.nofityFriends ? "#e55595" : "grey",
                        paddingLeft: 5
                      }}
                      value={this.state.comments}
                      onChangeText={value => {
                        // this.servicesDetails("breeder", "akcNumber", value)
                        this.setState({
                          comments: value
                        });
                      }}
                      placeholder={"Please fill in the details here"}
                    />
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={() => {
                  this.setState({
                    showBreederModal: !this.state.showBreederModal
                  });
                }}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Save</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => {
                  this.setState({
                    showBreederModal: !this.state.showBreederModal
                  });
                }}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {/*</TouchableWithoutFeedback>*/}
      </View>
    </Modal>
  );

  render() {
    return (
      <View style={{flex: 1}}>
        <NavigationEvents
          onWillFocus={payload => this.props.removeHeaderTitle()}
        />

        <HotSpotsMap
          style={{flex: 1}}
          category={this.state.category}
          latitude={this.state.latitude}
          longitude={this.state.longitude}
          items={this.state.items}
          navigation={this.props.navigation}
          markerIcon={this.state.markerIcon}
        />
        <View
          style={{
            position: "absolute",
            zIndex: 99,
            width: "100%",
            marginTop: 10,
            paddingHorizontal: 20
          }}
        >
          <FlatList
            horizontal
            data={this.categories}
            showsHorizontalScrollIndicator={false}
            renderItem={({item, index}) => (
              <TouchableHighlight
                // disabled={!this.state.locationStatus}
                onPress={() => {
                  // for
                  let target = this.categories[index];
                  target.count = target.count + 1;
                  this.categories = [...this.categories];

                  this.setState({
                    // showBreederModal: !this.state.showBreederModal
                    showBreederModal: false
                  });
                  // this.openPopup(true);
                  this.onCategoryPress(item.title, item.icon);
                }}
                style={[
                  styles.CategoryButton,
                  {opacity: this.state.category === item.title ? 0.9 : 0.5}
                ]}
              >
                <Text style={{color: "white", fontSize: 15}}>
                  {item.title}
                </Text>
              </TouchableHighlight>
            )}
            keyExtractor={(item, index) => index.toString()}
            extraData={HOTSPOT_CATEGORIES}
          />
        </View>
        {this.animalRescuerModal()}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.user.loading || state.pets.loading,
  user: state.user.user,
  pets: state.pets.pets,
  speciesAndBreeds: state.playDates.speciesAndBreeds
});
const styles = StyleSheet.create({
  CategoryButton: {
    backgroundColor: "#EF5595",
    paddingVertical: 6,
    paddingHorizontal: 10,
    marginRight: 12,
    marginHorizontal: 10,
    borderRadius: 4
  },
  saveButton: {
    height: 50,
    width: "100%",
    borderRadius: 10,
    backgroundColor: "#e55595",
    margin: 20
  },
  saveButtonContainer: {
    // alignSelf: "flex-end",
    marginHorizontal: 15,
    alignItems: "center",
    justifyContent: "center",
    height: "10%",
    marginBottom: 10,
    // width: "100%",
    marginTop: 10
  }
});

// export default HotSpotsScreen;
export default connect(
  mapStateToProps,
  {
    removeHeaderTitle
  }
)(HotSpotsScreen);
