import React, {Component} from "react";
import {
  Dimensions,
  Text,
  TouchableOpacity,
  View,
  FlatList
} from "react-native";
import connect from "react-redux/es/connect/connect";
import {NavigationEvents} from "react-navigation";
import moment from "moment";

import {
  notificationList,
  getNotificationList,
  deleteNotificationList,
  readNotificationList,
  setHeaderTitle,
  enableBackButton,
  disableBackButton,
  resetNotificationsCount
} from "../store/actions";
import GeneralStyles from "../Styles/General";
import ProfilePhoto from "../components/ProfilePhoto";

const width1 = Dimensions.get("window").width;

class Notifications extends Component {
  async componentDidMount() {
    await this.props.getNotificationList();
    this.props.resetNotificationsCount();
  }

  state = {
    refreshing: false
  }

  delete = async () => {
    await this.props.deleteNotificationList();
    await this.props.getNotificationList();
  };

  getTimeDiff(createdAt){
    let data = moment.duration(moment().diff(moment(createdAt)));
    var days = parseInt(data.asDays());
    var hours = parseInt(data.asHours());
    hours = hours - days*24;
    var minutes = parseInt(data.asMinutes());
    minutes = minutes - (days*24*60 + hours*60);
    var diffTime = "";
    if (days){
      diffTime = days + 'd ago';
    } else if (hours){
      diffTime = hours + 'h ago';
    } else if (minutes){
      diffTime = minutes + 'm ago';
    } else {
      diffTime = 'Just Now'
    }
    return(<Text style={{fontSize: 12, color: "#fff"}}>{diffTime}</Text>)

  }

  handleRefresh = async () => {
    if (this.state.refreshing) return;
    await this.setState({refreshing: true},
      async () => { await this.props.getNotificationList();}
    );

    await this.setState({ refreshing: false });
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: "#F7F9FF"}}>
        <NavigationEvents
          onWillFocus={() => {
            this.props.setHeaderTitle('Notifications');
            setTimeout(() => {
              this.props.enableBackButton();
            }, 10);
          }}
          onWillBlur={() => this.props.disableBackButton()}
        />
        <View style={{marginVertical: 10, alignItems: 'flex-end'}}>
          <TouchableOpacity
            style={{
              height: 30,
              width: 70,
              borderRadius: 10,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#e55595",
              marginRight: 20
            }}
            onPress={() => this.delete()}
          >
            <Text style={{color: "#fff"}}>Clear all</Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          {this.props.notifications.length > 0 ? ( 
          <FlatList
            data={this.props.notifications}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.props.readId}
            onRefresh={() => this.handleRefresh()}
            refreshing={this.state.refreshing}
            renderItem={({item}) => (
              <View
                style={[
                  GeneralStyles.Card,
                  {
                    marginBottom: 4,
                    padding: 5,
                    paddingHorizontal: 3,
                    marginHorizontal: 15,
                    width: width1 - 30,
                    shadowColor: "#e55595",
                    backgroundColor: item.read ? "#f2aac7" : "#e55595"
                  }
                ]}
              >
                <View style={{ padding: 10, flexDirection: "row"}}>
                  <ProfilePhoto photo={ item.user && item.user.photo ? item.user.photo : "https://petluvsstore.s3.amazonaws.com/Petluvs-defaults/petluvs.png"}/>
                  <View style={{ flex:1, paddingLeft: 3 }}>
                    <View style={{flex: 1}}>
                      <TouchableOpacity
                        style={{alignSelf: "center"}}
                        onPress={() => {
                          this.props.readNotificationList(item._id);

                          switch (item.category) {
                            case 1:
                              this.props.navigation.navigate("PlaydateDetails", {
                                playdateId: item.dataId,
                                fromNotification: true,
                                reviewThePlaydate: true
                              });
                              break;

                            case 2:
                              this.props.navigation.navigate("PlaydateDetails", {
                                playdateId: item.dataId,
                                fromNotification: true,
                                reviewThePlaydate: false
                              });
                              break;

                            case 4:
                              this.props.navigation.navigate("Home");
                              break;

                            default: {
                              this.props.navigation.navigate("Home");
                            }
                          }
                        }}
                      >
                        <Text style={{fontSize: 17, color: "#fff"}}>
                          {item.body}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View style={{ alignItems:'flex-end',width: 50 }}>
                    {this.getTimeDiff(item.createdAt)}
                  </View>
                </View>


              </View>
            )}
          />) : (null)}

        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.playDates.loading || state.user.loading,
  errors: state.user.errors,
  notifications: state.user.notifications,
  readId: state.user.readId
});

export default connect(
  mapStateToProps,
  {
    notificationList,
    getNotificationList,
    deleteNotificationList,
    readNotificationList,
    setHeaderTitle,
    enableBackButton,
    disableBackButton,
    resetNotificationsCount
  }
)(Notifications);
