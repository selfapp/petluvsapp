import React from "react";
import {
    Text,
    View,
    ScrollView,
    Dimensions,
    StyleSheet,
    TouchableOpacity
} from "react-native";
import Header from "../components/Header";

import MyTextInput from "../components/UI/MyTextInput";
import {reduxForm, Field} from "redux-form";
import {ImagePicker, Permissions} from "expo";

class SaveServiceProviderInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            details: false,
            image: null,
            imageBase64: null
        };
    }

    static navigationOptions = {
        header: null,
        gesturesEnabled: false
    };

    renderHeader() {
        return (
            <Header
                navigation={this.props.navigation}
                title={this.props.navigation.state.params.data}
            />
        );
    }

    saveProfile() {
        if (
            this.props.navigation.state.params.data == "Animal Breeder Information"
        ) {
            return (
                <Text style={{color: "#FFFFFF", fontSize: 16}}>
                    Save Breeder Profile
                </Text>
            );
        }
        return <Text style={{color: "#FFFFFF", fontSize: 16}}>Save Profile</Text>;
    }

    americanKennelNo() {
        return <Text style={styles.textLabel}>Enter Details</Text>;
    }

    _pickImage = async () => {
        await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
            base64: true
        });

        if (!result.cancelled) {
            this.setState({imageUri: result.uri});
            this.setState({imageBase64: result.base64});
        }
    };

    render() {
        const formStates = [
            "active",
            "autofilled",
            "asyncValidating",
            "dirty",
            "invalid",
            "pristine",
            "submitting",
            "touched",
            "valid",
            "visited"
        ];
        return (
            <View style={{flex: 1, backgroundColor: "#fff"}}>
                {this.renderHeader()}
                <ScrollView
                    style={{flex: 1}}
                    showsVerticalScrollIndicator={false}
                    removeClippedSubviews
                >
                    <View>
                        {this.americanKennelNo()}

                        {/*<View style={styles.ViewShadow}>*/}
                        <Field
                            name={"SaveServiceProviderInfo"}
                            component={MyTextInput}
                            fontSize={14}
                            style={styles.TextInput}
                            placeholder="DN12345678"
                            placeholderTextColor="gray"
                            multiline
                            showWarning
                            autoCorrect={false}
                            autoCapitalize="none"
                            selectionColor="gray"
                            value={this.state.details}
                            onChange={text => this.setState({details: text})}
                            keyboardType="email-address"
                            underlineColorAndroid="transparent"
                            // validate={[minLength(10)]}
                            // warn={alphaNumeric}
                        />
                        {/*</View>*/}
                        {/*{formStates.filter(state => this.props[state]).map(state =>*/}
                        {/*(*/}
                        {/*<Text key={state}> - {state}</Text>*/}
                        {/*)*/}

                        {/*)}*/}

                        <TouchableOpacity
                            onPress={() => this._pickImage()}
                        >
                            <View style={styles.profileImage}>
                                <Text style={{color: "#e55595"}}>upload images</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                // this.props.handleSubmit(() => {
                                //   this.props.navigation.navigate("Home");
                                // });
                                if (
                                    formStates.filter(state => this.props[state])[1] ===
                                    "valid" &&
                                    formStates.filter(state => this.props[state])[0] === "dirty"
                                ) {
                                    // this.props.navigation.navigate("Home");
                                    this.props.navigation.goBack();
                                    this.props.navigation.state.params.saveBreederInfo(
                                        this.props.navigation.state.params.data,
                                        this.state.details,
                                        this.state.imageBase64
                                    );

                                }
                            }}
                        >
                            <View style={styles.ButtonView}>
                                {}
                                {this.saveProfile()}
                            </View>
                            <Text>{}</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const {height, width} = Dimensions.get("window");
const styles = StyleSheet.create({
    wrap: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        height: "100%",
        width: "100%"
    },
    textLabel: {
        margin: 15
    },
    ViewShadow: {
        flexDirection: "row",
        marginHorizontal: 15,
        borderColor: "#000",
        justifyContent: "center",
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 5,
        elevation: 5,
        shadowColor: "#e55595"
    },
    TextInput: {
        flex: 1,
        height: height * 0.21,
        backgroundColor: "#FFFFFF",
        color: "gray",
        paddingLeft: 10,
        alignItems: "flex-start"
    },
    profileImage: {
        // flex: 1,
        // resizeMode: 'contain',
        borderColor: "#e55595",
        borderStyle: "dashed",
        width: "85%",
        height: 60,
        borderRadius: 10,
        alignItems: "center",
        borderWidth: 1,
        marginTop: 30,
        justifyContent: "center",
        alignSelf: "center"
    },
    ButtonView: {
        backgroundColor: "#e55595",
        height: height * 0.07,
        borderRadius: 5,
        marginBottom: 10,
        marginTop: 50,
        marginHorizontal: 15,
        alignItems: "center",
        justifyContent: "center"
    }
});

export default reduxForm({
    form: "SaveServiceProviderInfo",
    validate: values => {
        // const emailReg = /\S+@\S+\.\S+/g;
        const errors = {};
        if (values.BreederInformation) {
            errors.BreederInformation =
                values.BreederInformation.length < 10
                    ? "Some details is required"
                    : undefined;
        } else {
            return undefined;
        }
        return errors;
    }
})(SaveServiceProviderInfo);
