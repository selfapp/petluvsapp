import React, { Component } from "react";
import { connect } from "react-redux";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  TextInput,
  Dimensions
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { signin } from "../../store/actions/index";
import Spinner from "../../components/UI/activityIndicator";

// const EMAIL_REGEX = /\S*\@\S*\.\S+/g;
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
class SignIn extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: true
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      access_token: "",
      loader: false,
      errors: "",
      error: ""
    };
  }

  //need to pass the data
  signInHandler = async () => {
    const data = {
      email: this.state.email,
      password: this.state.password
    };
    // if (this.isValid() === true) {
      let res = await this.props.signin(data);
      if (res['success'] == true) {
        let confirmed = this.props.authInfo.confirmed;
        if (confirmed) {
          this.props.navigation.navigate("AuthLoading");
        } else {
          alert("Please confirm your email and log back in.");
        }      
      } else {
        alert(res.message);
      }
    // } 
  };

  // isValid() {
  //   const { email, password } = this.state;
  //   let valid = false;
  //   const reg = /\S+@\S+\.\S+/g;

  //   if (email.length > 0 && password.length > 0) {
  //     console.log(password.length);
  //     if (reg.test(email) === false) {
  //       alert("Please enter a valid email address.");
  //     } else if (password.trim().length <= 5) {
  //       alert("Password must contain 6 characters.");
  //     } else {
  //       valid = true;
  //     }
  //   }

  //   if (email.length === 0) {
  //     alert("Please enter an email address.");
  //   } else if (password.length === 0) {
  //     alert("Please enter a password.");
  //   }

  //   return valid;
  // }

  // componentWillReceiveProps(nextProps) {
  //   this.setState({ error: "" });
  // }

  // componentDidUpdate(prevProps) {
  //   if (prevProps.errors !== this.props.errors) {
  //     if (this.props.errors != null) {
  //       alert(this.props.errors);
  //     }
  //   }
  // }

  isSignInDataValid = () => {
    let isValid = false;
    let { email, password } = this.state;

    if (EMAIL_REGEX.test(email) && password.length >= 6) {
      isValid = true;
    }
    if (!EMAIL_REGEX.test(email) && password.length < 6) {
      isValid = false;
    } 
    return isValid;
  };

  render() {
    let isEnabled = this.isSignInDataValid();

    if (this.props.loading === true) {
      return <Spinner />;
    }

    return (
      <View style={styles.wrap}>
        <View style={styles.bgWrap}>
          <Image
            source={require("../../assets/login.png")}
            style={styles.bgPhoto}
          />
        </View>
        <KeyboardAwareScrollView
          ref="scroll"
          extraScrollHeight={10}
          keyboardOpeningTime={0}
          extraHeight={50}
          keyboardShouldPersistTaps="handled"
          enableOnAndroid
          enableResetScrollToCoords
        >
          <View style={styles.container}>
            <Image
              source={require("../../assets/loginLogo.png")}
              style={styles.loginLogo}
            />

            <View style={{ marginTop: 10, marginHorizontal: 5 }}>
              <View style={{ flexDirection: "row", marginTop: 350 }}>
                <View
                  style={{
                    width: 40,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FFFFFF"
                  }}
                >
                  <Image source={require("../../assets/user.png")} />
                </View>
                <TextInput
                  fontSize={14}
                  style={styles.TextInput}
                  placeholder="Email"
                  placeholderTextColor="gray"
                  autoCorrect={false}
                  autoCapitalize="none"
                  selectionColor="gray"
                  keyboardType="email-address"
                  underlineColorAndroid="transparent"
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                />
              </View>
              <View
                style={{ flexDirection: "row", marginTop: 10, width: "100%" }}
              >
                <View
                  style={{
                    width: 40,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FFFFFF"
                  }}
                >
                  <Image source={require("../../assets/lock.png")} />
                </View>
                <TextInput
                  fontSize={14}
                  style={styles.TextInput}
                  placeholder="Password"
                  placeholderTextColor="gray"
                  autoCorrect={false}
                  autoCapitalize="none"
                  selectionColor="gray"
                  secureTextEntry
                  underlineColorAndroid="transparent"
                  onChangeText={password => this.setState({ password })}
                  value={this.state.password}
                />
              </View>

              <TouchableOpacity
                onPress={() => {
                  this.signInHandler();
                }}
                disabled={isEnabled === false}
              >
                <View
                  style={[
                    styles.ButtonView,
                    { opacity: isEnabled === true ? 1.0 : 0.5 }
                  ]}
                >
                  {this.state.loader ? (
                    <ActivityIndicator
                      style={{
                        zIndex: 2,
                        position: "absolute",
                        bottom: 0,
                        top: 0,
                        alignSelf: "center"
                      }}
                      color="#FFFFFF"
                      size="large"
                    />
                  ) : null}
                  <Text style={{ color: "#FFFFFF", fontSize: 22 }}>Login</Text>
                </View>
              </TouchableOpacity>

              <Text
                style={{
                  color: "#000",
                  textDecorationLine: "underline",
                  fontSize: 18,
                  marginTop: 8,
                  alignSelf: "center"
                }}
                onPress={() => this.props.navigation.navigate("ForgotPassword")}
              >
                Forgot password?
              </Text>

              <Text
                onPress={() => this.props.navigation.navigate("Register")}
                style={{
                  color: "#000",
                  fontWeight: "bold",
                  fontSize: 18,
                  marginTop: 20,
                  alignSelf: "center"
                }}
              >
                New User? Register Here
              </Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const { height } = Dimensions.get("window");
const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    height: "100%",
    width: "100%"
  },
  bgWrap: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    width: "100%",
    height: "100%"
  },
  bgPhoto: {
    alignSelf: "center",
    flex: 1,
    resizeMode: "cover",
    bottom: 0
  },
  container: {
    flex: 1,
    padding: 20,
    alignItems: "center",
    justifyContent: "flex-end"
  },
  loginLogo: {
    resizeMode: "contain",
    position: "absolute",
    top: "10%"
  },

  ButtonView: {
    backgroundColor: "#e55595",
    height: height * 0.07,
    borderRadius: 2,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10
  },
  TextInput: {
    flex: 1,
    height: height * 0.07,
    backgroundColor: "#FFFFFF",
    color: "gray"
  }
});
const mapStateToProps = state => ({
  token: state.auth.token,
  loading: state.auth.loading,
  errors: state.auth.errors || null,
  errorMsg: state.auth.errorMsg || null,
  authInfo: state.auth.authInfo
});

export default connect(
  mapStateToProps,
  { signin }
)(SignIn);
