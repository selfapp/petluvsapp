import React, { Component } from "react";
import {
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  StyleSheet,
  Image
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Header from "../../components/Header";
import { resetPassword } from "../../store/actions";

const height1 = Dimensions.get("window").height;
const width1 = Dimensions.get("window").width;

const EMAIL_REGEX = /\S*\@\S*\.\S+/g;

export default class ResetPassword extends Component {
  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      header: (
        <Header
          title={"Reset Password"}
          navigation={navigation}
          hideNotificationBell={true}
        />
      ),
      gesturesEnabled: true
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      cPassword: ""
    };
  }

  onResetPassword = async () => {
    let { email, password, cPassword } = this.state;
    let data = {
      email: email,
      password: password,
      cPassword: cPassword
    }

    if (!this.canBeSubmitted)
      return;
    let res = await resetPassword(data);
    if (res['response']['success']) {
      alert(res['response']['msg']);
      await this.setState({
        email: "",
        password: "",
        cPassword: ""
      })
    } else {
      alert(res['response']['msg']);
    }
  }

  canBeSubmitted = () => {
    let isValid = false;
    let { email, password, cPassword } = this.state;

    if ( EMAIL_REGEX.test(email) &&  password.length >= 6 && cPassword === password){
      isValid = true;
    }

    return isValid;
  }

  render() {
    const navigate = this.props.navigation.navigate;
    const isEnabled = !this.canBeSubmitted();
    return (
      <View style={styles.wrap}>
        <View style={styles.bgWrap}>
          <Image
            source={require("../../assets/login.png")}
            style={styles.bgPhoto}
          />
        </View>
        <KeyboardAwareScrollView
          ref="scroll"
          extraScrollHeight={10}
          keyboardOpeningTime={0}
          extraHeight={50}
          keyboardShouldPersistTaps="handled"
          enableOnAndroid
          enableResetScrollToCoords
        >
          <View style={styles.container}>
            <View style={{ marginTop: 10, marginHorizontal: 5 }}>
              <View style={{ flexDirection: "row", marginTop: 100 }}>
                <View
                  style={{
                    width: 40,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FFFFFF"
                  }}
                >
                  <Image source={require("../../assets/user.png")} />
                </View>
                <TextInput
                  fontSize={14}
                  style={styles.TextInput}
                  placeholder="Email"
                  placeholderTextColor="gray"
                  autoCorrect={false}
                  autoCapitalize="none"
                  selectionColor="gray"
                  keyboardType="email-address"
                  underlineColorAndroid="transparent"
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                />
              </View>

              <View
                style={{ flexDirection: "row", marginTop: 10, width: "100%" }}
              >
                <View
                  style={{
                    width: 40,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FFFFFF"
                  }}
                >
                  <Image source={require("../../assets/lock.png")} />
                </View>
                <TextInput
                  fontSize={14}
                  style={styles.TextInput}
                  placeholder="password"
                  placeholderTextColor="gray"
                  autoCorrect={false}
                  autoCapitalize="none"
                  selectionColor="gray"
                  secureTextEntry
                  underlineColorAndroid="transparent"
                  onChangeText={password => this.setState({ password })}
                  value={this.state.password}
                />
              </View>

              <View
                style={{ flexDirection: "row", marginTop: 10, width: "100%" }}
              >
                <View
                  style={{
                    width: 40,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#FFFFFF"
                  }}
                >
                  <Image source={require("../../assets/lock.png")} />
                </View>
                <TextInput
                  fontSize={14}
                  style={styles.TextInput}
                  placeholder="Confirm Password"
                  placeholderTextColor="gray"
                  autoCorrect={false}
                  autoCapitalize="none"
                  selectionColor="gray"
                  secureTextEntry
                  underlineColorAndroid="transparent"
                  onChangeText={cPassword => this.setState({ cPassword })}
                  value={this.state.cPassword}
                />
              </View>

              <TouchableOpacity
                style={[styles.ButtonView, { opacity: isEnabled ? 0.5 : 1.0 }]}
                disabled={isEnabled}
                onPress={() => this.onResetPassword()}
              >
                <Text style={{ color: "#FFFFFF", fontSize: 22 }}>
                  Reset Password
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const { height } = Dimensions.get("window");

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    height: "100%",
    width: "100%"
  },
  ButtonView: {
    backgroundColor: "#e55595",
    height: height * 0.07,
    borderRadius: 2,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10
  },
  TextInput: {
    flex: 1,
    height: height * 0.07,
    backgroundColor: "#FFFFFF",
    color: "gray"
  },
  bgWrap: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    width: "100%",
    height: "100%"
  },
  bgPhoto: {
    alignSelf: "center",
    flex: 1,
    resizeMode: "cover",
    bottom: 0
  },
  container: {
    flex: 1,
    padding: 20,
    alignItems: "center",
    justifyContent: "flex-end"
  }
});
