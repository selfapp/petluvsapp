import React from "react";
import {connect} from "react-redux";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Modal,
  TouchableOpacity, AsyncStorage
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import InstagramLogin from "react-native-instagram-login";


import {facebookLogin, gmailLogin} from "../../store/actions/index";
import Spinner from "../../components/UI/activityIndicator";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };

  constructor(props) {
    super(props);
    this.state = {
      confirmModelVisible: false
    }
  }

//   componentWillReceiveProps(nextProps) {
//     this.onAuthComplete(nextProps);
//   }

//  async onAuthComplete(props) {
//     try {
//       if(props.token) {
//         if (props.confirmed) {
//             this.props.navigation.navigate("OnBoarding1");
//         } else {
//           await AsyncStorage.clear()
//           this.setState({confirmModelVisible: true});
//         }
//       }else if (props.signedIn) {
//         this.props.navigation.navigate("OnBoarding1");
//       }

//     } catch (e) {
//       // console.log(e);
//     }
//   }

  gmailLoginHandler = async () => {
    let res = await this.props.gmailLogin();

    if (res['success'] === true && this.props.token) {
      this.props.navigation.navigate("AuthLoading");
    }else {
      alert("Could not log on! Please try again.");
    }
  };

  facebookLoginHandler = async () => {
    let res = await this.props.facebookLogin();

    if (res['success'] == true && this.props.token) {
      this.props.navigation.navigate("AuthLoading");
    } else {
      alert("Could not log on! Please try again.")
    }
  };

  instagramLoginHandler = () => {
    this.props.navigation.navigate("AuthLoading");
  };

  render() {
    if (this.props.loading === true) {
      return <Spinner/>;
    }
    return (
      <View style={styles.wrap}>
        <View style={styles.bgWrap}>
          <Image
            source={require("../../assets/login.png")}
            style={styles.bgPhoto}
          />
        </View>

        <KeyboardAwareScrollView
          ref="scroll"
          extraScrollHeight={10}
          keyboardOpeningTime={0}
          extraHeight={50}
          keyboardShouldPersistTaps="handled"
          enableOnAndroid
          enableResetScrollToCoords
        >
        <View style={styles.container}>

          <Image
            source={require("../../assets/loginLogo.png")}
            style={styles.loginLogo}
          />

          <View style={{ marginTop: 10,flex:1, alignItems:'center', width:'100%' }}>
            <View style={{ marginTop: 350 }}>
            <Text style={styles.loginWithText}>login with</Text>
            </View>

          <TouchableHighlight
            style={styles.loginButton}
            onPress={() => this.facebookLoginHandler()}
          >
            <View>
              <Icon
                style={styles.loginButtonIcon}
                name="facebook-f"
                size={24}
                color="#4867AA"
              />
              <Text style={[styles.loginButtonText, styles.facebookText]}>
                Facebook
              </Text>
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.loginButton}
            onPress={() => this.gmailLoginHandler()}
          >
            <View>
              <Icon
                style={styles.loginButtonIcon}
                name="google"
                size={24}
                color="#fb3958"
              />
              <Text style={[styles.loginButtonText, styles.twitterText]}>
                Gmail
              </Text>
            </View>
          </TouchableHighlight>

          <InstagramLogin
            ref="instagramLogin"
            redirectUrl="http://www.anyalpha.com"
            clientId="dbc0926e30814b629a9e954d17278c21"
            scopes={["public_content+follower_list"]}
            onLoginSuccess={token => this.instagramLoginHandler(token)}
          />

          <Text style={[styles.loginButtonText, styles.socialText]}>
            <Text onPress={() => this.props.navigation.navigate("SignIn")}>
              Sign in{" "}
            </Text>
            |{" "}
            <Text onPress={() => this.props.navigation.navigate("Register")}>
              Create Account
            </Text>
            .
          </Text>
        </View>

        {/*Confirmation mail Popup*/}
        {/* <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.confirmModelVisible}
        >
          <View style={{backgroundColor: '#00000030', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <View style={{backgroundColor: 'white', borderRadius: 10}}>

              <View style={{alignItems: 'center', justifyContent: 'space-between', margin: 30}}>

                <Image
                  style={{resizeMode: "contain", height: 30, marginBottom: 10}}
                  source={require("./../../assets/petluvs.png")}
                />

                <Text
                  style={{textAlign: 'center'}}>{'An email has been sent to verify user credentials.\n Please check your email and\n' +
                'confirm'}</Text>

                <TouchableOpacity style={{
                  height: 50, width: '30%',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 10,
                  backgroundColor: "#e55595",
                  margin: 10, marginTop: 50
                }}
                                  onPress={() => {
                                    this.setState({confirmModelVisible: false})
                                  }}
                >
                  <Text style={{color: '#fff'}}>OK</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

        </Modal> */}

        </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  token: state.auth.token,
  confirmed: state.auth.confirmed,
  loading: state.auth.loading || state.user.loading,
  user: state.user.user
});

export default connect(
  mapStateToProps,
  {facebookLogin, gmailLogin}
)(LoginScreen);

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    height: "100%",
    width: "100%"
  },
  bgWrap: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    width: "100%",
    height: "100%"
  },
  bgPhoto: {
    alignSelf: "center",
    flex: 1,
    resizeMode: "cover",
    bottom: 0
  },
  container: {
    flex: 1,
    padding: 20,
    alignItems: "center",
    justifyContent: "flex-end",
  },
  loginLogo: {
    resizeMode: "contain",
    position: "absolute",
    top: "10%"
  },
  loginButton: {
    display: "flex",
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    width: "100%",
    paddingTop: 15,
    paddingBottom: 15,
    alignItems: "stretch",
    justifyContent: "center",
    marginBottom: 16,
  },
  loginButtonText: {
    fontSize: 18,
    textAlign: "center"
  },
  loginWithText: {
    fontSize: 14,
    textAlign: "center",
    color: "#8f858c",
    marginBottom: 10
  },
  otherSocialNetText: {
    fontSize: 16,
    textAlign: "center"
  },
  socialText: {
    color: "#e55595"
  },
  twitterText: {
    color: "#1DA1F2"
  },
  facebookText: {
    color: "#4867AA"
  },
  loginButtonIcon: {
    position: "absolute",
    left: 20,
    bottom: 0
  }
});
