import React from "react";
import { connect } from "react-redux";
import moment from 'moment';
import * as Font from 'expo-font';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  View,
  Image
} from "react-native";
import Constants from 'expo-constants';
import { AppLoading, Asset } from "expo";

import { autoLogin } from "../store/actions";
import saveLocation from "../services/saveLocation";
import registerNotifications from "../services/push_notification";


class AuthLoadingScreen extends React.Component {
  state = {
    isLoadingComplete: false
  };

  cacheImages(images) {
    return images.map(image => {
      if (typeof image === "string") {
        return Image.prefetch(image);
      }
      return Asset.fromModule(image).downloadAsync();
    });
  }

  loadResourcesAsync = async () => {
    Font.loadAsync({
      FontAwesome: require('../assets/fonts/FontAwesome.ttf'),
      Feather: require('../assets/fonts/Feather.ttf'),
      "Material Icons": require('../assets/fonts/MaterialIcons.ttf'),
      "MaterialIcons": require('../assets/fonts/MaterialIcons.ttf'),
    })

    const imageAssets = this.cacheImages([
      "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
      "https://placeimg.com/640/640/nature",
      "https://placeimg.com/640/640/people",
      "https://placeimg.com/640/640/animals",
      "https://placeimg.com/640/640/beer",
      require("../assets/slider2.png"),
      require("../assets/slider3.png"),
      require("../assets/slider1.png"),
      require("../assets/LocationPhoto1.png"),
      require("../assets/login.png"),
      require("../assets/loginLogo.png"),
      require("../assets/createPlaydateButton.png"),
      require("../assets/createBreederEventButton.png"),
      require("../assets/addPetIcon.png"),
      require("../assets/backIcon.png"),
      require("../assets/notificationIcon.png"),
      require("../assets/cross.png"),
      require("../assets/edit.png"),
      require("../assets/PetImage.png")
    ]);
    await Promise.all([...imageAssets]);
  };

  handleLoadingError = error => {
    // console.log("loading error" , error)
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
  };

  handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
    this.bootstrapAsync();
  };

  // Fetch the token from storage then navigate to our appropriate place
  bootstrapAsync = async () => {
    // AsyncStorage.removeItem("token");
    const userToken = await AsyncStorage.getItem("token");
    const HAS_APP_INSTALLED = await AsyncStorage.getItem("HAS_APP_INSTALLED");

    if (userToken) {
      let autoLogin = await this.props.autoLogin(userToken);
      // console.log("autoLogin", autoLogin);

      const location = await saveLocation(userToken);
      await AsyncStorage.getItem("@petLuvsStore:isBreeder");

      if (Constants.isDevice) {
        const pushToken = await registerNotifications(userToken);
        if (pushToken.message === "success") {
          await AsyncStorage.setItem("pushToken", pushToken.pushToken);
        }
      }
    }

    console.log("HAS_APP_INSTALLED", HAS_APP_INSTALLED);
    if (HAS_APP_INSTALLED == null) {
      await AsyncStorage.setItem('HAS_APP_INSTALLED', 'true');
    }

    if (!userToken) {
      await AsyncStorage.clear();
    }

    let NAVIGATION_ROUTE = 'OnBoarding';
    if (HAS_APP_INSTALLED) {
      NAVIGATION_ROUTE = 'App';
    }

    userToken
      ? this.props.navigation.navigate(NAVIGATION_ROUTE)
      : this.props.navigation.navigate("Auth");
  };

  // Render any loading content that you like here
  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync}
          onError={this.handleLoadingError}
          onFinish={this.handleFinishLoading}
        />
      );
    }
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#0000ff" />

        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "pink"
  }
});

export default connect(
  null,
  { autoLogin }
)(AuthLoadingScreen);
