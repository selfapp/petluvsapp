import React from "react";
import {
  Dimensions,
  FlatList,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  AsyncStorage,
  Image,
  Platform
} from "react-native";
import {Location, Permissions} from "expo";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import ModalDropdown from "react-native-modal-dropdown";
import {NavigationEvents} from "react-navigation";
import connect from "react-redux/es/connect/connect";

import SearchListing from "../components/SearchListing";
import {baseurl} from "../BaseUrl";
import {
  searchService,
  removeHeaderTitle
} from "../store/actions";

const {height, width} = Dimensions.get("window");

class ServicesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: null,
      errorMessage: null,
      data: [],
      presentableData: [],
      searchText: "",
      searchkey: "",
      params: null,
      searchModal: false,
      breeds: [],
      comments: "",
      multiSpecies: [],
      category: "",
      serviceTiming: "",
      refreshing: false,
      dogBreeds: []
    };
  }

  static navigationOptions = {
    title: "Services"
  };

  categories = [
    {
      title: "Pet Sitter",
      key: "petSitter",
      icon: require("../assets/marker_foster.png")
    },
    {
      title: "Pet Breeder",
      key: "breeder",
      icon: require("../assets/marker_foster.png")
    },
    {
      title: "Pet Groomers",
      key: "petGroomer",
      icon: require("../assets/marker_groomer.png")
    },
    {
      title: "Pet Trainer",
      key: "petTrainner",
      icon: require("../assets/marker_trainer.png")
    },
    {
      title: "Dog Walker",
      key: "dogWalker",
      icon: require("../assets/marker_trainer.png")
    }
  ];

  componentDidMount() {
    this.loadData();
  }

  setDogBreeds() {
    this.props.speciesAndBreeds.forEach((species) => {
      if (species.name === 'Dog') {
        this.setState({
          dogBreeds: species.children
        });
      }
    });
  }

  loadData = async () => {
    this.getLocationAsync();
    this.setDogBreeds();

    await navigator.geolocation.getCurrentPosition(
      async position => {
        await this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });

        // this.onCategoryPress("Pet Sitter", "petSitter");
      },
      error => this.setState({error: error.message}),
      {enableHighAccuracy: true, timeout: 2000, maximumAge: 2000}
    );
  }

  getLocationAsync = async () => {
    const {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    }

    const location = await Location.getCurrentPositionAsync({});
    await this.setState({location});
    const {latitude, longitude} = this.state.location.coords;

    const params = {
      ll: `${latitude},${longitude}`
      // query: this.state.searchText
    };
    await this.setState({
      ...this.state,
      params
    });
  };

  onCategoryPress = async (title, key) => {
    // this.getLocationAsync();
    await this.setState({category: title, searchKey: key});
    this.fetchServiceData();
  };

  handleRefresh = () => {
    this.setState({refreshing: true});
    this.fetchServiceData();
  };

  searchModal = () => (
    <Modal
      animationType="slide"
      transparent
      visible={this.state.searchModal}
      onDismiss={() => this.setState({breeds: []})}
    >
      <View
        style={{
          backgroundColor: "#00000030",
          alignItems: "center",
          justifyContent: "center",
          flex: 1
        }}
      >
        <View
          style={{
            height: height - (Platform.OS === "ios" ? 300 : 200),
            width: width - 40,
            backgroundColor: "#fff",
            borderRadius: 10
          }}
        >
          <View
            style={{
              margin: 5,
              alignItems: "center",
              justifyContent: "center",
              marginVertical: 15,
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.2,
              shadowRadius: 5,
              elevation: 5,
              shadowColor: "#000"
            }}
          >
            <Image
              style={{resizeMode: "contain", height: 30, marginBottom: 10}}
              source={require("./../assets/petluvs.png")}
            />
            <Text style={{fontSize: 17}}>
              I Am Looking For A {this.state.category}
            </Text>
          </View>
          <ScrollView
            contentContainerStyle={{
              width: width - 40,

              marginBottom: 0,
              paddingHorizontal: 10
            }}
          >
            <View>
              <Text
                style={[
                  styles.textLabel,
                  {
                    marginBottom: 5,
                    fontSize: 15
                  }
                ]}
              >
                {this.state.category === "Pet Breeder"
                  ? "Type of Breed(s) I am Looking For"
                  : null}
                {this.state.category === "Pet Sitter" ? "My pet is a" : null}
                {this.state.category === "Pet Daycare" ? "My pet is a" : null}

                {this.state.category === "Pet Friendly Restaurant"
                  ? "Type of Breed(s) I am Looking For"
                  : null}
                {this.state.category === "Dog walker" ? "My dog is a " : null}
                {this.state.category === "Dog parks"
                  ? "Type of Breed(s) I am Looking For"
                  : null}
                {this.state.category === "Pet Trainer" ? "My pet is a" : null}
                {this.state.category === "Pet Groomers"
                  ? "My pet is a"
                  : null}
              </Text>
              {
                this.state.searchKey === "breeder"?
              <SectionedMultiSelect
                items={this.props.speciesAndBreeds}
                uniqueKey="id"
                subKey={"children"}
                showCancelButton
                modalWithSafeAreaView
                modalWithTouchable
                styles={{button: {backgroundColor: "#e55595"}}}
                selectText="Please choose from the list"
                showDropDowns
                readOnlyHeadings
                onSelectedItemsChange={selectedItem =>
                  this.setState({breeds: selectedItem})
                }
                selectedItems={this.state.breeds}
              />
              : <SectionedMultiSelect
                items={this.state.dogBreeds}
                uniqueKey="id"
                showCancelButton
                modalWithSafeAreaView
                modalWithTouchable
                styles={{button: {backgroundColor: "#e55595"}}}
                selectText="Please choose from the list"
                onSelectedItemsChange={selectedItem =>
                  this.setState({breeds: selectedItem})
                }
                selectedItems={this.state.breeds}
              />
              }
            </View>
            {this.state.category === "Pet Sitter" ? (
              <View>
                <Text
                  style={[
                    styles.textLabel,
                    {
                      marginBottom: 5,
                      fontSize: 15
                    }
                  ]}
                >
                  Time
                </Text>
                <View style={styles.ViewShadow}>
                  <ModalDropdown
                    options={["Mins", "Hours"]}
                    onSelect={val => {
                      this.setState({serviceTiming: val});
                    }}
                    style={{width: 200}}
                    dropdownStyle={{
                      width: 200,
                      height: 90,
                      borderWidth: 2,
                      borderTopWidth: 0,
                      borderRadius: 5
                    }}
                  />
                </View>
                <Text
                  style={[
                    styles.textLabel,
                    {
                      marginTop: 10,
                      marginBottom: 5,
                      fontSize: 15
                    }
                  ]}
                >
                  Duration
                </Text>
                <View
                  style={{
                    height: 40,
                    marginHorizontal: 15,
                    marginTop: 5,
                    borderWidth: 2,
                    borderColor: "#fff",
                    backgroundColor: "#fff",
                    shadowOffset: {width: 1, height: 1},
                    elevation: 5,
                    shadowColor: "#e55595",
                    shadowOpacity: 0.5,
                    justifyContent: "space-between",
                    paddingLeft: 5,
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      alignItems: "flex-start",
                      justifyContent: "center"
                    }}
                  >
                    <View>
                      <View style={styles.radioButton1}>
                        <TextInput
                          keyboardType={"phone-pad"}
                          style={{
                            fontSize: 12,
                            paddingLeft: 5
                          }}
                          value={this.state.sittingTiming}
                          onChangeText={value => {
                            this.setState({
                              sittingTiming: value
                            });
                          }}
                          placeholder={"Please fill in the details here"}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.category === "Dog walker" ? (
              <View>
                <Text
                  style={[
                    styles.textLabel,
                    {
                      marginBottom: 5,
                      fontSize: 15
                    }
                  ]}
                >
                  Time
                </Text>
                <View style={styles.ViewShadow}>
                  <ModalDropdown
                    options={["Mins", "Hours"]}
                    onSelect={val => {
                      this.setState({serviceTiming: val});
                    }}
                    style={{width: 200}}
                    dropdownStyle={{
                      width: 200,
                      height: 90,
                      borderWidth: 2,
                      borderTopWidth: 0,
                      borderRadius: 5
                    }}
                  />
                </View>
                <Text
                  style={[
                    styles.textLabel,
                    {
                      marginTop: 10,
                      marginBottom: 5,
                      fontSize: 15
                    }
                  ]}
                >
                  Duration
                </Text>
                <View
                  style={{
                    height: 40,
                    marginHorizontal: 15,
                    marginTop: 5,
                    borderWidth: 2,
                    borderColor: "#fff",
                    backgroundColor: "#fff",
                    shadowOffset: {width: 1, height: 1},
                    elevation: 5,
                    shadowColor: "#e55595",
                    shadowOpacity: 0.5,
                    justifyContent: "space-between",
                    paddingLeft: 5,
                    flexDirection: "row"
                  }}
                >
                  <View
                    style={{
                      width: "100%",
                      alignItems: "flex-start",
                      justifyContent: "center"
                    }}
                  >
                    <View>
                      <View style={styles.radioButton1}>
                        <TextInput
                          keyboardType={"phone-pad"}
                          style={{
                            fontSize: 12,
                            paddingLeft: 5
                          }}
                          value={this.state.comments}
                          onChangeText={value => {
                            this.setState({
                              comments: value
                            });
                          }}
                          placeholder={"Please fill in the details here"}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            ) : null}
            {this.state.category === "Pet Groomers" ? (
              <View>
                <Text
                  style={[
                    styles.textLabel,
                    {
                      marginBottom: 5,
                      fontSize: 15
                    }
                  ]}
                >
                  Type of Grooming Service
                </Text>
                <View style={styles.ViewShadow}>
                  <ModalDropdown
                    options={[
                      "Hair Cut",
                      "Bath",
                      "Pedicure",
                      "All of the above"
                    ]}
                    onSelect={val => {
                      this.setState({serviceTiming: val});
                    }}
                    style={{width: 200}}
                    dropdownStyle={{
                      width: 200,
                      height: 100,
                      borderWidth: 2,
                      borderTopWidth: 0,
                      borderRadius: 5
                    }}
                  />
                </View>
              </View>
            ) : null}
            <Text
              style={[
                styles.textLabel,
                {
                  marginTop: 10,
                  marginBottom: 5,
                  fontSize: 15
                }
              ]}
            >
              Comments
            </Text>
            <View
              style={{
                height: 40,
                marginHorizontal: 15,
                marginTop: 5,
                marginBottom:10,
                borderWidth: 2,
                borderColor: "#fff",
                backgroundColor: "#fff",
                shadowOffset: {width: 1, height: 1},
                elevation: 5,
                shadowColor: "#e55595",
                shadowOpacity: 0.5,
                justifyContent: "space-between",
                paddingLeft: 5,
                flexDirection: "row"
              }}
            >
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <View>
                  <View style={styles.radioButton1}>
                    <TextInput
                      style={{
                        fontSize: 12,
                        paddingLeft: 5
                      }}
                      value={this.state.comments}
                      onChangeText={value => {
                        this.setState({
                          comments: value
                        });
                      }}
                      placeholder={"Please fill in the details here"}
                    />
                  </View>
                </View>
              </View>
            </View>
          </ScrollView>
          <View style={{flexDirection: "row"}}>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton]}
                onPress={async () => {
                  const token = await AsyncStorage.getItem("token");
                  let data = {};
                  if (this.state.category === "Dog walker") {
                    data = {
                      category: "walker",
                      durationtype: this.state.serviceTiming,
                      duration: this.state.serviceTiming,
                      comments: this.state.comments,
                      userId: token
                    };
                  }
                  if (this.state.category === "Pet Groomers") {
                    data = {
                      category: "groomer",
                      breed: this.state.breeds,
                      service: this.state.serviceTiming,
                      comments: this.state.comments,
                      userId: token
                    };
                  }
                  if (this.state.category === "Pet Sitter") {
                    data = {
                      category: "sitter",
                      breed: this.state.breeds,
                      durationtype: this.state.serviceTiming,
                      duration: this.state.sittingTiming,
                      comments: this.state.comments,
                      userId: token
                    };
                  }
                  if (this.state.category === "Breeder") {
                    data = {
                      category: "breeder",
                      breed: this.state.breeds,
                      service: "breeding",
                      comments: this.state.comments,
                      userId: token
                    };
                  }
                  if (this.state.category === "Pet Trainer") {
                    data = {
                      category: "trainer",
                      breed: this.state.breeds,
                      service: "training",
                      comments: this.state.comments,
                      userId: token
                    };
                  }

                  this.props.searchService(data);
                  this.setState({
                    searchModal: !this.state.searchModal
                  });
                }}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Search</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={[
                styles.saveButtonContainer,
                {height: 60, flex: 1, marginHorizontal: 15, paddingTop: 10}
              ]}
            >
              <TouchableOpacity
                style={[styles.saveButton, {backgroundColor: "gray"}]}
                onPress={() => {
                  this.setState({
                    searchModal: !this.state.searchModal
                  });
                }}
              >
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text style={{color: "#fff", fontSize: 16}}>Cancel</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );

  makeArrayOfSpeciesAndBreeds() {
    const result = [];
    this.props.speciesAndBreeds.map(item => {
      const species = {
        name: item.speciesName,
        id: item.speciesName,
        children: []
      };

      item.breeds.map(breed => {
        species.children.push({
          name: breed,
          id: breed
        });
      });

      result.push(JSON.parse(JSON.stringify(species)));
    });

    this.setState({multiSpecies: result});
  }

  fetchServiceData = async () => {
    await this.setState({
      ...this.state,
      data: [],
      presentableData: []
    });

    // let's get the data from backend. latitude, langutide, postData, and title should be dynamic//////////////
    const userToken = await AsyncStorage.getItem("token");
    const latitude = this.state.location.coords.latitude;
    const longitude = this.state.location.coords.longitude;
    const postData = {
      comment: "A test comment",
      speciesList: ["One", "Two"]
    };
    const searchText = this.state.category;
    const searchKey = this.state.searchKey;

    try {
      const response = await fetch(
        `${baseurl}/places/services-suggestions-list?query=${searchText}&searchKey=${searchKey}&lat=${latitude}&lng=${longitude}`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${userToken}`
          },
          body: JSON.stringify(postData)
        }
      );

      const responseJson = await response.json();

      const data = responseJson.response.data;
      const dataMod = data.map(item => ({
        name: item.name,
        Type: item.serviceType,
        Distance: item.distance,
        ClosingTime: item.isOpen,
        inAppServiceProvider: item.inAppServiceProvider ? true : false,
        Rating: item.rating,
        placeId: item.place_id,
        serviceProviderId: item.serviceProviderId
      }));

      await this.setState({presentableData: dataMod});
    } catch (e) {
      // console.log(`Error Fetching service list data ${e}`);
    }
  };

  render() {
    return (
      <View style={{paddingTop: 0, flex: 1}}>
        <NavigationEvents
          onWillFocus={payload => this.props.removeHeaderTitle()}
        />
        <View
          style={{
            zIndex: 99,
            width: "100%",
            marginVertical: 10,
            paddingHorizontal: 10
          }}
        >
          <FlatList
            horizontal
            data={this.categories}
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableHighlight
                onPress={() => {
                  this.setState({
                    searchModal: !this.state.searchModal
                  });
                  this.onCategoryPress(item.title, item.key);
                }}
                style={[
                  styles.CategoryButton,
                  {opacity: this.state.category === item.title ? 0.9 : 0.5}
                ]}
              >
                <Text style={{color: "white", fontSize: 15}}>
                  {item.title}
                </Text>
              </TouchableHighlight>
            )}
            keyExtractor={(item, index) => index.toString()}
            extraData={this.state.category}
          />
        </View>
        <SearchListing
          data={this.state.presentableData}
          searchKey={this.state.searchKey}
          refreshing={this.state.refreshing}
          handleRefresh={this.handleRefresh}
        />
        {this.searchModal()}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.user.loading || state.pets.loading,
  user: state.user.user,
  pets: state.pets.pets,
  speciesAndBreeds: state.playDates.speciesAndBreeds
});

const styles = StyleSheet.create({
  CategoryButton: {
    backgroundColor: "#EF5595",
    paddingVertical: 6,
    paddingHorizontal: 16,
    marginRight: 12,
    borderRadius: 4
  },
  ViewShadow: {
    marginHorizontal: 15,
    shadowRadius: 5,
    alignItems:'center',
    height: 40,
    marginTop: 5,
    borderWidth: 2,
    borderColor: "#fff",
    backgroundColor: "#fff",
    shadowOffset: {width: 1, height: 1},
    elevation: 5,
    shadowColor: "#e55595",
    shadowOpacity: 0.5,
    justifyContent: "space-between",
    paddingLeft: 5,
    flexDirection: "row"
  },
  saveButton: {
    height: 50,
    width: "100%",
    borderRadius: 10,
    backgroundColor: "#e55595",
    margin: 20
  },
  saveButtonContainer: {
    marginHorizontal: 15,
    alignItems: "center",
    justifyContent: "center",
    height: "10%",
    marginBottom: 10,
    marginTop: 10
  }
});

export default connect(
  mapStateToProps,
  {
    searchService,
    removeHeaderTitle
  }
)(ServicesScreen);
