/* eslint-disable prettier/prettier */
import React, {Component} from "react";
import {connect} from "react-redux";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ImageBackground,
  AsyncStorage,
  Dimensions,
  TouchableWithoutFeedback,
  ScrollView,
  SafeAreaView,
  Platform,
  InteractionManager,
  Modal,
  FlatList
} from "react-native";
import ImageSlider from "react-native-image-slider";
import StarRating from "react-native-star-rating";
import MaterialTabs from "react-native-material-tabs";
import {NavigationEvents} from "react-navigation";

import {
  fetchAllPets,
  fetchPlaydate,
  joinPlaydate,
  leavePlaydate,
  reviewPlaydate,
  fetchReviewPlaydate,
  acceptReviewPlaydate,
  declineReviewPlaydate,
  fetchUserReviews,
  shareReview,
  homeData,
  enableBackButton,
  disableBackButton
} from "../../store/actions";
import {dummyPets} from "../../Data/pets";
import Spinner from "../../components/UI/activityIndicator";
import GeneralStyles from "../../Styles/General";
import ReviewItem from "../../components/ReviewItem";
import {playdateDurationCalc} from "../../utility/playdateDurationCal";
import {PETLUVS_DEFAULT_IMAGE} from "../../utility/utils";

const width1 = Dimensions.get("window").width;
const {height, width} = Dimensions.get("window");
const height1 = (((height / 2 / height) * 100 + 3) / 100) * height;
let isUserJoined = false;
let isJoinShow = true;

class PlaydateDetails extends Component {
  static getDerivedStateFromProps(props, state) {
    if (props.playdate) {
      let hasUserJoined = state.hasUserJoined;
      let distance = state.distance;
      let type = state.type;

      if (props.playdate.userJoining) {
        hasUserJoined = props.playdate.userJoining.some(
          pd => pd === props.authInfo.id
        );
      }
      const playdatePets = props.playdate.pets;

      if (props.playdate && props.playdate.location)
        distance = props.playdate.location.distance;

      if (props.playdate && props.playdate.location)
        type = "";

      return {
        playateUserId: props.playdate.userId,
        isSelfPlaydate: props.authInfo.id === props.playdate.userId,
        hasUserJoined,
        playdatePets,
        distance,
        type
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      playdateId: props.navigation.getParam("playdateId"),
      playdateOwnerId: props.navigation.getParam("playdateOwnerId"),
      hasUserJoined: false,
      playateUserId: null,
      isSelfPlaydate: false,
      distance: "1.2 km",
      closingTime: "8 pm",
      starCount: 3,
      selectedTab: 0,
      showPetsModal: false,
      showPetsModalJoin: false,
      pets: [],
      playdatePets: [],
      playdateHost: [],
      joinOrLeave: "",
      showEditReview: false,
      showOptionsModal: false,
      showShareModal: false,
      review: "",
      type: ""
    };
  }

  async componentDidMount() {
    this.props.fetchPlaydate(this.state.playdateId);
    await this.props.fetchReviewPlaydate(this.state.playdateId);
    this.props.fetchAllPets();
    // this.props.fetchUserReviews();
    this.props.fetchUserReviews(this.state.playdateOwnerId);
    this.getItem();
    await this.setState({playdatePets: this.props.playdate.pets});
    InteractionManager.runAfterInteractions(() => {
      if (this.props.navigation.getParam("fromNotification")) {
        if(this.props.navigation.getParam("reviewThePlaydate")){
          this.setState({selectedTab: 1, showOptionsModal: true});
        }else{
          this.setState({selectedTab: 1, showOptionsModal: false});
        }
      }
    });
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  setTab = tab => {
    this.setState({selectedTab: tab});
  };

  timeLeftLatest = (startTime, endTime) => {
    const timeLeft = playdateDurationCalc(startTime, endTime);
    return timeLeft;
  }

  petSelectionHnadler = petId => {
    const pet = this.state.pets.some(id => id === petId);
    this.setState(state => {
      let pets;
      if (pet) {
        pets = state.pets.filter(id => id !== petId);
      }

      if (!pet) {
        pets = state.pets.concat(petId);
      }

      return {
        pets
      };
    });
  };

  bringPets = pets => (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      horizontal
      style={{width: "100%", marginTop: 20}}
    >
      {pets.map((item, key) => (
        <View key={key}>
          <TouchableOpacity onPress={() => this.petSelectionHnadler(item._id)}>
            <View style={styles.bringPetsSrollview}>
              <Image
                source={{
                  uri:
                    item.imagePath && item.imagePath[0] && item.imagePath[0].url !== null
                      ? item.imagePath[0].url
                      : "https://s3.amazonaws.com/petluvs/Leo.jpg"
                }}
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 70 / 2,
                  opacity: this.state.pets.some(id => id === item._id) ? 1 : 0.5,
                  marginBottom: 5
                }}
              />
              <Text
                style={{color: item.color, fontSize: 12, alignSelf: "center"}}
              >
                {item.name}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      ))}
    </ScrollView>
  );

  getItem = async () => {
    try {
      const myname = await AsyncStorage.getItem("name");
      const profileImage = await AsyncStorage.getItem("picture");
      this.setState({name: myname});
      this.setState({picture: profileImage});
    } catch (error) {
      // console.log("Error", error);
    }
  };

  onJoinPlaydate = async playdateId => {
    await this.props.joinPlaydate(
      this.props.navigation.state.params.playdateId,
      this.state.pets
    );
    await this.props.fetchPlaydate(this.state.playdateId);

  };

  onLeavePlaydate = async playdateId => {
    isUserJoined = false;
    await this.props.leavePlaydate(
      this.props.navigation.state.params.playdateId
    );
    await this.props.fetchPlaydate(this.state.playdateId);
    await this.setState({pets: []});
  };

  userJoinDisplayHandler = () => {
    this.setState({joinOrLeave: "Leave"});
    this.onJoinPlaydate(this.props.playdateId);
  };

  userLeaveDisplayHandler = () => {
    this.setState({joinOrLeave: "Join"});
    this.onLeavePlaydate(this.props.playdateId);
  };

  playDateJoinOrLeaveHandler = isUserJoined => {
    if (this.state.hasUserJoined) {
      return this.userLeaveDisplayHandler();
    }
    return this.userJoinDisplayHandler();
  };

  showHideModal = () => {
    this.setState({showPetsModal: !this.state.showPetsModal});
  };

  showHideModalJoin = () => {
    this.setState({showPetsModalJoin: !this.state.showPetsModalJoin});
  };

  onSubmitReview = () => {
    const data = {
      starCount: this.state.starCount,
      description: this.state.review,
      reviewFor: "Playdate",
      dataId: this.state.playdateId,
      receiverId: this.props.playdate.userId
    };

    this.setState({
      showOptionsModal: !this.state.showOptionsModal,
      review: ""
    });
    this.props.reviewPlaydate(
      data,
      this.state.playdateId,
    );
    this.props.fetchReviewPlaydate(this.state.playdateId);
    this.props.fetchUserReviews(this.state.playdateOwnerId);
  }

  JoinOrLeaveModal = pets => (
    <Modal animationType="fade" transparent visible={this.state.showPetsModal}>
      <TouchableWithoutFeedback
        onPress={() => {
          this.showHideModal();
        }}
      >
        <View
          style={{
            backgroundColor: "#00000030",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        >
          <TouchableWithoutFeedback>
            <View
              style={
                this.state.hasUserJoined
                  ? {
                    height: 100,
                    width: 0.7 * width,
                    backgroundColor: "#fff",
                    marginTop: 20,
                    borderRadius: 10,
                    alignItems: "center",
                    justifyContent: "center"
                  }
                  : {
                    height: 200,
                    width: 0.7 * width,
                    backgroundColor: "#fff",
                    marginTop: 20,
                    borderRadius: 10,
                    justifyContent: "center"
                  }
              }
            >
              {this.state.hasUserJoined ? (
                <Text style={{marginLeft: 10, marginTop: 30}}>
                  Are you sure you want to leave the playdate
                </Text>
              ) : (
                <Text style={{marginLeft: 10, marginTop: 10}}>
                  Please choose your pets to bring
                </Text>
              )}
              <ScrollView
                style={{
                  margin: 0.5,
                  marginLeft: 5,
                  marginRight: 5,
                  marginBottom: 10
                }}
                contentContainerStyle={{
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                {this.state.hasUserJoined ? null : this.bringPets(pets)}
              </ScrollView>
              <View style={{flexDirection: "row"}}>
                <TouchableOpacity
                  style={{
                    marginTop: 5,
                    width: 100,
                    height: 25,
                    alignItems: "center",
                    justifyContent: "center",
                    marginBottom: 10,
                    marginLeft: 10
                  }}
                  onPress={() => {
                    this.showHideModal();
                  }}
                >
                  <Text style={{color: "#749E9B", fontSize: 15}}>Close</Text>
                </TouchableOpacity>

                <View style={{flex: 1}}>
                  <TouchableOpacity
                    style={{
                      marginTop: 5,
                      width: 100,
                      height: 25,
                      alignSelf: "flex-end",
                      alignItems: "center",
                      justifyContent: "center",
                      marginBottom: 10,
                      marginRight: 10
                    }}
                    onPress={() => {
                      this.showHideModal();
                      isJoinShow
                        ? this.playDateJoinOrLeaveHandler(isUserJoined)
                        : null;
                    }}
                  >
                    <Text style={{color: "#749E9B", fontSize: 15}}>
                      {this.state.hasUserJoined ? "Leave" : "Join"}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );

  JoinPlaydateModal = () => (
    <Modal
      animationType="fade"
      transparent
      visible={this.state.showPetsModalJoin}
    >
      <TouchableWithoutFeedback
        onPress={() => {
          this.showHideModalJoin();
        }}
      >
        <View
          style={{
            backgroundColor: "#00000030",
            alignItems: "center",
            justifyContent: "center",
            flex: 1
          }}
        >
          <TouchableWithoutFeedback>
            <View
              style={{
                height: 200,
                width: 0.7 * width,
                backgroundColor: "#fff",
                marginTop: 20,
                borderRadius: 10,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text style={{fontSize: 20, margin: 10}}>
                Are you sure you want to join?
              </Text>
              <View style={{flexDirection: "row"}}>
                <View style={{flex: 1}}>
                  <TouchableOpacity
                    style={{
                      marginTop: 85,
                      width: 100,
                      height: 25,
                      alignSelf: "flex-end",
                      alignItems: "center",
                      justifyContent: "center",
                      marginBottom: 10,
                      marginRight: 10
                    }}
                    onPress={() => {
                      this.showHideModalJoin();
                      {
                        isJoinShow
                          ? this.playDateJoinOrLeaveHandler(isUserJoined)
                          : null;
                      }
                    }}
                  >
                    <Text style={{color: "#749E9B", fontSize: 15}}>Ok</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );

  tabView = () => {
    if (this.state.selectedTab === 0) {
      let filterdPets = null;
      if (this.state.playdatePets) {
        filterdPets = this.state.playdatePets;
        // filterdPets = this.state.playdatePets.filter(p => p.userId != this.state.playdateOwnerId);
      }
      let HOST_PET = this.props.navigation.state.params.item.record.pets[0];

      return (
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal={false}
          style={{width: "100%"}}
        >
          {this.props.navigation.state.params.fromNotification || (this.props.navigation.state.params.item.record.userId.local.email === undefined) ? null : (
            <View style={styles.playdatehost}>
              
              {
                HOST_PET && HOST_PET.imagePath && HOST_PET.imagePath[0].url && HOST_PET.imagePath[0].url !== ""
                ? <Image source={{ uri: HOST_PET.imagePath[0].url }} style={{width: 30, height: 30, borderRadius: 30 / 2}}/>
                : <Image source={ PETLUVS_DEFAULT_IMAGE } style={{width: 30, height: 30, borderRadius: 30 / 2}}/>
              }
              
              <View style={{alignItems: "center"}}>
                <Text style={{marginLeft: 20, marginTop: 5}}>
                  {this.props.navigation.state.params.item.record.pets[0].name}
                </Text>
                <Text style={{marginLeft: 20, fontSize: 10, color: "grey"}}>
                  Playdate host
                </Text>
              </View>
            </View>
          )}
          <View style={{flexDirection: "row"}}>
            <Text style={{marginLeft: 20, marginTop: 10, color: "grey"}}>
              Pets Attending
            </Text>
          </View>
          {this.state.playdatePets &&
          filterdPets.map((pet, key) => (
            <View key={key}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("PetProfile", {
                    tab: 0,
                    _id: pet._id,
                    static: true
                  })
                }
              >
                <View style={[styles.bringPetsSrollview, {flexDirection: 'row', alignItems: 'center'}]}>
                  {
                    pet.imagePath && pet.imagePath[0].url && pet.imagePath[0].url !== ""
                    ? <Image source={{ uri: pet.imagePath[0].url }} style={{width: 30, height: 30, borderRadius: 30 / 2}}/>
                    : <Image source={ PETLUVS_DEFAULT_IMAGE } style={{width: 30, height: 30, borderRadius: 30 / 2}}/>
                  }

                  <View>
                    <Text
                      style={{
                        color: "gray",
                        fontSize: 10,
                        marginLeft: 10
                      }}
                    >
                      {pet.name}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
      );
    } else if (this.state.selectedTab === 1) {
      return (
        <ScrollView showsVerticalScrollIndicator={false} removeClippedSubviews>
          {!this.state.isSelfPlaydate ? (
            <View
              style={[
                GeneralStyles.Card,
                {
                  marginBottom: 24,
                  padding: 15,
                  marginTop: 10,
                  marginHorizontal: 15,
                  width: width1 - 30,
                  shadowColor: "#BDFFFF"
                }
              ]}
            >
              <TouchableOpacity
                style={{flex: 1}}
                onPress={() => {
                  this.setState({
                    showOptionsModal: !this.state.showOptionsModal
                  });
                }}
              >
                <View style={{alignSelf: "center"}}>
                  <Text style={{fontSize: 20}}>Review Play Date</Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : null}
          <FlatList
            data={this.props.userReviews}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <View
                style={{
                  flex:1,
                  width: "100%",
                  marginBottom: 24,
                  padding: 15,
                  shadowColor: "#BDFFFF",
                  backgroundColor: "#fff"
                }}
              >
                <View style={[GeneralStyles.Card, {paddingBottom:5}]}>
                <ReviewItem
                  key={item._id}
                  id={item._id}
                  Name={item.reviewerId.userName}
                  ProfilePic={item.reviewerId.profilePic ? item.reviewerId.profilePic : "https://s3.amazonaws.com/petluvs/leo.jpg"}
                  BusinessName={item.reviewFor}
                  starCount={item.starCount}
                  ReviewText={item.description}
                  // showverification={item.verified}
                  // accept={this.props.acceptReviewPlaydate}
                  // decline={this.props.declineReviewPlaydate}
                  showverification
                  fetchReviews={() =>
                    this.props.fetchReviewPlaydate(this.state.playdateId)
                  }
                />
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    height: 30,
                    marginRight: 5,
                    // paddingVertical: 10,
                    paddingHorizontal:10,
                    alignItems: "center",
                    justifyContent: "center",
                    backgroundColor: "#e55595",
                    // borderRadius: 10,
                    borderRadius: 5,
                    flexDirection: "row"
                  }}
                  onPress={() =>
                    this.setState({
                      showShareModal: !this.state.showShareModal,
                      shareReviewid: item._id
                    })
                  }
                >
                  <Image
                    source={require("./../../assets/share.png")}
                    style={{tintColor: "#ffffff"}}
                  />
                  <Text
                    style={{
                      color: "#fff",
                      marginLeft: 10
                    }}
                  >
                    Share
                  </Text>
                </TouchableOpacity>
                </View>
              </View>
            )}
          />
        </ScrollView>
      );
    }

    return (
      <View>
        <View
          style={{height: "10%", width: "100%", justifyContent: "flex-end"}}
        >
          <Text style={{fontSize: 20, marginLeft: "5%"}}>
            American Kennel Club Number
          </Text>
        </View>
      </View>
    );
  };

  render() {
    if (this.props.loading) {
      return <Spinner/>;
    }
    try {
      if (this.props.navigation.state.params.userJoining) {
        isUserJoined = this.props.navigation.state.params.userJoining.some(
          el => el === this.props.authInfo.id
        );
      }
      if (this.props.isJoinShow) {
        isJoinShow = false;
      }
    } catch (e) {
    }

    let images = [];

    if (this.props.playdate !== null && this.props.playdate.location && this.props.playdate.location.photos) {
      images = this.props.playdate.location.photos;
    } else {
      images = [
        "https://placeimg.com/640/640/nature",
        "https://placeimg.com/640/640/people"
      ];
    }

    let pets;
    if (this.props.pets) {
      const data = this.props.pets;
      const petMod = data.map(pet => {
        pet.Image = dummyPets[0].Image;
        return pet;
      });
      pets = petMod;
    } else {
      pets = dummyPets;
    }

    return (
      <SafeAreaView style={styles.container}>
        <NavigationEvents
          onWillFocus={() => setTimeout(() => {
            this.props.enableBackButton();
          }, 10)}
          onWillBlur={() => this.props.disableBackButton()}
        />
        <View style={styles.imagesliderContainer}>
          <ImageSlider
            loopBothSides
            images={images}
            customSlide={({index, item, style, width}) => (
              <View key={index} style={[style, styles.customSlide]}>
                <ImageBackground
                  source={{uri: item}}
                  style={styles.customImage}
                />
              </View>
            )}
          />

          {
            (this.timeLeftLatest(this.props.playdate.startTime, this.props.playdate.endTime) === 'Completed' || this.timeLeftLatest(this.props.playdate.startTime, this.props.playdate.endTime) === 'Started') ? (null) : (
              !this.state.isSelfPlaydate ? (
                <View
                  style={{
                    position: "absolute",
                    top: 40,
                    right: 20,
                    flex: 1,
                    alignItems: "flex-end",
                    marginRight: 20,
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      borderRadius: 5,
                      backgroundColor: "#e55595",
                      height: 35,
                      width: 80,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                    onPress={() => this.showHideModal()}
                  >
                    <Text style={{color: "#fff", fontSize: 20}}>
                      {this.state.hasUserJoined ? "Leave" : "Join"}
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  style={{
                    position: "absolute",
                    top: 40,
                    right: 20,
                    flex: 1,
                    alignItems: "flex-end",
                    marginRight: 20,
                    justifyContent: "center"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      borderRadius: 5,
                      backgroundColor: "#e55595",
                      height: 35,
                      width: 80,
                      alignItems: "center",
                      justifyContent: "center"
                    }}
                    onPress={() => this.props.navigation.navigate("PlaydateEdit", {playdateId: this.state.playdateId})}
                  >
                    <Text style={{color: "#fff", fontSize: 20}}>Edit</Text>
                  </TouchableOpacity>
                </View>
              )
            )
          }


          <View style={styles.header}>
            <Text
              style={{
                height: 40,
                fontSize: 18,
                color: "#e55595",
                alignItems: "center"
              }}
            />
          </View>
        </View>

        <View style={styles.content2}>
          <View style={{flex: 3, paddingLeft: 20}}>
            <Text style={{color: "#fff"}}>{this.props.playdate.place}</Text>
            <View style={{flexDirection: "row"}}>
              <Text style={{color: "#fff", fontSize: 10, marginTop: 10}}>
                {this.state.type}
              </Text>
              <Text
                style={{
                  color: "#fff",
                  fontSize: 10,
                  marginTop: 10,
                  marginLeft: 15
                }}
              >
                {this.state.distance}
              </Text>
              <Text
                style={{
                  color: "#fff",
                  fontSize: 10,
                  marginTop: 10,
                  marginLeft: 15
                }}
              >
                Closes {this.state.closingTime}
              </Text>
            </View>
          </View>
          <View style={{flex: 1}}>
            <Text
              style={{
                color: "#fff",
                fontSize: 10,
                marginTop: 10
              }}
            >
              {this.state.starCount}
              .0
            </Text>
            <View style={{marginTop: 10, marginRight: 10}}>
              <StarRating
                disabled
                maxStars={5}
                rating={this.state.starCount}
                selectedStar={rating => this.onStarRatingPress(rating)}
                starSize={15}
                starColor={"#fff"}
                fullStarColor={"white"}
              />
            </View>
          </View>
        </View>
        <View style={styles.Tabs}>
          <MaterialTabs
            items={["Playdate Details", "Review"]}
            selectedIndex={this.state.selectedTab}
            onChange={val => this.setTab(val)}
            barColor="white"
            indicatorColor="#EF5595"
            activeTextColor="#EF5595"
            inactiveTextColor="#33212D"
            style={{zIndex: 0}}
          />
        </View>
        <View style={{flex: 1}}>{this.tabView()}</View>

        {this.JoinOrLeaveModal(
          this.props.pets
        )}
        {this.JoinPlaydateModal(isJoinShow, isUserJoined)}
        {this.state.showOptionsModal === true ? (
          <Modal
            animationType="fade"
            transparent
            visible={this.state.showOptionsModal}
          >
            <TouchableWithoutFeedback
              onPress={() => {
                this.setState({
                  showOptionsModal: !this.state.showOptionsModal
                });
              }}
            >
              <View
                style={{
                  backgroundColor: "#00000030",
                  alignItems: "center",
                  justifyContent: "center",
                  flex: 1
                }}
              >
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.setState({
                      showOptionsModal: !this.state.showOptionsModal
                    });
                  }}
                  style={{
                    position: "absolute",
                    top: 0,
                    right: 0,
                    height: height1,
                    width,
                    backgroundColor: "#00000050",
                    alignItems: "center",
                    justifyContent: "flex-start",
                    flex: 1
                  }}
                >
                  <View
                    style={{
                      height: 250,
                      backgroundColor: "#ffffff",
                      width: width - 100,
                      borderRadius: 15,
                      marginHorizontal: 50,
                      marginTop: 100
                    }}
                  >
                    <View>
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 10,
                          marginRight: 10,
                          marginBottom: 10,
                          marginHorizontal: 25,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <Text
                          style={{
                            fontSize: 15
                          }}
                        >
                          Please rate{" "}
                        </Text>
                        <StarRating
                          disabled={false}
                          maxStars={5}
                          rating={this.state.starCount}
                          selectedStar={rating =>
                            this.onStarRatingPress(rating)
                          }
                          starSize={25}
                          starColor={"#fff"}
                          fullStarColor={"#EF5595"}
                        />
                      </View>

                      <TextInput
                        name={"UserReview"}
                        fontSize={14}
                        style={styles.TextInput}
                        placeholder="Enter your review(optional)"
                        placeholderTextColor="gray"
                        multiline
                        showWarning
                        autoCorrect={false}
                        autoCapitalize="none"
                        selectionColor="gray"
                        value={this.state.review}
                        onChangeText={text => this.setState({review: text})}
                        underlineColorAndroid="transparent"
                      />
                      <TouchableOpacity
                        style={[styles.saveButton, {alignSelf: "center"}]}
                        onPress={() => this.onSubmitReview()}
                      >
                        <Text style={{fontSize: 15, color: "#fff"}}>
                          Submit Review
                        </Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={{marginBottom: 5, marginTop: 10}}
                        onPress={() => {
                          this.setState({
                            showOptionsModal: !this.state.showOptionsModal
                          });
                        }}
                      >
                        <View style={{alignSelf: "center"}}>
                          <Text style={{fontSize: 15, color: "#000"}}>
                            Cancel
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        ) : null}
        {this.state.showShareModal === true ? (
          <Modal
            animationType="fade"
            transparent
            visible={this.state.showShareModal}
          >
            <TouchableWithoutFeedback
              onPress={() => {
                this.setState({
                  showShareModal: !this.state.showShareModal
                });
              }}
            >
              <View
                style={{
                  backgroundColor: "#00000030",
                  // opacity: 0.3,
                  alignItems: "center",
                  justifyContent: "center",
                  flex: 1
                }}
              >
                <TouchableWithoutFeedback
                  // {/*>*/}
                  //s {/*<View*/}
                  onPress={() => {
                    this.setState({
                      showShareModal: !this.state.showShareModal
                    });
                  }}
                  style={{
                    position: "absolute",
                    top: 0,
                    right: 0,
                    height: height1,
                    width,
                    backgroundColor: "#00000050",
                    alignItems: "center",
                    justifyContent: "flex-start",
                    flex: 1
                  }}
                >
                  <View
                    style={{
                      // flex: 1,
                      height: 250,
                      // top: 50,
                      backgroundColor: "#ffffff",
                      width: width - 100,
                      borderRadius: 15,
                      marginHorizontal: 50
                    }}
                  >
                    <View>
                      <View
                        style={[
                          styles.TextInput,
                          {
                            alignItems: "center",
                            justifyContent: "center",
                            height: 120,
                            marginTop: 10
                          }
                        ]}
                      >
                        <Text
                          style={[
                            {
                              fontSize: 15
                            }
                          ]}
                        >
                          Are you sure you want to share this review in Home
                          page.
                        </Text>
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.saveButton,
                          {alignSelf: "center", width: 200}
                        ]}
                        onPress={() => {
                          this.setState({
                            showShareModal: !this.state.showShareModal
                          });
                          this.props.shareReview(this.state.shareReviewid);
                        }}
                      >
                        <Text style={{fontSize: 15, color: "#fff"}}>Yes</Text>
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={{marginBottom: 5, marginTop: 10}}
                        // onPress={()=> {}}
                        onPress={() => {
                          this.setState({
                            showShareModal: !this.state.showShareModal
                          });
                        }}
                      >
                        <View style={{alignSelf: "center"}}>
                          <Text style={{fontSize: 15, color: "#000"}}>
                            Cancel
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        ) : null}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  pets: state.pets.pets,
  authInfo: state.auth.authInfo,
  playdate: state.playDates.playdate,
  reviews: state.playDates.reviews,
  loading: false,
  userReviews: state.playDates.userReviews
});

export default connect(
  mapStateToProps,
  {
    fetchPlaydate,
    fetchAllPets,
    joinPlaydate,
    leavePlaydate,
    reviewPlaydate,
    fetchReviewPlaydate,
    acceptReviewPlaydate,
    declineReviewPlaydate,
    fetchUserReviews,
    shareReview,
    homeData,
    enableBackButton,
    disableBackButton
  }
)(PlaydateDetails);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F5FCFF"
  },
  imagesliderContainer: {
    height: "45%"
  },
  slider: {backgroundColor: "#000", height: 350},
  content1: {
    width: "100%",
    height: 50,
    marginBottom: 10,
    backgroundColor: "#000",
    justifyContent: "center",
    alignItems: "center"
  },
  content2: {
    width: "100%",
    height: "10%",
    backgroundColor: "#e55595",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  contentText: {color: "#fff"},
  buttons: {
    zIndex: 1,
    height: 15,
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  button: {
    margin: 3,
    width: 15,
    height: 15,
    opacity: 0.9,
    alignItems: "center",
    justifyContent: "center"
  },
  buttonSelected: {
    opacity: 1,
    color: "red"
  },
  customSlide: {
    backgroundColor: "green",
    alignItems: "center",
    justifyContent: "center"
  },
  customImage: {
    width: "100%",
    height: "100%"
  },
  Tabs: {
    borderTopColor: "rgba(217, 214, 216, .2)",
    borderTopWidth: 2,
    borderStyle: "solid",
    zIndex: 9999,
    marginTop: -2
  },
  header: {
    position: "absolute",
    alignItems: "center",
    height: 60,
    width: 80,
    elevation: 5,
    top: 20,
    left: 20
  },
  backButton: {
    resizeMode: "contain"
  },
  title: {
    height: 40,
    fontSize: 20,
    color: "#e55595"
  },
  iconRightStyle: {
    position: "absolute",
    right: 10,
    bottom: Platform.OS === "ios" ? 2 : 0,
    width: 50,
    height: 40,
    alignItems: "center",
    justifyContent: "center"
  },
  iconLeftStyle: {
    position: "absolute",
    left: 0,
    bottom: 5,
    zIndex: 2,
    width: 70,
    height: 60,
    alignItems: "center",
    justifyContent: "center"
  },
  bringPetsSrollview: {
    marginLeft: 20,
    // flexDirection: "row",
    marginTop: 10
  },
  playdatehost: {
    flexDirection: "row",
    marginLeft: 20,
    marginTop: 10
  },
  addPicture: {
    position: "absolute",
    right: 10,
    backgroundColor: "#fff",
    height: 30,
    width: 30,
    borderWidth: 30,
    borderRadius: 30,
    borderColor: "#fff",
    bottom: height1
  },
  TextInput: {
    height: 80,
    backgroundColor: "#FFFFFF",
    color: "gray",
    paddingLeft: 10,
    alignItems: "flex-start",
    marginBottom: 30
  },
  saveButton: {
    height: 30,
    width: "60%",
    borderRadius: 10,
    backgroundColor: "#e55595",
    opacity: 0.9,
    alignItems: "center",
    justifyContent: "center"
  }
});
