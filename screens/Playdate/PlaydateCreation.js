/* eslint-disable prettier/prettier */
import React, {Component} from "react";
import {connect} from "react-redux";
import {reduxForm, Field} from "redux-form";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Share,
  AsyncStorage,
  TextInput,
  Dimensions,
  FlatList,
  ActivityIndicator
} from "react-native";
import moment from "moment";
import {Location, Permissions} from "expo";
import { NavigationActions } from 'react-navigation';

import Header from "../../components/Header";
import Spinner from "../../components/UI/activityIndicator";
import MyDatePicker2 from "../../components/UI/MyDatePicker2";
import {dummyPets} from "../../Data/pets";
import {styles} from "./playDateStyle";
import {
  fetchAllPets,
  createPlaydate,
  fetchSinglePlaydate,
  homeData
} from "../../store/actions";
import {baseurl} from "../../BaseUrl";

const {height} = Dimensions.get("window");
const dateFormat = "MM-DD-YYYY hh:mm A";

class PlaydateCreation extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    const dateStart = moment().format(dateFormat);
    const dateEnd = moment(this.getStartTime()).add(30, "m").format(dateFormat);

    this.state = {
      isDateTimePickerVisible1: false,
      isDateTimePickerVisible2: false,
      isChecked1: true,
      isChecked2: false,
      address: null,
      dateStartIsSet: false,
      dateEndIsSet: false,
      pets: [],
      nofityFriends: true,
      dateStart: dateStart,
      dateEnd: dateEnd,
      shouldNavigate: false,
      searchData: [],
      hideResults: false,
      selectedSerchItem: "",
      places: [],
      location: {},
      searchPlace: "",
      showSearchResultsDropdown: false,
      searchPlaceResults: [],
      searchPlaceId: "",
      latitude: 0,
      longitude: 0,
      resultLoading: false,
      description:''
    };
  }

  componentDidMount() {
    this.getLocationAsync();
    this.props.fetchAllPets();
    this.getloc();
  }

  componentWillReceiveProps(nextProps) {
    // const {loading, errors, playateAdded} = nextProps;
    // if (
    //   playateAdded === true &&
    //   this.state.shouldNavigate &&
    //   loading === false &&
    //   errors === null
    // ) {
    //   this.props.homeData(1, true);
    //   this.props.navigation.navigate("Home");
    // }
  }

  // Final data submission to the api
  PlaydateCreationHandler = async () => {
    const platdateData = {
      pets: this.state.pets,
      place: this.state.searchPlace,
      startTime: moment(this.state.dateStart, dateFormat),
      endTime: moment(this.state.dateEnd, dateFormat),
      status: "public",
      notifyFriends: false,
      placeId: this.state.searchPlaceId,
      description: this.state.description
    };

    if (this.state.isChecked1) {
      platdateData.status = "public";
    } else if (this.state.isChecked2) {
      platdateData.status = "private";
    }

    let isPlaydateValid = this.isPlaydateValid();
    if (isPlaydateValid) {
      let res = await this.props.createPlaydate(platdateData);
      if (res === true) {
        await this.props.homeData(1, true);
        this.props.navigation.navigate("Home");
      }else {
        alert("Could not create playdate. Please try again!")
      }
      // await this.setState({
      //   shouldNavigate: true
      // });
    }else {
      alert("Please Enter Valid data to create a playdate");
    }
  };

  petSelectionHnadler = petId => {
    const pet = this.state.pets.some(id => id === petId);

    this.setState(state => {
      let pets;
      if (pet) {
        pets = state.pets.filter(id => id !== petId);
      }

      if (!pet) {
        pets = state.pets.concat(petId);
      }

      return {
        pets
      };
    });
  };

  async getLocationRequest(url, method, header) {
    return fetch(url, {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: header
      }
    });
  }

  getloc = () => {
    this.saveLocation();
    this.getLatLong();
  };

  getLatLong() {
    const _this = this;
    navigator.geolocation.getCurrentPosition(
      position => {
        _this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
        _this.saveLocation();
      },
      error => {
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 10000}
    );
  }

  getLocationAsync = async () => {
    const {status} = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    }

    const location = await Location.getCurrentPositionAsync({});
    await this.setState({location});
    const {latitude, longitude} = this.state.location.coords;

    const params = {
      ll: `${latitude},${longitude}`,
      query: this.state.searchText
    };
    await this.setState({
      ...this.state,
      latitude,
      longitude
    });

  };
  saveLocation = async () => {
    const _this = this;
    if (this.state.latitude && this.state.longitude) {
      // this.setState({ loading: true });
      const url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
      const response = await this.getLocationRequest(
        `${url + this.state.latitude},${
          this.state.longitude
          }&key=AIzaSyBMr6Ee1JudyIZH0Q2yvh8aoqFA6kmHS0Q`
      );

      if (response.status === 200) {
        try {
          response.json().then(data => {
            let address = "";
            if (data.results.length > 0) {
              address = data.results[0].formatted_address;
            }

            _this.setState({
              loading: false,
              address
            });
          });
        } catch (error) {
          this.setState({loading: false});
          alert("There was an error saving your contract.");
        }
      } else {
        this.setState({...this.state, loading: false});
      }
    } else {
      _this.getLatLong();
    }
  };

  saveDateStartinState = date => {
    const parsedDate = moment(date, dateFormat);
    const endDate = parsedDate.add(30, "m").format(dateFormat);

    this.setState({
      dateStartIsSet: true,
      dateEnd: endDate,
      dateStart: date
    });
  };

  saveDateEndinState = async date => {
    let pendDate = moment(date, dateFormat);
    let endDate = pendDate.format(dateFormat);

    await this.setState({dateEndIsSet: true, dateEnd: endDate});
  };

  notifyFriends = () => (
    <View style={styles.playdateLocation}>
      <View
        style={{
          width: "100%",
          alignItems: "flex-start",
          justifyContent: "center"
        }}
      >
        <TouchableOpacity
          onPress={() => {
            this.setState({nofityFriends: !this.state.nofityFriends});
            Share.share({
              title: "Shared via Petluvs",
              message: "Shared via Petluvs",
              url: "../assets/LocationPhoto1.png"
            });
          }}
          style={styles.radioButton1}
        >
          <View>
            <View style={styles.radioButton1}>
              <Image source={require("../../assets/notifyIcon.png")}/>
              <Text
                style={{
                  fontSize: 20,
                  color: this.state.nofityFriends ? "#e55595" : "grey",
                  paddingLeft: 10
                }}
              >
                Notify friends
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );

  addPet = () => {
    const {navigate} = this.props.navigation;
    navigate("AddPet");
  };

  bringPets = pets => (
    <ScrollView
      showsHorizontalScrollIndicator={false}
      horizontal
      style={{width: "100%"}}
    >
      {pets.length === 0 ? (
        <View>
          <TouchableOpacity onPress={() => this.addPet()}>
            <View
              style={{
                paddingLeft: 10,
                paddingRight: 10,
                paddingBottom: 10
              }}
            >
              <Image
                source={require("../../assets/addNewPet.png")}
                style={{width: 40, height: 40, borderRadius: 40 / 2}}
              />
              <Text style={{color: "#EF5595", fontSize: 12, marginLeft: 4}}>
                {/*{item.name}*/}
                Add Pet
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      ) : (
        pets.map((item, key) => (
          <View key={key}>
            <TouchableOpacity
              onPress={() => this.petSelectionHnadler(item._id)}
            >
              <View style={styles.bringPetsSrollview}>
                <Image
                  source={{
                    uri:
                      item.imagePath[0] && item.imagePath[0].url && item.imagePath[0].url !== ""
                        ? item.imagePath[0].url
                        : "https://petluvsstore.s3.amazonaws.com/Petluvs-defaults/petluvs.png"
                  }}
                  style={{
                    width: 50,
                    height: 50,
                    borderRadius: 50 / 2,
                    marginLeft: 10,
                    opacity: this.state.pets.some(id => id === item._id)
                      ? 1
                      : 0.5
                  }}
                />
                <Text
                  style={{
                    color: item.color,
                    fontSize: 10,
                    alignSelf: "center"
                  }}
                >
                  {item.name}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        ))
      )}
    </ScrollView>
  );

  _hideDateTimePicker1 = () => {
    this.setState({
      isDateTimePickerVisible1: !this.state.isDateTimePickerVisible1
    });
    // alert(this.state.dateStart)
  };
  _hideDateTimePicker2 = () => {
    this.setState({
      isDateTimePickerVisible2: !this.state.isDateTimePickerVisible2
    });
    //                            alert(date)
  };

  getStartTime() {
    let start = moment().startOf('hour').add(30, 'm');
    let now = moment().format(dateFormat);
    if(start.isAfter(now)) {
      return start;
    } else {
      return start.add(30, 'm');
    }
  }

  playdateTiming = () => (
    <View style={styles.playdateTiming}>
      <Text style={{marginBottom: 2, fontSize: 15}}>Start Time</Text>
      <View style={styles.datetimeContainerStart}>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row",
            backgroundColor: "#fff"
          }}
          onPress={() => this._hideDateTimePicker1()}
        >
          <View>
            <Image
              style={styles.calender}
              source={require("../../assets/startTimeFieldIcon.png")}
            />
          </View>
          <View style={{justifyContent: "center", position: "absolute"}}>
            <Text style={styles.dateTimeText}>{this.state.dateStart}</Text>
          </View>
          <View style={styles.dateTime}>
            <Field
              name={"datetime2"}
              component={MyDatePicker2}
              style={{
                backgroundColor: "#00000000",
                width: "100%",
                height: "100%"
              }}
              date={this.state.dateStart}
              minDate={moment().format(dateFormat)}
              mode="datetime"
              saveDateinState={this.saveDateStartinState}
              showWarning={false}
              placeholder="Select Date"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              setModalVisible={this.state.isDateTimePickerVisible1}
              hideText
              customStyles={{
                dateIcon: {
                  position: "absolute",
                  left: 10,
                  width: 18,
                  height: 18,
                  marginLeft: 0,
                  alignItems: "center"
                },
                dateInput: {
                  backgroundColor: "#00000000",
                  borderColor: "white",
                  paddingRight: "50%",
                  width: "100%"
                }
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: "10%",
          alignItems: "center",
          justifyContent: "center",
          height: 20
        }}
      />
      <Text style={{marginBottom: 2, fontSize: 15}}>End Time</Text>
      <View style={styles.datetimeContainerend}>
        <TouchableOpacity
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "row",
            backgroundColor: "#fff"
          }}
          onPress={() => this._hideDateTimePicker2()}
        >
          <View style={{justifyContent: "center", position: "absolute"}}>
            <Text style={styles.dateTimeText}>{this.state.dateEnd ? this.state.dateEnd : moment(this.getStartTime()).add(30, 'm').format(dateFormat)}</Text>
          </View>
          <View>
            <Image
              style={styles.calender}
              source={require("../../assets/startTimeFieldIcon.png")}
            />
          </View>
          <View style={styles.dateTime}>
            <Field
              name={"datetime2"}
              component={MyDatePicker2}
              style={{
                backgroundColor: "#00000000",
                width: "85%",
                height: "100%"
              }}
              date={moment(this.getStartTime()).add(30, 'm').format(dateFormat)}
              mode="datetime"
              saveDateinState={this.saveDateEndinState}
              showWarning={false}
              placeholder="select date"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              setModalVisible={this.state.isDateTimePickerVisible1}
              hideText
              minDate={moment(this.state.dateStart, dateFormat).add(30, 'm').format(dateFormat)}
              customStyles={{
                dateIcon: {
                  position: "absolute",
                  left: 10,
                  width: 18,
                  height: 18,
                  marginLeft: 0,
                  alignItems: "center"
                },
                dateInput: {
                  backgroundColor: "#00000000",
                  borderColor: "white",
                  paddingRight: "50%",
                  width: "90%"
                }
              }}
            />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );

  searchPlace = async () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      },
      error => this.setState({error: error.message}),
      {enableHighAccuracy: true, timeout: 2000, maximumAge: 2000}
    );

    const query = this.state.searchPlace;
    const token = await AsyncStorage.getItem("token");
    const lat = this.state.latitude;
    const lng = this.state.longitude;

    try {
      const response = await fetch(
        `${baseurl}/places/place-suggestions-google?query=${query}&lat=${lat}&lng=${lng}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          }
        }
      );

      const responseJson = await response.json();
      const result = [];

      await responseJson.response.data.map(item => {
        const place = {
          name: item.name,
          id: item.place_id,
          formatted_address: item.formatted_address
        };
        result.push(JSON.parse(JSON.stringify(place)));
      });

      this.setState({
        searchPlaceResults: result,
        resultLoading: false
      });
    } catch (e) {
      this.setState({
        resultLoading: false,
        searchPlaceResults: [
          {
            name: "No results found/ Enable location",
            formatted_address: "No results found/ Enable location",
            placeId: "No results found/ Enable location"
          }
        ]
      });
      const errors = "Request Failed";
    }
  };

  renderResultList = () => (
    <View
      style={{
        position: "absolute",
        top: 42,
        width: "100%",
        height: height * 0.4,
        marginVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: "#fff",
        borderWidth: 2,
        borderTopWidth: 0,
        borderColor: "#e55595",
        borderBottomEndRadius: 10,
        borderBottomLeftRadius: 10
      }}
    >
      <FlatList
        data={this.state.searchPlaceResults}
        ItemSeparatorComponent={() => (
          <View style={{height: 1, width: "100%", backgroundColor: "gray"}}/>
        )}
        initialNumToRender={7}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => {
              this.setState({
                searchPlace:
                  item.name !== "No results found/ Enable location"
                    ? item.name.length !== 0
                    ? item.name
                    : item.formatted_address
                    : null,
                searchPlaceId:
                  item.name !== "No results found/ Enable location"
                    ? item.id
                    : null,
                showSearchResultsDropdown: false
              });
            }}
            style={{
              height: 55,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={{marginHorizontal: 10, color: "gray"}}>
              {item.name.length !== 0 ? item.name : item.formatted_address}
            </Text>
          </TouchableOpacity>
        )}
        keyExtractor={(item, index) => index.toString()}
        extraData={this.state.searchPlaceResults}
      />
    </View>
  );

  onSuggesstionFocus = () => {
    if (this.state.searchPlaceResults.length !== 0) {
      this.setState({
        ...this.state,
        showSearchResultsDropdown: true
      })
    }
  };

  // Validate playdate date
  isPlaydateValid = () => {
    let isValid = false;
    let { dateStart, dateEnd, pets, searchPlaceId, dateStartIsSet, dateEndIsSet } = this.state;
    if (
      dateStart 
      && dateEnd 
      && pets.length > 0 
      && searchPlaceId
      && dateStartIsSet
      && dateEndIsSet
    ) 
      isValid = true    
    return isValid;
  }

  render() {
    if (this.props.loading === true || this.state.loading) {
      return <Spinner/>;
    }

    let isValid = this.isPlaydateValid();

    return (
      <View style={{flex: 1}}>
        <Header
          title={"Create a Playdate"}
          navigation={this.props.navigation}
        />

        <ScrollView
          style={styles.mainContainer}
          onScroll={() => {
            this.setState({showSearchResultsDropdown: false});
          }}
          keyboardDismissMode={"on-drag"}
        >
          <View style={{height: 3, backgroundColor: "#E6FFFD"}}/>
          <View style={styles.bringPetsContainer}>
            <Text style={styles.bringPetsText}>
              Which pets are you bringing?
            </Text>
            {this.bringPets(this.props.pets)}
          </View>

          <View style={{marginTop: 20}}>
            <Text style={{fontSize: 15}}>Where?</Text>
          </View>
          <View
            style={{
              marginTop: 5,
              zIndex: 1,
              marginBottom: 0,
              backgroundColor: "red"
            }}
          >
            <TextInput
              style={styles.autocompleteContainer}
              value={this.state.searchPlace}
              onFocus={() => this.onSuggesstionFocus()}
              returnKeyType="search"
              onChangeText={val => {
                this.setState({
                  searchPlace: val,
                  showSearchResultsDropdown:
                    this.state.searchPlace !== null
                      ? false
                      : this.state.showSearchResultsDropdown
                });
              }}
              onSubmitEditing={ ()  =>{
                this.searchPlace();
                this.setState({
                  showSearchResultsDropdown: true,
                  resultLoading: true
                });
              }}
            />
            <TouchableOpacity
              style={{
                backgroundColor: "#e55595",
                height: 40,
                width: 60,
                alignItems: "center",
                justifyContent: "center",
                alignSelf: "flex-end",
                borderRadius: 4
              }}
              onPress={() => {
                this.searchPlace();
                this.setState({
                  showSearchResultsDropdown: true,
                  resultLoading: true
                });
              }}
            >
              {this.state.resultLoading ? (
                <ActivityIndicator color={"#fff"}/>
              ) : (
                <Text style={{margin: 5, fontSize: 12, color: "#fff"}}>
                  Go
                </Text>
              )}
            </TouchableOpacity>
            {this.state.showSearchResultsDropdown && !this.state.resultLoading
              ? this.renderResultList()
              : null}
          </View>

          <View style={{marginTop: 20}}>
            <Text style={{fontSize: 15}}>Description</Text>
          </View>
          <View
            style={{
              marginTop: 5,
              marginBottom: 20,
              backgroundColor: "red"
            }}
          >
            <TextInput
              style={styles.autocompleteContainer}
              maxLength={100}
              value={this.state.description}
              placeholder={'Description eg. meet by the south parking lot by the pool'}
              returnKeyType="done"
              onChangeText={val => {
                this.setState({description: val});
              }}
            />
          </View>

          <View style={[styles.playdateLocationContainer, {marginTop:40}]}>
            {this.playdateTiming()}
            {this.state.dateStartIsSet && this.state.dateEndIsSet ? null : (
              <View>
                <Text style={styles.validationAddress}>
                  Please enter a valid date!!
                </Text>
              </View>
            )}
          </View>
          <View style={styles.notifyFriendsContainer}>
            <View style={{flexDirection: "row"}}>
              <View
                style={{
                  // alignItems: "center",
                  justifyContent: "center"
                }}
              >
              </View>
              <Text
                style={{
                  fontSize: 15,
                  marginLeft: 10,
                  marginBottom: 10,
                  marginTop: 10
                }}
              >
                Notify your friends
              </Text>
            </View>
            {this.notifyFriends()}
          </View>
          <View style={styles.saveButtonContainer}>
            <TouchableOpacity
              disabled={!isValid}
              style={[ styles.saveButton, {backgroundColor: !isValid? "gray": "#e55595"} ]}
              onPress={() => {
                this.PlaydateCreationHandler();
              }}
              opacity={isValid ? 0.5 : 1}
            >
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text style={{color: "#fff", fontSize: 16}}>
                  Create Playdate
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.playDates.loading,
  errors: state.playDates.errors,
  pets: state.pets.pets,
  playateAdded: state.playDates.playateAdded
});

const PlaydateCreationForm = reduxForm({
  form: "AddPetForm",
  validate: values => {
    // const emailReg = /\S+@\S+\.\S+/g;
    const errors = {};
    errors.name = !values.name
      ? "name field is required"
      : // : !emailReg.test(values.email)
        //   ? "Email format is invalid"
      undefined;

    errors.startDateTime = !values.startDateTime
      ? "Please choose one of the startDateTime"
      : undefined;
    errors.DateTime1 = !values.DateTime1
      ? "Please choose one of the DateTime1"
      : undefined;
    errors.endDateTime = !values.endDateTime
      ? "Please choose one of the endDateTime"
      : undefined;
    errors.Location = !values.Location
      ? "Please enter a valid location"
      : undefined;
    errors.breedOptions = !values.breedOptions
      ? "Please choose one of the pet breed"
      : undefined;

    return errors;
  }
})(PlaydateCreation);
export default connect(
  mapStateToProps,
  {createPlaydate, fetchAllPets, fetchSinglePlaydate, homeData}
)(PlaydateCreationForm);
