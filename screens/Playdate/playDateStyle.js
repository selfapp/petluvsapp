import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: 20
  },
  bringPetsContainer: {},
  bringPetsText: {
    marginTop: 20,
    marginBottom: 10,
    fontSize: 15
  },
  bringPetsSrollview: {
    alignItems: "center",
    justifyContent: "center"
  },
  playdateLocation: {
    height: 40,
    width: "100%",
    borderWidth: 2,
    borderColor: "#fff",
    backgroundColor: "#fff",
    shadowOffset: { width: 1, height: 1 },
    elevation: 5,
    shadowColor: "#e55595",
    shadowOpacity: 0.5,
    justifyContent: "space-between",
    paddingLeft: 5,
    flexDirection: "row"
  },
  playdateLocationContainer: {
    marginTop: 20
  },
  CategoryButton: {
    backgroundColor: "#fff",
    paddingVertical: 6,
    paddingHorizontal: 16,
    marginRight: 12,
    marginHorizontal: 10,
    borderRadius: 4
  },
  dateTime: {
    flex: 1,
    height: "75%"
  },
  calender: {
    marginLeft: 10
  },
  dateTimeText: {
    alignSelf: "center",
    justifyContent: "center",
    color: "grey",
    fontSize: 14,
    marginLeft: 15
  },
  datetimeContainerStart: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#fff",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#e55595",
    shadowOpacity: 0.5,
    elevation: 5
  },
  datetimeContainerend: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#fff",
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#e55595",
    shadowOpacity: 0.5,
    elevation: 5
  },
  playdateTiming: {
    height: 140,
    width: "100%",
    borderWidth: 2,
    borderColor: "#fff",
    backgroundColor: "#fff",
    justifyContent: "space-between"
  },
  statusContainer: {
    height: "10%",
    marginTop: 20
  },
  radioButtonContainer: {
    marginTop: 10
  },
  radioButton1: {
    flexDirection: "row",
    alignItems: "center"
  },
  notifyFriendsContainer: {
    marginTop: 20
  },
  saveButton: {
    height: 50,
    width: "100%",
    borderRadius: 10,
    backgroundColor: "#e55595",
    marginBottom: 20
  },
  saveButtonContainer: {
    alignSelf: "flex-end",
    alignItems: "center",
    justifyContent: "flex-start",
    height: "10%",
    marginBottom: 10,
    width: "100%",
    marginTop: 10
  },
  TextInput: {
    width: "80%"
  },
  validationAddress: {
    position: "absolute",
    fontSize: 10,
    color: "red",
    marginTop: 2,
    marginLeft: 1
  },
  autocompleteContainer: {
    position: "absolute",
    width: "100%",
    height: 40,
    borderWidth: 0,
    borderColor: "#fff",
    backgroundColor: "#fff",
    shadowOffset: { width: 1, height: 1 },
    elevation: 5,
    shadowColor: "#e55595",
    shadowOpacity: 0.5,
    paddingLeft: 10
  },
  textInputContainer: {
    backgroundColor: "#fff",
    height: 14,
    borderTopColor: "#00000000",
    borderBottomColor: "#00000000",
    borderTopWidth: 0,
    borderBottomWidth: 0,
    flexDirection: "row"
  },
  textLabel: {
    margin: 15
  },
  autocompleteContainerRe: {
    flex: 1,
    left: 0,
    position: "absolute",
    right: 0,
    top: 0,
    zIndex: 1
  }
});
