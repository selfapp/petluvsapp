import { StyleSheet, Platform, Dimensions } from 'react-native';

const { height, width } = Dimensions.get("window");
const height1 = (((height / 2 / height) * 100 + 3) / 100) * height;


export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#F5FCFF"
    },
    imagesliderContainer: {
      height: "45%"
    },
    slider: { backgroundColor: "#000", height: 350 },
    content1: {
      width: "100%",
      height: 50,
      marginBottom: 10,
      backgroundColor: "#000",
      justifyContent: "center",
      alignItems: "center"
    },
    content2: {
      width: "100%",
      height: "10%",
      // marginTop: 10,
      backgroundColor: "#e55595",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row"
    },
    contentText: { color: "#fff" },
    buttons: {
      zIndex: 1,
      height: 15,
      // marginTop: -25,
      marginBottom: 10,
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "row"
    },
    button: {
      margin: 3,
      width: 15,
      height: 15,
      opacity: 0.9,
      alignItems: "center",
      justifyContent: "center"
    },
    buttonSelected: {
      opacity: 1,
      color: "red"
    },
    customSlide: {
      backgroundColor: "green",
      alignItems: "center",
      justifyContent: "center"
    },
    customImage: {
      width: "100%",
      height: "100%"
    },
    Tabs: {
      borderTopColor: "rgba(217, 214, 216, .2)",
      borderTopWidth: 2,
      borderStyle: "solid",
      zIndex: 9999,
      marginTop: -2
    },
    header: {
      position: "absolute",
      alignItems: "center",
      // justifyContent: 'flex-end',
      height: 60,
      width: 80,
      elevation: 5,
      top: 20,
      left: 20,
    },
    backButton: {
      height: "80%",
      width: "60%"
    },
    title: {
      height: 40,
      fontSize: 20,
      color: "#e55595"
    },
    iconRightStyle: {
      position: "absolute",
      right: 10,
      bottom: Platform.OS === "ios" ? 2 : 0, //-3:0
      width: 50,
      height: 40,
      alignItems: "center",
      justifyContent: "center"
    },
    iconLeftStyle: {
      position: "absolute",
      left: 0,
      bottom: 5,
      zIndex: 2,
      width: 50,
      height: 40,
      alignItems: "center",
      justifyContent: "center"
    },
    bringPetsSrollview: {
      marginLeft: 20,
      flexDirection: "row",
      marginTop: 10
    },
    playdatehost: {
      flexDirection: "row",
      marginLeft: 20,
      marginTop: 10
    },
    addPicture: {
      position: "absolute",
      right: 10,
      backgroundColor: "#fff",
      height: 30,
      width: 30,
      borderWidth: 30,
      borderRadius: 30,
      borderColor: "#fff",
      bottom: height1
    },
    bringPetsSrollview: {
      marginLeft: 20
    }
  });