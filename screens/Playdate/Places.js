import React from "react";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";

const listview = false;
let GooglePlacesInput = props => (
  <GooglePlacesAutocomplete
    placeholder="Search"
    minLength={2} // minimum length of text to search
    autoFocus={false}
    returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
    // listViewDisplayed={listview} // true/false/undefined
    fetchDetails
    renderDescription={row => row.description} // custom description render
    onPress={(data, details, row = null) => {
      // listview= false;

      props.getResults(details.icon,details.address_components[0].long_name,details)
    }}

    query={{
      // available options: https://developers.google.com/places/web-service/autocomplete
      key: "AIzaSyDycvP1_9xXHabi7CvSkYRjVg5NdfupRvw",
      language: "en", // language of the results
      types: "address" // default: 'geocode'
    }}
    styles={{
      textInputContainer: {
        backgroundColor: "#fff",
        height: 14,
        borderTopColor: "#00000000",
        borderBottomColor: "#00000000",
        borderTopWidth: 0,
        borderBottomWidth: 0,
        flexDirection: "row"
      },
      listView: {
        // color: "red", //To see where exactly the list is
        backgroundColor: "#ffffff",
        // zIndex: 9999, //To popover the component outwards
        borderWidth: 1,
        borderColor: "#999999",
        // position: "absolute",,
        marginTop: 30
        // top: 30
      },
      textInput: {
        flex: 1,
        borderRadius: 0,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
        marginTop: 0,
        marginLeft: 0,
        marginRight: 8
      },
      description: {
        fontWeight: "bold"
      },
      predefinedPlacesDescription: {
        color: "#1faadb"
      },
      container: props.container
    }}
    debounce={200}
  />
);

export default GooglePlacesInput;
