/* eslint-disable prettier/prettier */
import React from "react";
import {connect} from "react-redux";
import {
  View,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Text,
  Modal,
  Dimensions
} from "react-native";
import {Notifications} from "expo";
import CountDown from 'react-native-countdown-component';
import moment from 'moment';
import {NavigationEvents} from "react-navigation";

import {
  homeData,
  joinPlaydate,
  leavePlaydate,
  getNotificationList,
  notificationList,
  petSpeciesBreed,
  serviceRequestModel,
  removeHeaderTitle,
  incrementNotificationsCount
} from "../store/actions";
import ReviewItem from "../components/ReviewItem";
import MediaItem from "../components/MediaItem";
import GeneralStyles from "../Styles/General";
import PlaydateItem from "../components/PlaydateItem";

let {width} = Dimensions.get('window');

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  static getDerivedStateFromProps(props, state) {
    if (props.refreshing === false) {  
      return {
        refreshing: false,
        refresh: false
      };
    }
    return null;
  }

  state = {
    data: [],
    page: 1,
    loading: false,
    refreshing: false,
    refresh: false,
    serviceRequestModel: false,
    duration: 0,
    message: '',
  };

  componentDidMount() {
    this.props.getNotificationList();
    this.props.petSpeciesBreed();
    this.notificationSubscription = Notifications.addListener(
      this.handleNotification
    );
    this.props.homeData(this.state.page, this.state.refresh);

    this.willFocusLitener = this.props.navigation.addListener('willFocus', 
      (route) => {
        this.handleRefresh(); 
      }
    );
  }

  componentWillUnmount() {
    this.willFocusLitener.remove();
  }

  handleNotification = notificationRecieved => {
    this.props.incrementNotificationsCount();
    this.props.notificationList(notificationRecieved);
    const {origin} = notificationRecieved;
    const playdateId = notificationRecieved.data.id;
    const fromNotification = true;

    switch (notificationRecieved.data.category) {
      case 1:
        if (origin === "selected") {
          // noinspection JSAnnotator
          this.props.navigation.navigate("PlaydateDetails", {
            playdateId,
            fromNotification
          });
        }
        break;
      case 2:
        if (origin === "selected") {
          // noinspection JSAnnotator
          this.props.navigation.navigate("PlaydateDetails", {
            playdateId,
            fromNotification
          });
        }
        break;
      case 3:
        var startdate = moment(notificationRecieved.data.date);
        var endate = moment(startdate).add(31, 'seconds');
        var diff = endate.diff(startdate, 'seconds');

        if (diff > 0) {
          this.setState({duration: diff, message: notificationRecieved.data.popup});
          this.props.serviceRequestModel();
        }

        break;

      default: {
        this.props.navigation.navigate("Home");
      }
    }
  };

  handleLoadMore = () => {
    if (this.state.refreshing)
      return;
    if (this.props.totalPage > this.state.page) {
      this.setState(
        {
          page: this.state.page + 1,
          refreshing: false
        },
        () => {
          this.props.homeData(this.state.page, this.state.refresh);
        }
      );
    }
  };

  handleRefresh = async () => {
    if (this.state.refreshing)
      return;
    await this.setState({
        page: 1,
        refreshing: true,
        refresh: true
      },
      () => {
        this.props.homeData(1, true);
      }
    );
  };

  render() {
    return (
      <View
        style={GeneralStyles.ScreenContainer}
        showsVerticalScrollIndicator={false}
      >
        <NavigationEvents
          onWillFocus={payload => this.props.removeHeaderTitle()}
        />
        <View>
          <Modal
            animationType="slide"
            transparent={true}
            // visible={true}
            visible={this.props.serviceRequestModal}
          >
            <View style={{backgroundColor: '#00000040', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <View style={{backgroundColor: 'white'}}>
                <CountDown
                  until={this.state.duration}
                  size={30}
                  onFinish={() => {
                    this.props.serviceRequestModel()
                  }}
                  digitStyle={{backgroundColor: '#FFF'}}
                  digitTxtStyle={{color: '#1CC625'}}
                  timeToShow={['S']}
                  timeLabels={{s: 'SS'}}
                />

                <View style={{flexDirection: 'row', justifyContent: 'space-between', margin: 30}}>
                  <TouchableOpacity style={{
                    height: 50, width: '30%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 10,
                    backgroundColor: "#e55595",
                    margin: 20
                  }}>
                    <Text>Accept</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{
                    height: 50, width: '30%',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 10,
                    backgroundColor: "gray",
                    margin: 20
                  }}
                    onPress={() => {
                      this.props.serviceRequestModel()
                    }}
                  >
                    <Text>Decline</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

          </Modal>
          <FlatList
            data={this.props.data}
            keyExtractor={(item, i) => `key-${i}`}
            extraData={this.props.data}
            renderItem={({item}) =>
              item.category === 1 ? (
                <View style={{width: width - 40, alignSelf: 'center'}}>
                  <PlaydateItem
                    playdateId={item.record._id}
                    Name={
                      item.record.pets.length > 0
                        ? item.record.pets[0].name
                        : item.record.userId.local.email
                    }
                    TimeLeft={item.record.endTime}
                    StartTime={item.record.startTime}
                    EndTime={item.record.endTime}
                    Location={
                      item !== null &&
                      item.record.location !== undefined &&
                      item.record.location
                        ? item.record.location.locName
                        : ""
                    }
                    MilesAway={
                      item !== null &&
                      item.record.location !== undefined &&
                      item.record.location.distance !== undefined
                        ? item.record.location.distance
                        : "5.6"
                    }
                    // userJoining={item.userJoining}
                    item={item}
                    locPhotos={
                      item !== null &&
                      item.record.location !== undefined &&
                      item.record.location.photos !== undefined
                        ? item.record.location.photos
                        : []
                    }
                    photo={
                      item.record.pets.length > 0 &&
                      item.record.pets[0].imagePath[0].url !== null
                        ? item.record.pets[0].imagePath[0].url
                        : "https://s3.amazonaws.com/petluvs/leo.jpg"
                    }
                    playdateOwnerId={item.record.userId._id}
                    // fromHomeScreen={true}
                  />
                </View>
              ) : item.category === 2 ? (
                <View style={{marginBottom: 14, width: width - 40, alignSelf: 'center'}}>
                  <View style={[GeneralStyles.Card]}>
                  <ReviewItem
                    key={item._id}
                    id={item._id}
                    Name={item.record.userId.local.userName}
                    ProfilePic={item.record.userId.profilePic ? item.record.userId.profilePic : "https://s3.amazonaws.com/petluvs/leo.jpg"}
                    // BusinessName={item.record.playdateId.place}
                    starCount={item.record.starCount}
                    ReviewText={item.record.description}
                    playdateOwner={item.record.userId.pets[0].name}
                    showverification
                  />
                  </View>
                </View>
              ) : item.category === 4 ? (
                <View style={{marginBottom: 14, width: width - 40, alignSelf: 'center'}}>
                  <MediaItem
                    key={item._id}
                    id={item._id}
                    Name={item.record.petId.name}
                    MediaUri={item.record.url}
                    MediaType={item.record.mediaType}
                    Photo={
                      item.record.petId.imagePath 
                      && item.record.petId.imagePath[0].url 
                      && item.record.petId.imagePath[0].url !== ""
                      ? item.record.petId.imagePath[0].url
                      : "https://s3.amazonaws.com/petluvs/leo.jpg"
                    }
                  />
                </View>
              )
              : (
                <View style={{backgroundColor: "red", height: 20}}/>
              )
            }
            onRefresh={() => this.handleRefresh()}
            refreshing={this.state.refreshing}
            ListFooterComponent={() =>
              this.props.loading === true ? (
                <ActivityIndicator animating size="large"/>
              ) : null
            }
            onEndReached={() => {this.handleLoadMore()}}
            onEndReachedThreshold={0.5}
            ListHeaderComponent={() => <View style={{height: 20}}/>}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  data: state.home.data,
  loading: state.home.loading || state.user.loading,
  totalPage: state.home.totalPage,
  user: state.user.user,
  refreshing: state.home.refreshing,
  serviceRequestModal: state.user.serviceRequestModel
});

export default connect(
  mapStateToProps,
  {
    homeData,
    joinPlaydate,
    leavePlaydate,
    getNotificationList,
    notificationList,
    petSpeciesBreed,
    serviceRequestModel,
    removeHeaderTitle,
    incrementNotificationsCount
  }
)(HomeScreen);
