export class PlaceDetailsModel {
    placeId = "";
    serviceProviderId = "";
    description = "";
    starCount = 0;
    address = "";
    venueDetails = {
        name: "",
        description: "",
        venuePhotos: [],
        category: "",
        formattedAddress: "",
        tipsList: [],
        formattedPhone: "",
        canonicalUrl: "",
        count: 0,
        status: "",
        categories: []
    };

    constructor(data) {
        if (data.provider === "google") {
            this.placeId = data.placeId;
            this.description = data.description;
            this.starCount = data.star;
            this.address = data.address;

            this.venueDetails.name = data.locName;
            this.venueDetails.description = data.description;
            this.venueDetails.venuePhotos = [
                ...this.venueDetails.venuePhotos,
                ...data.photos
            ];
            this.venueDetails.category = data.types[0].toUpperCase();
            this.venueDetails.formattedAddress = data.address;
            this.venueDetails.tipsList = [
                ...this.venueDetails.tipsList,
                ...data.reviews
            ];
            this.venueDetails.formattedPhone = data.phoneNumber;
            this.venueDetails.canonicalUrl = data.website;
            this.venueDetails.count = data.star;
            this.venueDetails.status = "TBD";
            this.venueDetails.categories = [
                ...this.venueDetails.categories,
                ...data.types
            ];
        } else if (data.provider === "inAppServiceProvider") {
            this.venueDetails.name = data.userName;
            this.venueDetails.category = data.serviceType.toUpperCase();;
            this.venueDetails.venuePhotos = [
                ...this.venueDetails.venuePhotos,
                ...data.photos
            ];
            this.venueDetails.formattedPhone = data.phoneNumber;
            this.venueDetails.canonicalUrl = data.businessUrl ? data.businessUrl : "";
            this.venueDetails.count = data.star;
            this.venueDetails.description = data.description;

        } else {
            let {
                contact,
                location,
                photos,
                canonicalUrl,
                name,
                categories
            } = data;
            let {status} = data.hours;
            let {count} = data.likes;
            let {tips} = data;


            let venuePhotos = photos.groups
                .filter(el => el.type === "venue")[0]
                .items.map(el => {
                    return `${el.prefix}${el.height}x${el.width}${el.suffix}`;
                });
            this.venueDetails.venuePhotos = [
                ...this.venueDetails.venuePhotos,
                ...venuePhotos
            ];

            this.venueDetails.category = categories[0]["shortName"];
            this.venueDetails.formattedPhone = contact.formattedPhone;
            this.venueDetails.formattedAddress = location.formattedAddress;

            let tipsList = tips.groups.filter(el => el.type === "others")[0].items;
            this.venueDetails.tipsList = [
                ...this.venueDetails.tipsList,
                ...tipsList
            ];
        }
    }
}
