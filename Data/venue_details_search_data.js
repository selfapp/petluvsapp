export const venue_details = {
  meta: {
    code: 200,
    requestId: "5c38496d9fb6b72661fe1e63"
  },
  response: {
    venue: {
      id: "52d7ea06498ed9360e3b2ea3",
      name: "Pawliday Inn Pet Resort",
      contact: {
        phone: "2147993377",
        formattedPhone: "(214) 799-3377",
        twitter: "pawlidayinn",
        facebook: "611842922170435",
        facebookUsername: "pawlidayinndallas",
        facebookName: "Pawliday Inn Pet Resort"
      },
      location: {
        address: "1407 S Akard St",
        crossStreet: "at Bellebew",
        lat: 32.7701072692871,
        lng: -96.7927322387695,
        labeledLatLngs: [
          {
            label: "display",
            lat: 32.7701072692871,
            lng: -96.7927322387695
          }
        ],
        postalCode: "75215",
        cc: "US",
        city: "Dallas",
        state: "TX",
        country: "United States",
        formattedAddress: [
          "1407 S Akard St (at Bellebew)",
          "Dallas, TX 75215",
          "United States"
        ]
      },
      canonicalUrl: "https://foursquare.com/pawlidayinn",
      categories: [
        {
          id: "5032897c91d4c4b30a586d69",
          name: "Pet Service",
          pluralName: "Pet Services",
          shortName: "Pet Service",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }
      ],
      verified: true,
      stats: {
        tipCount: 2
      },
      url: "http://www.pawlidayinnresort.com",
      likes: {
        count: 3,
        groups: [
          {
            type: "others",
            count: 3,
            items: [
              {
                id: "6992623",
                firstName: "Adelaida",
                lastName: "Diaz-Roa",
                gender: "female",
                photo: {
                  prefix: "https://fastly.4sqi.net/img/user/",
                  suffix: "/6992623-AJSVQI2T40LRJUCW.jpg"
                }
              },
              {
                id: "65031687",
                firstName: "Nate",
                lastName: "Foster",
                gender: "none",
                photo: {
                  prefix: "https://fastly.4sqi.net/img/user/",
                  suffix: "/65031687-S0DCDYAK5M53ORSF.jpg"
                }
              },
              {
                id: "76335900",
                firstName: "Pawliday Inn Pet Resort",
                gender: "none",
                photo: {
                  prefix: "https://fastly.4sqi.net/img/user/",
                  suffix: "/76335900-KVTY3CZY0245SSKS.png"
                },
                type: "venuePage",
                venue: {
                  id: "52d7ea06498ed9360e3b2ea3"
                }
              }
            ]
          }
        ],
        summary: "3 Likes"
      },
      dislike: false,
      ok: false,
      allowMenuUrlEdit: true,
      beenHere: {
        count: 0,
        unconfirmedCount: 0,
        marked: false,
        lastCheckinExpiredAt: 0
      },
      specials: {
        count: 0,
        items: []
      },
      photos: {
        count: 21,
        groups: [
          {
            type: "checkin",
            name: "Friends' check-in photos",
            count: 0,
            items: []
          },
          {
            type: "venue",
            name: "Venue photos",
            count: 21,
            items: [
              {
                id: "53447d80498e31bd64e4592b",
                createdAt: 1396997504,
                source: {
                  name: "Foursquare Web",
                  url: "https://foursquare.com"
                },
                prefix: "https://fastly.4sqi.net/img/general/",
                suffix:
                  "/76335900_PbwwJHEze689qlw7xuh7fzQ4IiyuO-lmXBxiHDfyacU.jpg",
                width: 2000,
                height: 1333,
                user: {
                  id: "76335900",
                  firstName: "Pawliday Inn Pet Resort",
                  gender: "none",
                  photo: {
                    prefix: "https://fastly.4sqi.net/img/user/",
                    suffix: "/76335900-KVTY3CZY0245SSKS.png"
                  },
                  type: "venuePage",
                  venue: {
                    id: "52d7ea06498ed9360e3b2ea3"
                  }
                },
                visibility: "public"
              },
              {
                id: "52d7edfc498ed9360e3c2951",
                createdAt: 1389882876,
                source: {
                  name: "Foursquare Web",
                  url: "https://foursquare.com"
                },
                prefix: "https://fastly.4sqi.net/img/general/",
                suffix:
                  "/76335900_RwF9vkqsJXpOWVAVvpkn9hlTq5yfC7MdF4MzW_ChFAY.jpg",
                width: 852,
                height: 640,
                user: {
                  id: "76335900",
                  firstName: "Pawliday Inn Pet Resort",
                  gender: "none",
                  photo: {
                    prefix: "https://fastly.4sqi.net/img/user/",
                    suffix: "/76335900-KVTY3CZY0245SSKS.png"
                  },
                  type: "venuePage",
                  venue: {
                    id: "52d7ea06498ed9360e3b2ea3"
                  }
                },
                visibility: "public"
              }
            ]
          }
        ],
        summary: "0 photos"
      },
      venuePage: {
        id: "76335900"
      },
      reasons: {
        count: 0,
        items: []
      },
      description:
        "Pawliday Inn is the perfect place to leave your pets while at work or vacation! We offer services such as grooming, chauffeurs, daycare, and boarding.",
      page: {
        user: {
          id: "76335900",
          firstName: "Pawliday Inn Pet Resort",
          gender: "none",
          photo: {
            prefix: "https://fastly.4sqi.net/img/user/",
            suffix: "/76335900-KVTY3CZY0245SSKS.png"
          },
          type: "venuePage",
          venue: {
            id: "52d7ea06498ed9360e3b2ea3"
          },
          tips: {
            count: 22
          },
          lists: {
            groups: [
              {
                type: "created",
                count: 2,
                items: []
              }
            ]
          },
          homeCity: "Dallas, TX",
          bio: "",
          contact: {
            twitter: "pawlidayinn",
            facebook: "611842922170435"
          }
        }
      },
      hereNow: {
        count: 0,
        summary: "Nobody here",
        groups: []
      },
      createdAt: 1389881862,
      tips: {
        count: 2,
        groups: [
          {
            type: "others",
            name: "All tips",
            count: 2,
            items: [
              {
                id: "5344807b498e7b69aaee59a0",
                createdAt: 1396998267,
                text:
                  "Leave your puppy for a fun day of daycare while you're at work! :)",
                type: "user",
                canonicalUrl:
                  "https://foursquare.com/item/5344807b498e7b69aaee59a0",
                likes: {
                  count: 0,
                  groups: []
                },
                logView: true,
                agreeCount: 0,
                disagreeCount: 0,
                todo: {
                  count: 0
                },
                user: {
                  id: "6992623",
                  firstName: "Adelaida",
                  lastName: "Diaz-Roa",
                  gender: "female",
                  photo: {
                    prefix: "https://fastly.4sqi.net/img/user/",
                    suffix: "/6992623-AJSVQI2T40LRJUCW.jpg"
                  }
                },
                authorInteractionType: "liked"
              },
              {
                id: "530e384211d2bfb85c2f794d",
                createdAt: 1393440834,
                text:
                  "I LOVE it! Ever since my 1st visit, my pups can't get enough, they always have fun with other pups & have a lot of space to run in so it's awesome. I also get cute text updates. Best service ever! :)",
                type: "user",
                canonicalUrl:
                  "https://foursquare.com/item/530e384211d2bfb85c2f794d",
                likes: {
                  count: 0,
                  groups: []
                },
                logView: true,
                agreeCount: 0,
                disagreeCount: 0,
                todo: {
                  count: 0
                },
                user: {
                  id: "6992623",
                  firstName: "Adelaida",
                  lastName: "Diaz-Roa",
                  gender: "female",
                  photo: {
                    prefix: "https://fastly.4sqi.net/img/user/",
                    suffix: "/6992623-AJSVQI2T40LRJUCW.jpg"
                  }
                },
                authorInteractionType: "liked"
              }
            ]
          }
        ]
      },
      shortUrl: "http://4sq.com/Kllkfo",
      timeZone: "America/Chicago",
      listed: {
        count: 0,
        groups: [
          {
            type: "others",
            name: "Lists from other people",
            count: 0,
            items: []
          }
        ]
      },
      hours: {
        status: "Closed until 7:00 AM",
        richStatus: {
          entities: [],
          text: "Closed until 7:00 AM"
        },
        isOpen: false,
        isLocalHoliday: false,
        dayData: [],
        timeframes: [
          {
            days: "Mon–Fri",
            includesToday: true,
            open: [
              {
                renderedTime: "7:00 AM–7:00 PM"
              }
            ],
            segments: []
          },
          {
            days: "Sat",
            open: [
              {
                renderedTime: "11:00 AM–3:00 PM"
              }
            ],
            segments: []
          }
        ]
      },
      pageUpdates: {
        count: 22,
        items: []
      },
      inbox: {
        count: 0,
        items: []
      },
      attributes: {
        groups: [
          {
            type: "payments",
            name: "Credit Cards",
            summary: "Credit Cards",
            count: 7,
            items: [
              {
                displayName: "Credit Cards",
                displayValue: "Yes (incl. Discover & Visa)"
              }
            ]
          },
          {
            type: "wifi",
            name: "Wi-Fi",
            summary: "Free Wi-Fi",
            count: 1,
            items: [
              {
                displayName: "Wi-Fi",
                displayValue: "Free"
              }
            ]
          }
        ]
      },
      bestPhoto: {
        id: "53447d80498e31bd64e4592b",
        createdAt: 1396997504,
        source: {
          name: "Foursquare Web",
          url: "https://foursquare.com"
        },
        prefix: "https://fastly.4sqi.net/img/general/",
        suffix: "/76335900_PbwwJHEze689qlw7xuh7fzQ4IiyuO-lmXBxiHDfyacU.jpg",
        width: 2000,
        height: 1333,
        visibility: "public"
      }
    }
  }
};

// https://fastly.4sqi.net/img/general/2000x1333/76335900_PbwwJHEze689qlw7xuh7fzQ4IiyuO-lmXBxiHDfyacU.jpg
