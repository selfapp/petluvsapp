export const pet_trainers = {
  meta: {
    code: 200,
    requestId: "5c3847ba351e3d7774f62b20"
  },
  response: {
    venues: [{
      id: "52d7ea06498ed9360e3b2ea3",
      name: "Pawliday Inn Pet Resort",
      location: {
        address: "1407 S Akard St",
        crossStreet: "at Bellebew",
        lat: 32.7701072692871,
        lng: -96.7927322387695,
        labeledLatLngs: [{
          label: "display",
          lat: 32.7701072692871,
          lng: -96.7927322387695
        }],
        distance: 835,
        postalCode: "75215",
        cc: "US",
        city: "Dallas",
        state: "TX",
        country: "United States",
        formattedAddress: [
          "1407 S Akard St (at Bellebew)",
          "Dallas, TX 75215",
          "United States"
        ]
      },
      categories: [{
        id: "5032897c91d4c4b30a586d69",
        name: "Pet Service",
        pluralName: "Pet Services",
        shortName: "Pet Service",
        icon: {
          prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
          suffix: ".png"
        },
        primary: true
      }],
      venuePage: {
        id: "76335900"
      },
      referralId: "v-1547192250",
      hasPerk: false
    },
      {
        id: "5b8479a5b6eedb002cd1c2a7",
        name: "Christopher's Aquatic Pet Supplies",
        location: {
          crossStreet: "Dallas",
          lat: 32.782403813986825,
          lng: -96.80623412132263,
          labeledLatLngs: [{
            label: "display",
            lat: 32.782403813986825,
            lng: -96.80623412132263
          }],
          distance: 1072,
          postalCode: "75201",
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "Dallas",
            "Dallas, TX 75201",
            "United States"
          ]
        },
        categories: [{
          id: "56aa371be4b08b9a8d573508",
          name: "Pet Café",
          pluralName: "Pet Cafés",
          shortName: "Pet Café",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "5c06b1d29411f2002c1c0138",
        name: "Dog Walker - Dog Walkers and Pet Sitters in Dallas",
        location: {
          address: "2011 Cedar Springs Rd",
          lat: 32.791072,
          lng: -96.80552,
          labeledLatLngs: [{
            label: "display",
            lat: 32.791072,
            lng: -96.80552
          }],
          distance: 1787,
          postalCode: "75201",
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "2011 Cedar Springs Rd",
            "Dallas, TX 75201",
            "United States"
          ]
        },
        categories: [{
          id: "5032897c91d4c4b30a586d69",
          name: "Pet Service",
          pluralName: "Pet Services",
          shortName: "Pet Service",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "5a616e3f1acf115ff990bd64",
        name: "Atera Pet Playground",
        location: {
          address: "4606 Cedar Springs Rd",
          lat: 32.81869283786294,
          lng: -96.81764698549784,
          labeledLatLngs: [{
            label: "display",
            lat: 32.81869283786294,
            lng: -96.81764698549784
          }],
          distance: 5058,
          postalCode: "75219",
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "4606 Cedar Springs Rd",
            "Dallas, TX 75219",
            "United States"
          ]
        },
        categories: [{
          id: "4bf58dd8d48988d163941735",
          name: "Park",
          pluralName: "Parks",
          shortName: "Park",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/park_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "59524c0c4a7aae35d820ef20",
        name: "Feather & Fur Pet Care",
        location: {
          lat: 32.76086485213582,
          lng: -96.78429004670761,
          labeledLatLngs: [{
            label: "display",
            lat: 32.76086485213582,
            lng: -96.78429004670761
          }],
          distance: 2126,
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "Dallas, TX",
            "United States"
          ]
        },
        categories: [{
          id: "5032897c91d4c4b30a586d69",
          name: "Pet Service",
          pluralName: "Pet Services",
          shortName: "Pet Service",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "536a2196498eb6d532a8e406",
        name: "Lucca Bella pet spa",
        location: {
          lat: 32.79561213798568,
          lng: -96.80091646723004,
          labeledLatLngs: [{
            label: "display",
            lat: 32.79561213798568,
            lng: -96.80091646723004
          }],
          distance: 2136,
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "Dallas, TX",
            "United States"
          ]
        },
        categories: [{
          id: "5032897c91d4c4b30a586d69",
          name: "Pet Service",
          pluralName: "Pet Services",
          shortName: "Pet Service",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "5472444d498e78d5fb28fe64",
        name: "Pet Supplies Plus",
        location: {
          address: "1304 Greenville",
          crossStreet: "Ross",
          lat: 32.811402934444224,
          lng: -96.7699168843133,
          labeledLatLngs: [{
            label: "display",
            lat: 32.811402934444224,
            lng: -96.7699168843133
          }],
          distance: 4620,
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "1304 Greenville (Ross)",
            "Dallas, TX",
            "United States"
          ]
        },
        categories: [{
          id: "4bf58dd8d48988d100951735",
          name: "Pet Store",
          pluralName: "Pet Stores",
          shortName: "Pet Store",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "58c2ae04ae97567d4fc15350",
        name: "Pet Supplies Plus",
        location: {
          address: "2525 Wycliff Ave",
          crossStreet: "Dallas North Tollway",
          lat: 32.81876640483231,
          lng: -96.81763113985517,
          labeledLatLngs: [{
            label: "display",
            lat: 32.81876640483231,
            lng: -96.81763113985517
          }],
          distance: 5065,
          postalCode: "75219",
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "2525 Wycliff Ave (Dallas North Tollway)",
            "Dallas, TX 75219",
            "United States"
          ]
        },
        categories: [{
          id: "4bf58dd8d48988d100951735",
          name: "Pet Store",
          pluralName: "Pet Stores",
          shortName: "Pet Store",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "5328da1d498e0b93ad694cd5",
        name: "Pet Supermarket",
        location: {
          address: "3911 Lemmon Ave",
          lat: 32.81278214507325,
          lng: -96.80728250216336,
          labeledLatLngs: [{
            label: "display",
            lat: 32.81278214507325,
            lng: -96.80728250216336
          }],
          distance: 4130,
          postalCode: "75219",
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "3911 Lemmon Ave",
            "Dallas, TX 75219",
            "United States"
          ]
        },
        categories: [{
          id: "4bf58dd8d48988d100951735",
          name: "Pet Store",
          pluralName: "Pet Stores",
          shortName: "Pet Store",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      },
      {
        id: "56045e14498e6d3775930820",
        name: "Pet Supplies Plus",
        location: {
          address: "6060 E Mockingbird Ln",
          crossStreet: "at Skillman St.",
          lat: 32.836241207602406,
          lng: -96.76227179415007,
          labeledLatLngs: [{
            label: "display",
            lat: 32.836241207602406,
            lng: -96.76227179415007
          }],
          distance: 7381,
          postalCode: "75206",
          cc: "US",
          city: "Dallas",
          state: "TX",
          country: "United States",
          formattedAddress: [
            "6060 E Mockingbird Ln (at Skillman St.)",
            "Dallas, TX 75206",
            "United States"
          ]
        },
        categories: [{
          id: "4bf58dd8d48988d100951735",
          name: "Pet Store",
          pluralName: "Pet Stores",
          shortName: "Pet Store",
          icon: {
            prefix: "https://ss3.4sqi.net/img/categories_v2/shops/pet_store_",
            suffix: ".png"
          },
          primary: true
        }],
        referralId: "v-1547192250",
        hasPerk: false
      }
    ]
  }
};