export const dogParks = {
  meta: {
    code: 200,
    requestId: "5c3838c64434b94789d499a2"
  },
  response: {
    minivenues: [
      {
        id: "4c0a8e6cffb8c9b674c06b61",
        name: "Browder St. Mall Dog Park",
        location: {
          address: "1506 Commerce St",
          crossStreet: "Browder",
          city: "Dallas",
          state: "TX",
          postalCode: "75201",
          country: "US",
          lat: 32.77951926855419,
          lng: -96.79826691268886,
          distance: 335
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "50909aa2e4b0263fae91fcf6",
        name: "Third Rail Dog Park",
        location: {
          city: "Dallas",
          state: "TX",
          postalCode: "75202",
          country: "US",
          lat: 32.78091426127246,
          lng: -96.79992956920137,
          distance: 543
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "4eba0b454901ce3895fe070f",
        name: "Davis Building dog park",
        location: {
          address: "1309 Main St",
          city: "Dallas",
          state: "TX",
          postalCode: "75202",
          country: "US",
          lat: 32.78079824039632,
          lng: -96.8006980135438,
          distance: 572
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "4e52a22788775123c0eb9e50",
        name: "Dog Park",
        location: {
          city: "Dallas",
          state: "TX",
          country: "US",
          lat: 32.78260925963876,
          lng: -96.79933139703398,
          distance: 693
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "51322aa6e4b082687eb7f62c",
        name: "Cedars Dog Park",
        location: {
          city: "Dallas",
          state: "TX",
          postalCode: "75215",
          country: "US",
          lat: 32.768781995210205,
          lng: -96.7952869724363,
          distance: 895
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "58ddc9d4ca10703b62c6e2f7",
        name: "Petopia - 555 Ross Dog Park",
        location: {
          city: "Dallas",
          state: "TX",
          postalCode: "75202",
          country: "US",
          lat: 32.7820789607713,
          lng: -96.80860597355478,
          distance: 1240
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "4b538089f964a520ea9f27e3",
        name: "Meadows Foundation Dog Park",
        location: {
          address: "500 Liberty St.",
          crossStreet: "btwn Live Oak St. & Swiss Ave.",
          city: "Dallas",
          state: "TX",
          postalCode: "75204",
          country: "US",
          lat: 32.78906407622916,
          lng: -96.78568569145853,
          distance: 1736
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "509d8d31e4b038882485e3e4",
        name: "Klyde Warren Dog Park",
        location: {
          city: "Dallas",
          state: "TX",
          postalCode: "75201",
          country: "US",
          lat: 32.7900699415852,
          lng: -96.80061913334741,
          distance: 1526
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "4e2cefd2e4cd3bc166a54ced",
        name: "Amli Dog Park",
        location: {
          address: "821 Allen St",
          city: "Dallas",
          state: "TX",
          postalCode: "75204",
          country: "US",
          lat: 32.79037011695919,
          lng: -96.79043875200344,
          distance: 1640
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      },
      {
        id: "51c91a23498e5015b0ee0f21",
        name: "Northend Dog Park",
        location: {
          city: "Dallas",
          state: "TX",
          postalCode: "75201",
          country: "US",
          lat: 32.78860684792558,
          lng: -96.80740383425639,
          distance: 1644
        },
        categories: [
          {
            id: "4bf58dd8d48988d1e5941735",
            name: "Dog Run",
            pluralName: "Dog Runs",
            shortName: "Dog Run",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/dogrun_",
              suffix: ".png"
            },
            primary: true
          },
          {
            id: "4d4b7105d754a06377d81259",
            name: "Outdoors & Recreation",
            pluralName: "Outdoors & Recreation",
            shortName: "Outdoors & Recreation",
            icon: {
              prefix:
                "https://ss3.4sqi.net/img/categories_v2/parks_outdoors/default_",
              suffix: ".png"
            }
          }
        ],
        hasPerk: false
      }
    ]
  }
};
