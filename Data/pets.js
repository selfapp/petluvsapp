export const dummyPets = [
  {
    _id: "5bc6925053be3712ec5461b8",
    name: "Tommy",
    Image: require("../assets/PetImage.png"),
    breed: "Pug / Boston Terrier",
    species: "Cat"
  },
  {
    _id: "5bc692ce53be3712ec5461b9",
    name: "Tank",
    Image: require("../assets/PetImage.png"),
    breed: "Pug / Boston Terrier",
    species: "Dog"
  },
  {
    _id: "5bc693e353be3712ec5461bb",
    name: "Bella",
    Image: require("../assets/PetImage.png"),
    breed: "",
    species: "Cat"
  },
  {
    _id: "5bc9491353be3712ec5461be",
    name: "Daisy",
    Image: require("../assets/PetImage.png"),
    breed: "",
    species: "Cat"
  }
];
