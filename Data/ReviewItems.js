export const reviewItems = [
  {
    _id: 1,
    name: "Md Rahaman",
    businessName: "Pup Suds Pet Grooming",
    reviewText: "Works perfectly."
  },
  {
    _id: 2,
    name: "Shagor",
    businessname: "The Pawsitive Co",
    reviewText:
      "I love that I get a nice looking collar and it also helps to feed another animal all at the same time."
  },
  {
    _id: 3,
    name: "Loretta French",
    businessname: "The Pawsitive Co",
    reviewText:
      "I love that I get a nice looking collar and it also helps to feed another animal all at the same time."
  }
];
